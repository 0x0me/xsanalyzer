// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.ListModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.springframework.binding.value.ValueModel;
import org.springframework.binding.value.support.ValueHolder;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.richclient.application.PageComponentContext;
import org.springframework.richclient.application.support.AbstractView;
import org.springframework.richclient.command.CommandGroup;
import org.springframework.richclient.command.GuardedActionCommandExecutor;
import org.springframework.richclient.command.support.AbstractActionCommandExecutor;
import org.springframework.richclient.command.support.GlobalCommandIds;
import org.springframework.richclient.factory.ComponentFactory;
import org.springframework.richclient.layout.GridBagLayoutBuilder;
import org.springframework.richclient.list.FilteredListModel;
import org.springframework.richclient.list.ListSelectionValueModelAdapter;
import org.springframework.richclient.util.PopupMenuMouseListener;

import de.mindcrimeilab.util.SimilarTypeEntryAccessor;
import de.mindcrimeilab.xsanalyzer.actions.AbstractXSObjectCommand;
import de.mindcrimeilab.xsanalyzer.actions.TypeHierarchyCommand;
import de.mindcrimeilab.xsanalyzer.actions.TypeInspectionCommand;
import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.actions.XsElementPropertiesExecutor;
import de.mindcrimeilab.xsanalyzer.model.JListSelectionValueModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.SimilarTypeListModelEntry;
import de.mindcrimeilab.xsanalyzer.model.SimilarTypeListModelFactory;
import de.mindcrimeilab.xsanalyzer.model.SimilarTypesListModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.rules.constraints.SchemaTypeConstraint;
import de.mindcrimeilab.xsanalyzer.ui.MasterListDetailList;
import de.mindcrimeilab.xsanalyzer.ui.filter.FilterToolbarFactory;
import de.mindcrimeilab.xsanalyzer.ui.renderer.SchemaElementsRenderer;
import de.mindcrimeilab.xsanalyzer.ui.renderer.SimilarTypeRenderer;

/**
 * User interface to the discovered similar types in the schema.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SimilarTypesView extends AbstractView implements ApplicationListener {

    private final MasterListDetailList masterDetailList;

    private XsAnalyzerApplicationModel model;

    private final ValueModel numSimilarTypesSchema = new ValueHolder(0);

    private final ValueModel numSimilarTypes = new ValueHolder(0);

    private final XsElementPropertiesExecutor xsElementPropertiesExecutor = new XsElementPropertiesExecutor();

    private final GuardedActionCommandExecutor propertiesExecutor = new PropertiesExecutor();

    private final AbstractXSObjectCommand typesListTypeHierarchyCommand;

    private final AbstractXSObjectCommand similarTypesTypeHierarchyCommand;

    private final AbstractXSObjectCommand typesListTypeInspectionCommand;

    private final AbstractXSObjectCommand similarTypesInspectionCommand;

    private final SchemaTypeConstraint elementTypeConstraint;

    public SimilarTypesView() {
        final SimilarTypeEntryAccessor accessor = new SimilarTypeEntryAccessor();

        similarTypesTypeHierarchyCommand = new TypeHierarchyCommand();
        similarTypesTypeHierarchyCommand.setAccessor(accessor);

        similarTypesInspectionCommand = new TypeInspectionCommand();
        similarTypesInspectionCommand.setAccessor(accessor);

        typesListTypeHierarchyCommand = new TypeHierarchyCommand();
        typesListTypeInspectionCommand = new TypeInspectionCommand();

        elementTypeConstraint = new SchemaTypeConstraint(accessor);

        masterDetailList = new MasterListDetailList();
        masterDetailList.setListModelFactory(new SimilarTypeListModelFactory(elementTypeConstraint));

        masterDetailList.addPropertyChangeListener(MasterListDetailList.PC_MASTERLIST_LISTMODEL, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                logger.debug("Adjusting commands to new model " + evt.getPropertyName());
                final ListModel listModel = (ListModel) evt.getNewValue();
                final ValueModel selectionModel = new ListSelectionValueModelAdapter(masterDetailList.getMasterList().getSelectionModel());
                final ValueModel vModel = new JListSelectionValueModelAdapter(listModel, (ListSelectionValueModelAdapter) selectionModel);
                similarTypesTypeHierarchyCommand.setValueModel(vModel);
                similarTypesInspectionCommand.setValueModel(vModel);
                numSimilarTypesSchema.setValue(listModel.getSize());
            }
        });

        masterDetailList.addPropertyChangeListener(MasterListDetailList.PC_DETAILLIST_LISTMODEL, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                logger.debug("Adjusting commands to new model " + evt.getPropertyName());
                final ListModel listModel = (ListModel) evt.getNewValue();
                final ValueModel selectionModel = new ListSelectionValueModelAdapter(masterDetailList.getDetailList().getSelectionModel());
                final ValueModel vModel = new JListSelectionValueModelAdapter(listModel, (ListSelectionValueModelAdapter) selectionModel);
                typesListTypeHierarchyCommand.setValueModel(vModel);
                typesListTypeInspectionCommand.setValueModel(vModel);
                numSimilarTypes.setValue(listModel.getSize());
            }
        });
    }

    @Override
    protected void registerLocalCommandExecutors(PageComponentContext context) {
        context.register(GlobalCommandIds.PROPERTIES, propertiesExecutor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.application.support.AbstractView#createControl()
     */
    @Override
    protected JComponent createControl() {
        final ComponentFactory cf = getComponentFactory();

        final JList masterList = masterDetailList.getMasterList();
        final JList detailList = masterDetailList.getDetailList();
        detailList.setCellRenderer(new SchemaElementsRenderer());
        masterList.setCellRenderer(new SimilarTypeRenderer());

        // build master view
        final GridBagLayoutBuilder typeListPanelBuilder = new GridBagLayoutBuilder();
        typeListPanelBuilder.append(cf.createLabel("similarTypesView.typeList.label", new ValueModel[] { numSimilarTypesSchema})).nextLine();
        typeListPanelBuilder.append(cf.createScrollPane(masterList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), 1, 1, true, true);

        final CommandGroup group = getWindowCommandManager().createCommandGroup("similarTypesCommandGroup", new Object[] { similarTypesTypeHierarchyCommand, similarTypesInspectionCommand, "separator", GlobalCommandIds.PROPERTIES});
        masterList.addMouseListener(new PopupMenuMouseListener(group.createPopupMenu()));

        masterList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent evt) {
                Map<String, XSObject> parameters = new HashMap<String, XSObject>();
                final SimilarTypeListModelEntry similarTypeListModelEntry = (SimilarTypeListModelEntry) masterList.getSelectedValue();
                if (null == similarTypeListModelEntry) return;
                parameters.put(XsElementPropertiesExecutor.OBJECT, similarTypeListModelEntry.getType());
                xsElementPropertiesExecutor.execute(parameters);
            }
        });

        // build client view
        final GridBagLayoutBuilder similarTypesListPanelBuilder = new GridBagLayoutBuilder();
        similarTypesListPanelBuilder.append(cf.createLabel("similarTypesView.similarTypeList.label", new ValueModel[] { numSimilarTypes})).nextLine();
        similarTypesListPanelBuilder.append(cf.createScrollPane(detailList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), 1, 1, true, true);

        final CommandGroup detailListGroup = getWindowCommandManager().createCommandGroup("typeListCommandGroup", new Object[] { typesListTypeHierarchyCommand, typesListTypeInspectionCommand, "separator", GlobalCommandIds.PROPERTIES});
        detailList.addMouseListener(new PopupMenuMouseListener(detailListGroup.createPopupMenu()));

        detailList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent evt) {
                Map<String, XSObject> parameters = new HashMap<String, XSObject>();
                parameters.put(XsElementPropertiesExecutor.OBJECT, (XSObject) detailList.getSelectedValue());
                xsElementPropertiesExecutor.execute(parameters);
            }
        });

        // build main component
        JSplitPane jspSimilarTypes = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jspSimilarTypes.setDividerLocation(0.66f);
        jspSimilarTypes.setTopComponent(typeListPanelBuilder.getPanel());
        jspSimilarTypes.setBottomComponent(similarTypesListPanelBuilder.getPanel());

        JPanel jpSimilarTypes = cf.createPanel(new BorderLayout());
        jpSimilarTypes.add(jspSimilarTypes, BorderLayout.CENTER);
        jpSimilarTypes.add(FilterToolbarFactory.createFilterToolBar(elementTypeConstraint), BorderLayout.NORTH);

        return jpSimilarTypes;
    }

    /**
     * @return the model
     */
    public XsAnalyzerApplicationModel getModel() {
        return model;
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(XsAnalyzerApplicationModel model) {
        this.model = model;
        updateModel();
    }

    private void updateModel() {
        if (null != model) {
            update(model.getSimilarTypes());
        }
    }

    /**
     * @param similarTypes
     */
    private void update(final Map<String, XSObjectList> similarTypes) {
        final SimilarTypesListModelAdapter similarTypesListModelAdapter = new SimilarTypesListModelAdapter(similarTypes);
        final FilteredListModel filteredModel = new FilteredListModel(similarTypesListModelAdapter, elementTypeConstraint);
        masterDetailList.setMasterListModel(filteredModel);
        numSimilarTypesSchema.setValue(similarTypesListModelAdapter.getSize());
    }

    private void invalidate() {
        update(Collections.<String, XSObjectList> emptyMap());
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof XsAnalyzerApplicationEvent) {
            switch (((XsAnalyzerApplicationEvent) event).getEventType()) {
                case SIMILAR_TYPES:
                    XsAnalyzerApplicationModel appmodel = (XsAnalyzerApplicationModel) getApplicationContext().getBean("applicationModel");
                    setModel(appmodel);
                    break;
                case OPEN:
                    invalidate();
                    break;
            }
        }

    }

    private class PropertiesExecutor extends AbstractActionCommandExecutor {

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public void execute() {
            SimilarTypeListModelEntry selectedValue = (SimilarTypeListModelEntry) masterDetailList.getMasterList().getSelectedValue();
            Map<String, XSObject> parameters = new HashMap<String, XSObject>();
            parameters.put(XsElementPropertiesExecutor.OBJECT, selectedValue.getType());
            xsElementPropertiesExecutor.execute(parameters);
        }
    }
}
