// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.xerces.impl.xs.util.XSObjectListImpl;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeUsageWorker extends AbstractXsComponentWorker implements XsComponentWorker {

    /** XRef Map of types and respective users */
    private final Map<XSObject, XSObjectListImpl> usedBy;

    public TypeUsageWorker() {
        super();
        usedBy = new HashMap<XSObject, XSObjectListImpl>();
    }

    /**
     * Returns the xref map of types and users
     * 
     * @return the usedBy
     */
    public Map<XSObject, ? extends XSObjectList> getUsedBy() {
        return Collections.unmodifiableMap(usedBy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#execute(org.apache.xerces .xs.XSObject,
     * org.apache.xerces.xs.XSObject)
     */
    @Override
    public void execute(XSObject object, XSObject parent) {
        final XSTypeDefinition type = XSModelHelper.getBaseType(object);
        if (null == type) { return; }

        addUsedByType(object, type);

    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#isSupported(org.apache. xerces.xs.XSObject)
     */
    @Override
    public boolean isSupported(XSObject object) {
        // TODO Auto-generated method stub
        return true;
    }

    /**
     * Add xref to usedBy map.
     * 
     * @param element
     *            element which is used by <code>type<code>
     * @param type
     *            type definition
     */
    private void addUsedByType(XSObject element, XSTypeDefinition type) {
        // do not add null ;)
        if (null == element) { return; }

        // check for self references
        if (XSModelHelper.isSameTypeDefinition(element, type)) { return; }

        if (usedBy.containsKey(type)) {
            final XSObjectListImpl objectList = usedBy.get(type);
            for (int i = 0; i < objectList.getLength(); ++i) {
                XSObject object = objectList.item(i);
                // check for duplicate types
                if (XSModelHelper.isSameTypeDefinition(object, element)) { return; }
            }
            objectList.add(element);
        }
        else {
            final XSObjectListImpl xsoList = new XSObjectListImpl();
            xsoList.add(element);
            usedBy.put(type, xsoList);
        }
    }

}
