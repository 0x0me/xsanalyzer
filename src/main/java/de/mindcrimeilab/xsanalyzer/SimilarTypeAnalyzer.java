// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008,2009,2010 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSObjectList;
import org.springframework.context.MessageSource;
import org.springframework.richclient.application.Application;

import de.mindcrimeilab.xsanalyzer.actions.EventType;
import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SimilarTypeAnalyzer extends AbstractAnalyzer implements Analyzer {

    private final SimilarTypeFinderWorker similarTypeFinder;

    private final Collection<XsComponentWorker> workers;

    /**
     * 
     */
    public SimilarTypeAnalyzer() {
        similarTypeFinder = new SimilarTypeFinderWorker();
        workers = new LinkedList<XsComponentWorker>();
        workers.add(similarTypeFinder);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#getWorkers()
     */
    @Override
    public Collection<XsComponentWorker> getWorkers() {
        return workers;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#analyze(de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel)
     */
    @Override
    public void analyze(XsAnalyzerApplicationModel model) {
        final XSModel xsmodel = model.getSchemaModel();
        final XsModelWalker walker = new XsModelWalker();
        walker.addPropertyChangeListener(getProgessListener());
        walker.addWorker(similarTypeFinder);
        walker.walkModel(xsmodel);

        this.setResult(model);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#showResult()
     */
    @Override
    public void showResult() {
        Application.instance().getActiveWindow().getPage().showView("similarTypesView");
        Application.instance().getApplicationContext().publishEvent(new XsAnalyzerApplicationEvent(EventType.SIMILAR_TYPES, this));
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#setResult(de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel)
     */
    @Override
    public void setResult(XsAnalyzerApplicationModel model) {
        model.setSimilarTypes((Map<String, XSObjectList>) similarTypeFinder.getSimilarTypes());
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#setup(de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel)
     */
    @Override
    public void setup(XsAnalyzerApplicationModel model) {
        // no setup needed.
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#getLabelProvider()
     */
    @Override
    public LabelProvider getLabelProvider() {
        return new SimilarTypeLabelProvider();
    }

    private final static class SimilarTypeLabelProvider implements LabelProvider {

        private static final MessageSource MESSAGE_SOURCE = (MessageSource) Application.services().getService(MessageSource.class);

        private static String getMessage(String code) {
            return SimilarTypeLabelProvider.MESSAGE_SOURCE.getMessage(code, null, code, Locale.getDefault());
        }

        /*
         * (non-Javadoc)
         * 
         * @see de.mindcrimeilab.xsanalyzer.LabelProvider#getLabel()
         */
        @Override
        public String getLabel() {
            return getMessage("similarTypeFinderCommand.label");
        }

        /*
         * (non-Javadoc)
         * 
         * @see de.mindcrimeilab.xsanalyzer.LabelProvider#getCaption()
         */
        @Override
        public String getCaption() {
            return getMessage("similarTypeFinderCommand.caption");
        }

        /*
         * (non-Javadoc)
         * 
         * @see de.mindcrimeilab.xsanalyzer.LabelProvider#getDescription()
         */
        @Override
        public String getDescription() {
            return getMessage("similarTypeFinderCommand.description");
        }
    }

}
