// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import javax.swing.JComponent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSObject;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.richclient.application.support.AbstractView;

import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.ui.panels.AnnotationPanel;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class AnnotationToolWindow extends AbstractView implements ApplicationListener {

    /** logger */
    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private final AnnotationPanel annotationPanel;

    /**
     * ctor()
     */
    public AnnotationToolWindow() {
        this.annotationPanel = new AnnotationPanel();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.application.support.AbstractView#createControl()
     */
    @Override
    protected JComponent createControl() {
        return annotationPanel.getPanel();
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        logger.debug(event);
        if (event instanceof XsAnalyzerApplicationEvent) {
            switch (((XsAnalyzerApplicationEvent) event).getEventType()) {
                case SHOW_SCHEMA_ELEMENT_DETAIL:
                    Object obj = event.getSource();
                    if (obj instanceof XSObject) {
                        update((XSObject) obj);
                    }
                    break;
            }
        }
    }

    /**
     * update content view
     * 
     * @param xso
     *            object to extract annotations from
     */
    private void update(XSObject xso) {
        this.annotationPanel.update(xso);
    }
}
