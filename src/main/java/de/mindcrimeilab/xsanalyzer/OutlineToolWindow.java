// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.apache.xerces.xs.XSObject;
import org.springframework.binding.value.ValueModel;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.application.PageComponentContext;
import org.springframework.richclient.application.statusbar.StatusBar;
import org.springframework.richclient.application.support.AbstractView;
import org.springframework.richclient.command.CommandGroup;
import org.springframework.richclient.command.support.GlobalCommandIds;
import org.springframework.richclient.util.PopupMenuMouseListener;

import de.mindcrimeilab.util.DefaultMutableTreeNodeAccessor;
import de.mindcrimeilab.xsanalyzer.actions.TypeInspectionCommand;
import de.mindcrimeilab.xsanalyzer.actions.TypeHierarchyCommand;
import de.mindcrimeilab.xsanalyzer.actions.TypeHierarchyExecutor;
import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.actions.XsElementPropertiesExecutor;
import de.mindcrimeilab.xsanalyzer.model.JTreeValueModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.TreeModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.TreeSelectionValueModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.TreeSingleSelectionGuard;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.ui.renderer.SchemaElementsRenderer;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class OutlineToolWindow extends AbstractView implements ApplicationListener {

    private JTree jtStructure;

    private XsAnalyzerApplicationModel model;

    private final XsElementPropertiesExecutor xsElementPropertiesExecutor = new XsElementPropertiesExecutor();

    private final TypeHierarchyExecutor typeHierarchyExecutor = new TypeHierarchyExecutor();

    @Override
    protected void registerLocalCommandExecutors(PageComponentContext context) {
        context.register(GlobalCommandIds.PROPERTIES, xsElementPropertiesExecutor);
    }

    /**
     * @return the model
     */
    public XsAnalyzerApplicationModel getModel() {
        return model;
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(XsAnalyzerApplicationModel model) {
        this.model = model;
        if (null != this.model.getSchemaModel() && null != jtStructure) {
            jtStructure.setModel(new TreeModelAdapter(this.model.getSchemaModel()));
        }
    }

    /*
     * Create the actual UI control for this view. It will be placed into the window according to the layout of the page
     * holding this view.
     * 
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.application.support.AbstractView#createControl ()
     */
    @Override
    protected JComponent createControl() {
        createTree();
        JPanel jpOutline = getComponentFactory().createPanel(new BorderLayout());
        jpOutline.add(getComponentFactory().createScrollPane(jtStructure, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);
        return jpOutline;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        logger.debug(event);
        if (event instanceof XsAnalyzerApplicationEvent) {
            switch (((XsAnalyzerApplicationEvent) event).getEventType()) {
                case OPEN:
                    jtStructure.setModel(new TreeModelAdapter(model.getSchemaModel()));
                    StatusBar bar = Application.instance().getLifecycleAdvisor().getStatusBar();
                    bar.setMessage(getMessage("statusBar.label.action.open", new Object[] { model.getSchemaFile().getAbsolutePath()}));
                    break;
            }
        }
    }

    private void createTree() {
        jtStructure = new JTree(new DefaultMutableTreeNode(getMessage("outlineView.structureTree.initialMessage")));
        jtStructure.setCellRenderer(new SchemaElementsRenderer());

        final ValueModel selectionHolder = new TreeSelectionValueModelAdapter(jtStructure.getSelectionModel());
        final ValueModel vModel = new JTreeValueModelAdapter((TreeSelectionValueModelAdapter) selectionHolder);

        final DefaultMutableTreeNodeAccessor accessor = new DefaultMutableTreeNodeAccessor();
        final CommandGroup group = getWindowCommandManager().createCommandGroup("schemaElementsCommandGroup", new Object[] { new TypeHierarchyCommand(vModel), new TypeInspectionCommand(vModel), "separator", "propertiesCommand"});

        jtStructure.addMouseListener(new PopupMenuMouseListener() {

            @Override
            protected JPopupMenu getPopupMenu(MouseEvent e) {
                return group.createPopupMenu();
            }
        });
        jtStructure.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    final Object selectedObject = jtStructure.getLastSelectedPathComponent();
                    if (selectedObject instanceof XSObject) {
                        Map<String, XSObject> parameters = new HashMap<String, XSObject>();
                        parameters.put(TypeHierarchyExecutor.TARGET_OBJECT, (XSObject) selectedObject);
                        typeHierarchyExecutor.execute(parameters);
                    }
                }
            }
        });

        jtStructure.addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent e) {
                TreePath path = e.getPath();
                if (null != path) {
                    Map<String, XSObject> parameters = new HashMap<String, XSObject>();

                    final Object lastPathComponent = path.getLastPathComponent();
                    if (lastPathComponent instanceof XSObject) {
                        parameters.put(XsElementPropertiesExecutor.OBJECT, (XSObject) lastPathComponent);
                        xsElementPropertiesExecutor.execute(parameters);
                    }
                }

            }

        });

        new TreeSingleSelectionGuard(selectionHolder, xsElementPropertiesExecutor);
        new TreeSingleSelectionGuard(selectionHolder, typeHierarchyExecutor);
        // new XSObjectTreeSingleSelectionGuard(selectionHolder, (Guarded)
        // getWindowCommandManager().getCommand("testCommand"));
    }
}
