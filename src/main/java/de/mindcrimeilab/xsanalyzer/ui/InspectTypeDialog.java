// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import org.apache.xerces.xs.XSObject;
import org.springframework.richclient.command.AbstractCommand;
import org.springframework.richclient.dialog.CloseAction;
import org.springframework.richclient.dialog.TitledApplicationDialog;
import org.springframework.richclient.factory.ComponentFactory;
import org.springframework.richclient.layout.GridBagLayoutBuilder;

import de.mindcrimeilab.xsanalyzer.ui.panels.AnnotationPanel;
import de.mindcrimeilab.xsanalyzer.ui.panels.TypeHierarchyPanel;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class InspectTypeDialog extends TitledApplicationDialog {

    private final XSObject type;

    private final AnnotationPanel annotationPanel;

    public InspectTypeDialog(XSObject type) {
        super();
        setTitle(getMessage("inspectType.title"));
        setTitlePaneTitle(getMessage("inspectType.label"));
        setDescription(getMessage("inspectType.description", new Object[] { type.getName()}));
        setResizable(false);
        setCloseAction(CloseAction.DISPOSE);
        this.type = type;
        this.annotationPanel = new AnnotationPanel();
        this.annotationPanel.update(type);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.dialog.ApplicationDialog#onFinish()
     */
    @Override
    protected boolean onFinish() {
        return true;
    }

    @Override
    protected JComponent createTitledDialogContentPane() {
        final ComponentFactory cf = getComponentFactory();
        JTabbedPane jtb = cf.createTabbedPane();

        final TypeHierarchyPanel typeHierarchyPanel = new TypeHierarchyPanel();
        typeHierarchyPanel.setType(type);

        GridBagLayoutBuilder builder = new GridBagLayoutBuilder();

        final JTextField jtfLocalName = cf.createTextField();
        jtfLocalName.setText(type.getName());
        jtfLocalName.setEditable(false);

        final JTextField jtfNamespace = cf.createTextField();
        jtfNamespace.setText(type.getNamespace());
        jtfNamespace.setEditable(false);

        builder.append(new JLabel("Local name:")).append(jtfLocalName, 1, 1, true, false).nextLine();
        builder.append(new JLabel("Namespace:")).append(jtfNamespace, 1, 1, true, false).nextLine();
        builder.append(Box.createVerticalStrut(10), 2, 1, true, true);

        jtb.add("General", builder.getPanel());
        jtb.add("Type hierarchy", typeHierarchyPanel);
        jtb.add("Annotations", annotationPanel.getPanel());

        return jtb;
    }

    @Override
    protected Object[] getCommandGroupMembers() {
        return new AbstractCommand[] { getFinishCommand()};
    }

}
