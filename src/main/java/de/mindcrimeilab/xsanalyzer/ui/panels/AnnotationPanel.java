// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.panels;

import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.swing.Box;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ListCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSAnnotation;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.richclient.application.ApplicationServicesLocator;
import org.springframework.richclient.factory.ComponentFactory;
import org.springframework.richclient.layout.GridBagLayoutBuilder;
import org.springframework.richclient.list.ComboBoxListModel;

import de.mindcrimeilab.xsanalyzer.xml.AnnotationContentHandler;
import de.mindcrimeilab.xsanalyzer.xml.XmlSchemaAnnotation;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class AnnotationPanel {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    /** content view to display annotation contents */
    private JTextPane jetDocumentation;

    /** combobox to select desired annotation because there might be several annotations per selected element */
    private JComboBox jcbAnnotations;

    /** combobox model */
    private final ComboBoxListModel annotationsListModel;

    /** checkbox to enable/disable html view */
    private JCheckBox jcbEnableHtml;

    private static String NO_ANNOTATIONS = "No annotations";

    public AnnotationPanel() {
        NO_ANNOTATIONS = getMessage("annotationToolWindow.annotations.noAnnotation");
        this.annotationsListModel = new ComboBoxListModel(Arrays.asList(new String[] { NO_ANNOTATIONS}));
        createDocumentationEditorPane();
        createJcbAnnotations();
        createJcbEnableHtml();
        setupListeners();
    }

    public JPanel getPanel() {
        ComponentFactory cf = getComponentFactory();
        JLabel label = cf.createLabelFor(getMessage("annotationToolWindow.annotations.comboBox.label"), this.jcbAnnotations);
        GridBagLayoutBuilder builder = new GridBagLayoutBuilder();
        builder.setAutoSpanLastComponent(false);
        builder.setDefaultInsets(new Insets(5, 5, 5, 5));
        builder.appendLabel(label).append(this.jcbAnnotations, 1, 1, false, false).append(Box.createHorizontalStrut(10), 1, 1, true, false).append(jcbEnableHtml, 1, 1, false, false, new Insets(5, 5, 5, 5)).nextLine();
        builder.append(cf.createScrollPane(jetDocumentation, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), 4, 1, true, true).nextLine();

        return builder.getPanel();
    }

    /**
     * @return
     */
    private ComponentFactory getComponentFactory() {
        return (ComponentFactory) ApplicationServicesLocator.services().getService(ComponentFactory.class);
    }

    public String getMessage(String messageCode) {
        return ((MessageSourceAccessor) ApplicationServicesLocator.services().getService(MessageSourceAccessor.class)).getMessage(messageCode, null, messageCode, Locale.getDefault());
    }

    /**
     * update content view
     * 
     * @param xso
     *            object to extract annotations from
     */
    public void update(XSObject xso) {
        try {
            Method method = xso.getClass().getMethod("getAnnotations", new Class[] {});
            XSObjectList annotations = (XSObjectList) method.invoke(xso, new Object[] {});
            setAnnotations(annotations);
        }
        catch (Exception e) {
            logger.info("No annotations found.", e);
        }
    }

    /**
     * update annotations view
     * 
     * @param annotations
     *            to display
     */
    private void setAnnotations(XSObjectList annotations) {
        // no annotations at all - clear view
        if (null == annotations || annotations.getLength() < 1) {
            this.annotationsListModel.clear();
            this.annotationsListModel.add(NO_ANNOTATIONS);
            return;
        }
        // update and display annotations
        for (int i = 0; i < annotations.getLength(); ++i) {
            XSAnnotation annotation = (XSAnnotation) annotations.item(i);
            logger.debug("Annotation #" + i + " [" + annotation + "]");
            logger.debug("Annotation #" + i + " type=[" + annotation.getType() + "], value=[" + annotation.getAnnotationString() + "]");
            AnnotationContentHandler contentHandler = new AnnotationContentHandler();
            annotation.writeAnnotation(contentHandler, XSAnnotation.SAX_CONTENTHANDLER);

            List<XmlSchemaAnnotation> annotationsList = contentHandler.getAnnotations();
            this.annotationsListModel.clear();
            if (null == annotationsList || annotationsList.isEmpty()) {
                this.annotationsListModel.add(NO_ANNOTATIONS);
            }
            else {
                this.annotationsListModel.addAll(annotationsList);
            }
        }
    }

    /**
     * initialize annotations combobox
     */
    private final void createJcbAnnotations() {
        this.jcbAnnotations = getComponentFactory().createComboBox();
        this.jcbAnnotations.setModel(this.annotationsListModel);
        this.jcbAnnotations.setRenderer(new AnnotationsListCellRenderer());
        this.jcbAnnotations.setEnabled(false);
    }

    /**
     * initialize html view checkbox
     */
    private final void createJcbEnableHtml() {
        this.jcbEnableHtml = new JCheckBox(getMessage("annotationToolWindow.annotations.enableHtmlCheckbox.label"));
        this.jcbEnableHtml.setSelected(false);
        this.jcbEnableHtml.setEnabled(false);
    }

    /**
     * initialize annotations view
     */
    private final void createDocumentationEditorPane() {
        this.jetDocumentation = new JTextPane();
        jetDocumentation.setEditable(false);
        jetDocumentation.setText(NO_ANNOTATIONS);
        jetDocumentation.setEnabled(false);
    }

    /**
     * setup listeners for internal components
     */
    private final void setupListeners() {
        assert null != this.jetDocumentation;
        assert null != this.jcbAnnotations;
        assert null != this.jcbEnableHtml;
        this.jcbAnnotations.addActionListener(new SelectAnnotationsActionListener(this.jetDocumentation, this.jcbEnableHtml));
        this.jcbEnableHtml.addActionListener(new SwitchHtmlViewActionListener(this.jetDocumentation, this.jcbAnnotations));
    }

    /**
     * Internal listener handling selection events of the annotations combox.
     * 
     * @author Michael Engelhardt
     * @author $Author$
     * @version $Revision$
     */
    private class SelectAnnotationsActionListener implements ActionListener {

        /** content view to update */
        private final JEditorPane editorPane;

        /** switch for html view to update */
        private final JCheckBox checkBox;

        /**
         * ctor()
         * 
         * @param editorPane
         *            to update
         * @param checkBox
         *            to uddate
         */
        SelectAnnotationsActionListener(JEditorPane editorPane, JCheckBox checkBox) {
            this.editorPane = editorPane;
            this.checkBox = checkBox;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            JComboBox comboBox = (JComboBox) evt.getSource();
            Object item = comboBox.getSelectedItem();
            final String text;
            if (item instanceof XmlSchemaAnnotation) {
                text = ((XmlSchemaAnnotation) item).getContent();
                comboBox.setEnabled(true);
            }
            else {
                text = NO_ANNOTATIONS;

                comboBox.setEnabled(false);
            }
            editorPane.setText(text);
            editorPane.setEnabled(comboBox.isEnabled());
            checkBox.setEnabled(comboBox.isEnabled());
        }

    }

    /**
     * Internal listener handling events of the enable/disable html view checkbox. For some reason the combox have to be
     * reselected again in order to trigger propper update of the content view.
     * 
     * @author Michael Engelhardt
     * @author $Author$
     * @version $Revision$
     */
    private class SwitchHtmlViewActionListener implements ActionListener {

        /** content view to update */
        private final JEditorPane editorPane;

        /** combox to perform callback on */
        private final JComboBox comboBox;

        /**
         * ctor();
         * 
         * @param editorPane
         *            to update
         * @param comboBox
         *            to update
         */
        SwitchHtmlViewActionListener(JEditorPane editorPane, JComboBox comboBox) {
            this.editorPane = editorPane;
            this.comboBox = comboBox;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            JCheckBox checkBox = (JCheckBox) evt.getSource();
            if (checkBox.isSelected()) {
                editorPane.setContentType("text/html");
            }
            else {
                editorPane.setContentType("text/plain");
            }
            // trigger update of content view
            comboBox.setSelectedIndex(jcbAnnotations.getSelectedIndex());
        }

    }

    /**
     * Renderer used by the annotations combox box.
     * 
     * @author Michael Engelhardt
     * @author $Author$
     * @version $Revision$
     */
    private class AnnotationsListCellRenderer extends DefaultListCellRenderer implements ListCellRenderer {

        /**
         * serial version uid
         */
        private static final long serialVersionUID = -1950469130279232642L;

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JLabel cell = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof XmlSchemaAnnotation) {
                switch (((XmlSchemaAnnotation) value).getType()) {
                    case APPINFO:
                        cell.setText("appinfo");
                        break;
                    case DOCUMENTATION:
                        cell.setText("documentation");
                        break;
                    default:
                        cell.setText("ERROR: unknown type");
                }
            }
            return cell;
        }

    }
}
