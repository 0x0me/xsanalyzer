// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.panels;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.tree.TreeModel;

import org.apache.xerces.xs.XSObject;

import de.mindcrimeilab.swing.util.JTreeHelper;
import de.mindcrimeilab.xsanalyzer.model.TypeHierarchyTreeModelAdapter;
import de.mindcrimeilab.xsanalyzer.ui.renderer.SchemaElementsRenderer;

/**
 * Type hierarchy panel displays the type hierarchy (extension / restriction) for a given type.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeHierarchyPanel extends JPanel {

    /**
     * serial version UID
     */
    private static final long serialVersionUID = -131361261198571107L;

    /**
     * Type hierarchy tree
     */
    private JTree jtTypeHierarchy;

    /**
     * ctor() Construct new type hierarchy panel
     */
    public TypeHierarchyPanel() {
        super(new BorderLayout());
        initializeGui();
    }

    /**
     * Initialize GUI
     */
    private final void initializeGui() {
        jtTypeHierarchy = new JTree();
        jtTypeHierarchy.setCellRenderer(new SchemaElementsRenderer());
        add(new JScrollPane(jtTypeHierarchy, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);
    }

    /**
     * Get type for which the hierarchy was created.
     * 
     * @return
     */
    public XSObject getType() {
        final TreeModel model = jtTypeHierarchy.getModel();
        Object node = null;
        if (null != model) {
            node = model.getRoot();
            while (!model.isLeaf(node)) {
                node = model.getChild(node, model.getChildCount(node) - 1);
            }
        }
        return (XSObject) node;
    }

    /**
     * Set type
     * 
     * @param type
     */
    public void setType(XSObject type) {
        TreeModel model = jtTypeHierarchy.getModel();
        model = null;
        jtTypeHierarchy.setModel(new TypeHierarchyTreeModelAdapter(type));
        JTreeHelper.expandToLast(jtTypeHierarchy);
    }
}
