// $Id:AttributeRenderer.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.renderer;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

import org.apache.xerces.xs.XSAttributeDeclaration;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.image.IconSource;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class AttributeRenderer implements TreeCellRenderer, ListCellRenderer {

    private final static Icon icon = ((IconSource) Application.services().getService(IconSource.class)).getIcon("attribute.icon");

    private final TreeCellRenderer treeCellRendererDelegate = new DefaultTreeCellRenderer();

    private final ListCellRenderer listCellRendererDelegate = new DefaultListCellRenderer();

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeCellRenderer#getTreeCellRendererComponent(javax. swing.JTree, java.lang.Object,
     * boolean, boolean, boolean, int, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        JLabel cell = (JLabel) treeCellRendererDelegate.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        AttributeRenderer.renderAttributeType(value, cell);

        return cell; // TODO Auto-generated method stub
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing .JList, java.lang.Object, int,
     * boolean, boolean)
     */
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

        JLabel cell = (JLabel) listCellRendererDelegate.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        AttributeRenderer.renderAttributeType(value, cell);

        return cell;
    }

    /**
     * @param value
     * @param cell
     */
    private static void renderAttributeType(final Object value, JLabel cell) {
        if (value instanceof XSAttributeDeclaration) {
            XSAttributeDeclaration attribute = (XSAttributeDeclaration) value;
            StringBuilder sb = new StringBuilder();
            sb.append(attribute.getName());
            sb.append(", <");
            final String namespace = attribute.getNamespace();
            sb.append((null == namespace) ? "default namespace" : namespace);
            sb.append(">");
            cell.setText(sb.toString());
            cell.setIcon(AttributeRenderer.icon);
        }
    }

}
