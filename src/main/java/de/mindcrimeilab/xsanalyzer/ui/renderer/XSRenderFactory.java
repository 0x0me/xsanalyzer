// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.renderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSTypeDefinition;

import com.l2fprod.common.propertysheet.PropertyRendererFactory;
import com.l2fprod.common.propertysheet.PropertyRendererRegistry;
import com.l2fprod.common.propertysheet.PropertySheetTableModel;
import com.l2fprod.common.propertysheet.PropertySheetTableModel.Item;

/**
 * Renderer for xs properties in property sheet view.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XSRenderFactory extends PropertyRendererRegistry implements PropertyRendererFactory {

    /**
     * ctor
     */
    public XSRenderFactory() {
        super();
        this.registerRenderer(XSTypeDefinition.class, getXSTypeDefinitionRenderer());
        this.registerRenderer(short.class, getXSShortRenderer());
        this.registerRenderer(StringList.class, getXSStringListRenderer());
    }

    /**
     * Return renderer for XSTypeDefinition properties.
     * 
     * @return
     */
    private XSTypeDefinitionRenderer getXSTypeDefinitionRenderer() {
        return new XSTypeDefinitionRenderer();
    }

    /**
     * Return renderer for short properties.
     * 
     * @return
     */
    private XSShortRenderer getXSShortRenderer() {
        return new XSShortRenderer();
    }

    /**
     * Return renderer for StringList properties
     * 
     * @return
     */
    private XSStringListRenderer getXSStringListRenderer() {
        return new XSStringListRenderer();
    }

    /**
     * 
     * @author Michael Engelhardt<me@mindcrime-ilab.de>
     * @author $Author$
     * @version $Revision$
     * 
     */
    private class XSShortRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            // TODO Auto-generated method stub
            final JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            PropertySheetTableModel tableModel = (PropertySheetTableModel) table.getModel();

            Item item = tableModel.getPropertySheetElement(row);

            String name = item.getName();

            if ("contentType".equals(name)) {
                short id = (Short) value;
                switch (id) {
                    case XSComplexTypeDefinition.CONTENTTYPE_ELEMENT:
                        cell.setText("element");
                        break;
                    case XSComplexTypeDefinition.CONTENTTYPE_EMPTY:
                        cell.setText("empty");
                        break;
                    case XSComplexTypeDefinition.CONTENTTYPE_MIXED:
                        cell.setText("mixed");
                        break;
                    case XSComplexTypeDefinition.CONTENTTYPE_SIMPLE:
                        cell.setText("simple");
                        break;

                }
            }
            else if ("derivationMethod".equals(name)) {
                short id = (Short) value;
                switch (id) {
                    case XSConstants.DERIVATION_EXTENSION:
                        cell.setText("extension");
                        break;
                    case XSConstants.DERIVATION_LIST:
                        cell.setText("list");
                        break;
                    case XSConstants.DERIVATION_NONE:
                        cell.setText("none");
                        break;
                    case XSConstants.DERIVATION_RESTRICTION:
                        cell.setText("restriction");
                        break;
                    case XSConstants.DERIVATION_SUBSTITUTION:
                        cell.setText("substitution");
                        break;
                    case XSConstants.DERIVATION_UNION:
                        cell.setText("union");
                        break;
                }
            }
            return cell;
        }

    }

    /**
     * 
     * @author Michael Engelhardt<me@mindcrime-ilab.de>
     * @author $Author$
     * @version $Revision$
     * 
     */
    private class XSTypeDefinitionRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            // TODO Auto-generated method stub
            final JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            if (value instanceof XSTypeDefinition) {
                XSTypeDefinition def = (XSTypeDefinition) value;
                cell.setText("{" + def.getNamespace() + "}:" + def.getName());
            }

            return cell;
        }
    }

    /**
     * 
     * @author Michael Engelhardt<me@mindcrime-ilab.de>
     * @author $Author$
     * @version $Revision$
     * 
     */
    private class XSStringListRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            final JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            if (value instanceof StringList) {
                StringList def = (StringList) value;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < def.getLength(); ++i) {
                    sb.append(def.item(i));
                    if (i < (def.getLength() - 1)) {
                        sb.append(", ");
                    }
                }
                cell.setText(sb.toString());
            }

            return cell;
        }
    }
}
