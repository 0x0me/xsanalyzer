// $Id:ElementRenderer.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.renderer;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

import org.apache.xerces.xs.XSElementDeclaration;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.image.IconSource;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class ElementRenderer implements TreeCellRenderer {

    private final static Icon icon = ((IconSource) Application.services().getService(IconSource.class)).getIcon("element.icon");

    private final TreeCellRenderer delegateTreeCellRenderer = new DefaultTreeCellRenderer();

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeCellRenderer#getTreeCellRendererComponent(javax. swing.JTree, java.lang.Object,
     * boolean, boolean, boolean, int, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        JLabel cell = (JLabel) delegateTreeCellRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        if (value instanceof XSElementDeclaration) {
            XSElementDeclaration xselem = (XSElementDeclaration) value;
            StringBuilder sb = new StringBuilder();
            sb.append(xselem.getName());
            sb.append(":");
            sb.append(xselem.getTypeDefinition().getName());
            sb.append(", <");
            sb.append(xselem.getNamespace());
            sb.append(">");
            cell.setText(sb.toString());
            cell.setIcon(ElementRenderer.icon);
        }

        return cell;
    }

}
