// $Id:SchemaElementsRenderer.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.renderer;

import java.awt.Component;
import java.util.Locale;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;

import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSModelGroupDefinition;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.springframework.context.MessageSource;
import org.springframework.richclient.application.Application;

import de.mindcrimeilab.xsanalyzer.model.TreeModelAdapter;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class SchemaElementsRenderer implements TreeCellRenderer, ListCellRenderer {

    private final ComplexTypeCellRenderer tcrComplexTypes = new ComplexTypeCellRenderer();

    private final SimpleTypeCellRenderer tcrSimpleTypes = new SimpleTypeCellRenderer();

    private final ElementRenderer tcrElements = new ElementRenderer();

    private final AttributeRenderer tcrAttributes = new AttributeRenderer();

    private final GroupCellRenderer tcrModelGroups = new GroupCellRenderer();

    private final ListCellRenderer lcrParticle = new XSParticleRenderer();

    private final TreeCellRenderer tcrDefault = new DefaultTreeCellRenderer();

    private final ListCellRenderer lcrDefault = new DefaultListCellRenderer();

    private static final MessageSource MESSAGE_SOURCE = (MessageSource) Application.services().getService(MessageSource.class);

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean,
     * boolean, boolean, int, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        JLabel cell = null;
        if (value instanceof XSComplexTypeDefinition) {
            cell = (JLabel) tcrComplexTypes.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }
        else if (value instanceof XSElementDeclaration) {
            cell = (JLabel) tcrElements.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }
        else if (value instanceof XSSimpleTypeDefinition) {
            cell = (JLabel) tcrSimpleTypes.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }
        else if (value instanceof XSModelGroupDefinition) {
            cell = (JLabel) tcrModelGroups.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }
        else if (value instanceof XSAttributeDeclaration) {
            cell = (JLabel) tcrAttributes.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }
        else if (value instanceof DefaultMutableTreeNode) {
            cell = (JLabel) getTreeCellRendererComponent(tree, ((DefaultMutableTreeNode) value).getUserObject(), selected, expanded, leaf, row, hasFocus);
        }
        else if (value instanceof String) {
            // @TODO re-think
            final TreeModel model = tree.getModel();
            final String message;
            if (model instanceof TreeModelAdapter) {
                message = SchemaElementsRenderer.getMessage((String) value, new Object[] { model.getChildCount(value)});
            }
            else {
                message = SchemaElementsRenderer.getMessage((String) value);
            }
            cell = (JLabel) tcrDefault.getTreeCellRendererComponent(tree, message, selected, expanded, leaf, row, hasFocus);
        }
        else {
            cell = (JLabel) tcrDefault.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }

        return cell;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel cell = null;
        if (value instanceof XSComplexTypeDefinition) {
            cell = (JLabel) tcrComplexTypes.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            // } else if (value instanceof XSElementDeclaration) {
            // cell = (JLabel) tcrElements.getListCellRendererComponent(list,
            // value, index, isSelected, cellHasFocus);
        }
        else if (value instanceof XSSimpleTypeDefinition) {
            cell = (JLabel) tcrSimpleTypes.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            // } else if (value instanceof XSModelGroupDefinition) {
            // cell = (JLabel) tcrModelGroups.getListCellRendererComponent(list,
            // value, index, isSelected, cellHasFocus);
        }
        else if (value instanceof XSAttributeDeclaration) {
            cell = (JLabel) tcrAttributes.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
        else if (value instanceof XSAttributeDeclaration) {
            cell = (JLabel) lcrParticle.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
        else {
            cell = (JLabel) lcrDefault.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }

        return cell;
    }

    private static String getMessage(String code) {
        return SchemaElementsRenderer.MESSAGE_SOURCE.getMessage(code, null, code, Locale.getDefault());
    }

    private static String getMessage(String code, Object[] args) {
        return SchemaElementsRenderer.MESSAGE_SOURCE.getMessage(code, args, code, Locale.getDefault());
    }
}
