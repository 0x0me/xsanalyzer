// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.renderer;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import de.mindcrimeilab.xsanalyzer.model.SimilarTypeListModelEntry;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SimilarTypeRenderer extends DefaultListCellRenderer implements ListCellRenderer {

    private final ListCellRenderer renderDelegate;

    public SimilarTypeRenderer() {
        renderDelegate = new SchemaElementsRenderer();
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        final JLabel cell;

        if (value instanceof SimilarTypeListModelEntry) {
            SimilarTypeListModelEntry entry = (SimilarTypeListModelEntry) value;

            cell = (JLabel) renderDelegate.getListCellRendererComponent(list, entry.getType(), index, isSelected, cellHasFocus);
            cell.setText(cell.getText());
            cell.setToolTipText("Computed similarity md5 sum:" + entry.getTypeSignature() + ".");
        }
        else {
            cell = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
        return cell;
    }
}
