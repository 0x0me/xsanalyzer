// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.ScrollPaneConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.richclient.command.AbstractCommand;
import org.springframework.richclient.dialog.ApplicationDialog;
import org.springframework.richclient.dialog.CloseAction;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class LicenseDialog extends ApplicationDialog {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private final JEditorPane jepLicenseText = new JEditorPane();

    public LicenseDialog() {
        super();
        setCloseAction(CloseAction.DISPOSE);
    }

    @Override
    protected void onInitialized() {
        setTitle(getMessage("licenseDialog.title"));
        try {
            jepLicenseText.setPage(this.getClass().getClassLoader().getResource("license.txt"));
        }
        catch (IOException e) {
            LicenseDialog.logger.error("Could not load license for reason: " + e.getMessage(), e);
            StringWriter sw = new StringWriter();
            PrintWriter pt = new PrintWriter(sw);
            e.printStackTrace(pt);
            jepLicenseText.setText(sw.toString());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.dialog.ApplicationDialog#createDialogContentPane()
     */
    @Override
    protected JComponent createDialogContentPane() {
        JPanel contentPane = getComponentFactory().createPanel(new BorderLayout());
        contentPane.setPreferredSize(new Dimension(600, 450));
        contentPane.setBorder(BorderFactory.createEmptyBorder(10, 20, 20, 20));

        jepLicenseText.setEditable(false);
        jepLicenseText.setMaximumSize(contentPane.getSize());
        contentPane.add(getComponentFactory().createScrollPane(jepLicenseText, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED));

        return contentPane;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.dialog.ApplicationDialog#onFinish()
     */
    @Override
    protected boolean onFinish() {
        return true;
    }

    @Override
    protected Object[] getCommandGroupMembers() {
        return new AbstractCommand[] { getFinishCommand()};
    }
}
