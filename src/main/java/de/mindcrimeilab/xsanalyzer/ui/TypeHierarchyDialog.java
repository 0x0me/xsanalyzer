// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui;

import javax.swing.JComponent;

import org.apache.xerces.xs.XSObject;
import org.springframework.richclient.command.AbstractCommand;
import org.springframework.richclient.dialog.CloseAction;
import org.springframework.richclient.dialog.TitledApplicationDialog;

import de.mindcrimeilab.xsanalyzer.ui.panels.TypeHierarchyPanel;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeHierarchyDialog extends TitledApplicationDialog {

    private final XSObject type;

    public TypeHierarchyDialog(XSObject type) {
        super();
        setTitle(getMessage("typeHierarchy.title"));
        setTitlePaneTitle(getMessage("typeHierarchy.label"));
        setDescription(getMessage("typeHierarchy.description", new Object[] { type.getName()}));
        setResizable(false);
        setCloseAction(CloseAction.DISPOSE);
        this.type = type;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.dialog.ApplicationDialog#onFinish()
     */
    @Override
    protected boolean onFinish() {
        return true;
    }

    @Override
    protected JComponent createTitledDialogContentPane() {
        final TypeHierarchyPanel contentPane = new TypeHierarchyPanel();
        contentPane.setType(type);
        return contentPane;
    }

    @Override
    protected Object[] getCommandGroupMembers() {
        return new AbstractCommand[] { getFinishCommand()};
    }

}
