// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.support.mydoggy;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.richclient.application.ApplicationPage;
import org.springframework.richclient.application.ApplicationPageFactory;
import org.springframework.richclient.application.ApplicationWindow;
import org.springframework.richclient.application.PageDescriptor;
import org.springframework.richclient.application.PageListener;

/**
 * Credits go to Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net for the mydoggy swing-rcp integration
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class MyDoggyApplicationPageFactory implements ApplicationPageFactory, InitializingBean {

    private final Log logger = LogFactory.getLog(getClass());

    /*
     * (non-Javadoc)
     * 
     * @seeorg.springframework.richclient.application.ApplicationPageFactory# createApplicationPage
     * (org.springframework.richclient.application.ApplicationWindow,
     * org.springframework.richclient.application.PageDescriptor)
     */
    public ApplicationPage createApplicationPage(ApplicationWindow window, PageDescriptor descriptor) {
        MyDoggyApplicationPage page = new MyDoggyApplicationPage(window, descriptor);

        window.addPageListener(new PageListener() {

            public void pageOpened(ApplicationPage page) {
                // normally this should be called on application start
                MyDoggyApplicationPage myDoggyApplicationPage = (MyDoggyApplicationPage) page;
                // determine wether we can load from file or
                // we should start with only one predefined page
                boolean appliedLayout = false;

                try {
                    if (myDoggyApplicationPage.loadLayout()) {
                        appliedLayout = true;
                    }
                }
                catch (IOException ex) {
                    logger.warn("Failed to restore layout from file!", ex);
                }

                if (!appliedLayout) {
                    myDoggyApplicationPage.buildInitialLayout();
                }
            }

            public void pageClosed(ApplicationPage aPage) {
                // TODO this won't be called?
                // normally this should be called on exit of the application
            }
        });

        return page;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // TODO Auto-generated method stub

    }
}
