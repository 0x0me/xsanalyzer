// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package de.mindcrimeilab.xsanalyzer.ui.filter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JToggleButton;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.mindcrimeilab.xsanalyzer.rules.constraints.SchemaTypeConstraint;

/**
 * Internally used action listener to apply the selected filter operation to the unused types list model.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SchemaTypeConstraintActionListener implements ActionListener {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    /** schema element type the filter applies to */
    private final short type;

    /** constraint to apply */
    private final SchemaTypeConstraint constraint;

    /**
     * ctor()
     * 
     * @param constraint
     * @param type
     */
    public SchemaTypeConstraintActionListener(SchemaTypeConstraint constraint, short type) {
        super();
        this.type = type;
        this.constraint = constraint;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        JToggleButton toogleButton = (JToggleButton) evt.getSource();
        SchemaTypeConstraintActionListener.logger.debug("Filter complex types  [" + toogleButton.isSelected() + "]");
        constraint.setFilter(type, toogleButton.isSelected());
    }
}
