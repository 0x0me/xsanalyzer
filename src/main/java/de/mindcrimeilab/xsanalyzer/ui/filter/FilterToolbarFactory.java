// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui.filter;

import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSTypeDefinition;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.image.IconSource;

import de.mindcrimeilab.xsanalyzer.rules.constraints.SchemaTypeConstraint;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class FilterToolbarFactory {

    private FilterToolbarFactory() {
    };

    public static JToolBar createFilterToolBar(final SchemaTypeConstraint[] constraints) {
        final IconSource iconSource = (IconSource) Application.services().getService(IconSource.class);
        final JToggleButton jtbAttributes = new JToggleButton(iconSource.getIcon("attributeHide.icon"));
        jtbAttributes.setSelected(false);
        jtbAttributes.setPressedIcon(iconSource.getIcon("attribute.icon"));
        jtbAttributes.setToolTipText("Hide/show  attributes.");

        final JToggleButton jtbComplexTypes = new JToggleButton(iconSource.getIcon("complexTypeHide.icon"));
        jtbComplexTypes.setSelected(false);
        jtbComplexTypes.setPressedIcon(iconSource.getIcon("complexType.icon"));
        jtbComplexTypes.setToolTipText("Hide/show complex types.");

        final JToggleButton jtbElements = new JToggleButton(iconSource.getIcon("elementHide.icon"));
        jtbElements.setSelected(false);
        jtbElements.setPressedIcon(iconSource.getIcon("elementType.icon"));
        jtbElements.setToolTipText("Hide/show elements.");

        final JToggleButton jtbModelGroups = new JToggleButton(iconSource.getIcon("modelGroupHide.icon"));
        jtbModelGroups.setSelected(false);
        jtbModelGroups.setPressedIcon(iconSource.getIcon("modelGroup.icon"));
        jtbModelGroups.setToolTipText("Hide/show model groups.");

        final JToggleButton jtbSimpleTypes = new JToggleButton(iconSource.getIcon("simpleTypeHide.icon"));
        jtbSimpleTypes.setSelected(false);
        jtbSimpleTypes.setPressedIcon(iconSource.getIcon("simpleType.icon"));
        jtbSimpleTypes.setToolTipText("Hide/show simple types.");

        for (SchemaTypeConstraint constraint : constraints) {
            jtbAttributes.addActionListener(new SchemaTypeConstraintActionListener(constraint, XSConstants.ATTRIBUTE_DECLARATION));
            jtbComplexTypes.addActionListener(new SchemaTypeConstraintActionListener(constraint, XSTypeDefinition.COMPLEX_TYPE));
            jtbElements.addActionListener(new SchemaTypeConstraintActionListener(constraint, XSConstants.ELEMENT_DECLARATION));
            jtbModelGroups.addActionListener(new SchemaTypeConstraintActionListener(constraint, XSConstants.MODEL_GROUP_DEFINITION));
            jtbSimpleTypes.addActionListener(new SchemaTypeConstraintActionListener(constraint, XSTypeDefinition.SIMPLE_TYPE));
        }

        final JToolBar toolbar = new JToolBar();
        toolbar.add(jtbAttributes);
        toolbar.add(jtbComplexTypes);
        toolbar.add(jtbElements);
        toolbar.add(jtbModelGroups);
        toolbar.add(jtbSimpleTypes);
        toolbar.setRollover(true);
        toolbar.setBorderPainted(false);
        return toolbar;
    }

    public static JToolBar createFilterToolBar(final SchemaTypeConstraint constraint) {
        return FilterToolbarFactory.createFilterToolBar(new SchemaTypeConstraint[] { constraint});
    }

}
