// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSModel;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.dialog.TitledApplicationDialog;
import org.springframework.richclient.factory.ComponentFactory;
import org.springframework.richclient.layout.GridBagLayoutBuilder;

import de.mindcrimeilab.swing.util.VisualProgressWorker;
import de.mindcrimeilab.xsanalyzer.Analyzer;
import de.mindcrimeilab.xsanalyzer.XsComponentWorker;
import de.mindcrimeilab.xsanalyzer.XsModelWalker;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class RunMultipleDialog extends TitledApplicationDialog {

    private final List<Analyzer> selectedAnalyzers;

    /**
     * 
     */
    public RunMultipleDialog() {
        super();
        this.selectedAnalyzers = new LinkedList<Analyzer>();

        setTitle(getMessage("runMultiple.title"));
        setTitlePaneTitle(getMessage("runMultiple.label"));
        setDescription(getMessage("runMultiple.description"));
        getAvailableAnalyzers();
    }

    public Collection<Analyzer> getSelectedAnalyzers() {
        return Collections.unmodifiableCollection(this.selectedAnalyzers);
    }

    /**
     * 
     */
    private Collection<Analyzer> getAvailableAnalyzers() {
        Map<String, Analyzer> availableAnalyzers = Application.instance().getApplicationContext().getBeansOfType(Analyzer.class);
        logger.debug("Found anlayzers [" + availableAnalyzers + "]");
        return (null == availableAnalyzers) ? Collections.<Analyzer> emptyList() : availableAnalyzers.values();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.dialog.TitledApplicationDialog#createTitledDialogContentPane()
     */
    @Override
    protected JComponent createTitledDialogContentPane() {
        ComponentFactory cf = getComponentFactory();
        GridBagLayoutBuilder builder = new GridBagLayoutBuilder();

        builder.appendLabel(new JLabel(getMessage("runMultiple.analyzerSelectLabel")));
        builder.nextLine();

        for (Analyzer analyzer : getAvailableAnalyzers()) {
            JCheckBox jcb = cf.createCheckBox(analyzer.getLabelProvider().getCaption());
            jcb.addActionListener(new AnalyzerCheckBoxListener(this.selectedAnalyzers, analyzer));
            builder.append(jcb);
            builder.nextLine();
        }

        return builder.getPanel();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.dialog.ApplicationDialog#onFinish()
     */
    @Override
    protected boolean onFinish() {
        logger.debug("Selected analyzer classes [" + this.getSelectedAnalyzers() + "]");
        if (this.getSelectedAnalyzers().isEmpty()) return true;

        final XsAnalyzerApplicationModel model = (XsAnalyzerApplicationModel) Application.instance().getApplicationContext().getBean("applicationModel");

        VisualProgressWorker<Collection<Analyzer>, Void> worker = new VisualProgressWorker<Collection<Analyzer>, Void>(this.getParentWindow()) {

            @Override
            protected Collection<Analyzer> doInBackground() throws Exception {
                XSModel xsmodel = model.getSchemaModel();

                final XsModelWalker walker = new XsModelWalker();

                final PropertyChangeListener changeListener = new PropertyChangeListener() {

                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        setDetailMessage(RunMultipleDialog.this.getMessage("visualProgressWorker.detailMessage", new Object[] { evt.getNewValue()}));
                    }

                };

                walker.addPropertyChangeListener(changeListener);

                for (Analyzer analyzer : getSelectedAnalyzers()) {
                    analyzer.setup(model);
                    walker.addWorkers(analyzer.getWorkers());
                }
                walker.walkModel(xsmodel);

                for (Analyzer analyzer : getSelectedAnalyzers()) {
                    analyzer.setResult(model);
                }

                return null;
            }

            /*
             * (non-Javadoc)
             * 
             * @see de.mindcrimeilab.swing.util.VisualProgressWorker#done()
             */
            @Override
            protected void done() {
                for (Analyzer analyzer : getSelectedAnalyzers()) {
                    analyzer.showResult();
                }
                super.done();
            }
        };

        worker.setTitle(getMessage("runMultipleDialog.visualProgressWorker.title"));
        worker.setActionText(getMessage("runMultipleDialog.visualProgressWorker.actionText"));
        worker.showProgressDialog();
        worker.execute();

        return true;
    }

    /**
     * Action listener wired to the checkboxes in order to modify the list of selected analyzers.
     * 
     * @author Michael Engelhardt<me@mindcrime-ilab.de>
     * @author $Author$
     * @version $Revision$
     * 
     */
    private static final class AnalyzerCheckBoxListener implements ActionListener {

        /** logger */
        private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

        /** reference to list of analyzer classes */
        final private Analyzer analyzerClass;

        /** analyzer which is handled by the listener */
        final private List<Analyzer> analyzers;

        /**
         * ctor()
         * 
         * @param analyzerList
         *            reference to list of analyzers to modify
         * @param analyzer
         *            class to set or remove form the analyzer list
         */
        AnalyzerCheckBoxListener(List<Analyzer> analyzerList, Analyzer analyzer) {
            this.analyzerClass = analyzer;
            this.analyzers = analyzerList;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent evt) {
            JCheckBox jcb = (JCheckBox) evt.getSource();
            if (jcb.isSelected()) {
                analyzers.add(this.analyzerClass);
                logger.debug("analyzer added for [" + this.analyzerClass + "]");
            }
            else {
                analyzers.remove(this.analyzerClass);
                logger.debug("analyzer removed for [" + this.analyzerClass + "]");
            }
        }
    }
}
