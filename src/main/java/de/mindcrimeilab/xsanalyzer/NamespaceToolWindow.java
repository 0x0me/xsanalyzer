// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ScrollPaneConstants;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.richclient.application.support.AbstractView;

import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.model.NamespacesListModel;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.ui.renderer.NamespaceItemRenderer;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class NamespaceToolWindow extends AbstractView implements ApplicationListener {

    private JList jlNamespaces;

    private XsAnalyzerApplicationModel model;

    /**
     * @return the model
     */
    public XsAnalyzerApplicationModel getModel() {
        return model;
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(XsAnalyzerApplicationModel model) {
        this.model = model;
        if (null != this.model && null != jlNamespaces) {
            jlNamespaces.setModel(new NamespacesListModel(model.getSchemaModel()));
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.application.support.AbstractView#createControl ()
     */
    @Override
    protected JComponent createControl() {
        createList();
        JPanel jpNamespaces = getComponentFactory().createPanel(new BorderLayout());
        jpNamespaces.add(getComponentFactory().createScrollPane(jlNamespaces, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);
        return jpNamespaces;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        logger.debug(event);
        if (event instanceof XsAnalyzerApplicationEvent) {
            switch (((XsAnalyzerApplicationEvent) event).getEventType()) {
                case OPEN:
                    jlNamespaces.setModel(new NamespacesListModel(model.getSchemaModel()));
                    break;
            }
        }

    }

    private void createList() {
        jlNamespaces = getComponentFactory().createList();
        jlNamespaces.setCellRenderer(new NamespaceItemRenderer());
    }
}
