// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.springframework.richclient.application.support.AbstractView;

import de.mindcrimeilab.swing.log4j.SwingConsole;
import de.mindcrimeilab.swing.log4j.SwingConsoleAppender;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class LoggerToolWindow extends AbstractView {

    /**
     * Create the actual UI control for this view. It will be placed into the window according to the layout of the page
     * holding this view.
     */
    @Override
    protected JComponent createControl() {
        JPanel panel = getComponentFactory().createPanel(new BorderLayout());
        SwingConsole console = new SwingConsole();
        Logger logger = Logger.getLogger("xsAnalyzerApplicationLogger");
        SwingConsoleAppender appender = (SwingConsoleAppender) logger.getAppender("swing");

        appender.setConsole(console);
        panel.add(console, BorderLayout.CENTER);

        return panel;
    }
}
