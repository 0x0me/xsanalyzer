// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Collection;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSModelGroup;
import org.apache.xerces.xs.XSModelGroupDefinition;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.richclient.application.support.AbstractView;

import com.l2fprod.common.propertysheet.Property;
import com.l2fprod.common.propertysheet.PropertySheetPanel;
import com.l2fprod.common.propertysheet.PropertySheetTable;

import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.ui.renderer.SchemaElementsRenderer;
import de.mindcrimeilab.xsanalyzer.ui.renderer.XSRenderFactory;
import de.mindcrimeilab.xsanalyzer.util.SchemaElementsPropertyFactory;
import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XsPropertyToolWindow extends AbstractView implements ApplicationListener {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private PropertySheetPanel propertySheetPanel;

    private JList jlPropertyDetails;

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.application.support.AbstractView#createControl ()
     */
    @Override
    protected JComponent createControl() {
        createPropertySheet();
        createDetailList();
        JPanel jpProperties = getComponentFactory().createPanel(new BorderLayout());

        JSplitPane jsp = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jsp.setTopComponent(getComponentFactory().createScrollPane(propertySheetPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED));

        JPanel jpDetails = getComponentFactory().createPanel(new BorderLayout());

        final JScrollPane jspPropertyDetails = getComponentFactory().createScrollPane(jlPropertyDetails, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jpDetails.add(getComponentFactory().createLabelFor("propertySheetPanel.detail.label", jspPropertyDetails), BorderLayout.NORTH);
        jpDetails.add(jspPropertyDetails, BorderLayout.CENTER);

        jsp.setBottomComponent(jpDetails);
        jsp.setDividerLocation(0.5);
        jpProperties.add(jsp, BorderLayout.CENTER);
        return jpProperties;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        XsPropertyToolWindow.logger.debug(event);
        if (event instanceof XsAnalyzerApplicationEvent) {
            switch (((XsAnalyzerApplicationEvent) event).getEventType()) {
                case SHOW_SCHEMA_ELEMENT_DETAIL:
                    showProperties(event.getSource());
                    ((DefaultListModel) jlPropertyDetails.getModel()).clear();
                    break;
            }
        }
    }

    private void showProperties(Object selectedObject) {
        try {
            if (selectedObject instanceof XSComplexTypeDefinition) {
                final Property[] p = SchemaElementsPropertyFactory.getComplexTypeProperties((XSComplexTypeDefinition) selectedObject);
                propertySheetPanel.setProperties(p);
                propertySheetPanel.readFromObject(selectedObject);
            }
            else if (selectedObject instanceof XSSimpleTypeDefinition) {
                final Property[] p = SchemaElementsPropertyFactory.getSimpleTypeProperties((XSSimpleTypeDefinition) selectedObject);
                propertySheetPanel.setProperties(p);
                propertySheetPanel.readFromObject(selectedObject);
            }
            else if (selectedObject instanceof XSModelGroupDefinition) {
                final Property[] p = SchemaElementsPropertyFactory.getModelGroupProperties((XSModelGroupDefinition) selectedObject);
                propertySheetPanel.setProperties(p);
                propertySheetPanel.readFromObject(selectedObject);
            }
            else if (selectedObject instanceof XSElementDeclaration) {
                final Property[] p = SchemaElementsPropertyFactory.getElementProperties((XSElementDeclaration) selectedObject);
                propertySheetPanel.setProperties(p);
                propertySheetPanel.readFromObject(selectedObject);
            }
            else if (selectedObject instanceof String) {
                // explicit exclude strings - details should be shown for XS*
                // types only
            }
            else {
                XsPropertyToolWindow.logger.debug("------------------- Fallback -----------------");
                XsPropertyToolWindow.logger.debug("Fallback for class [" + selectedObject.getClass().getName() + "]");
                final PropertyDescriptor[] pd = Introspector.getBeanInfo(selectedObject.getClass()).getPropertyDescriptors();

                propertySheetPanel.setProperties(pd);
                propertySheetPanel.readFromObject(selectedObject);
            }
        }
        catch (IntrospectionException e) {
            XsPropertyToolWindow.logger.warn("valueChanged(TreeSelectionEvent)", e);
            e.printStackTrace();
        }
    }

    private void createPropertySheet() {
        propertySheetPanel = new PropertySheetPanel();
        propertySheetPanel.setMinimumSize(new Dimension(200, 200));
        propertySheetPanel.setPreferredSize(new Dimension(200, 200));
        propertySheetPanel.setSize(new Dimension(200, 200));
        propertySheetPanel.setRendererFactory(new XSRenderFactory());
        // setup table
        final PropertySheetTable table = propertySheetPanel.getTable();
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.getSelectionModel().addListSelectionListener(new TableSelectionControl());
        table.setCellEditor(null);
    }

    private void createDetailList() {
        jlPropertyDetails = getComponentFactory().createList();
        jlPropertyDetails.setModel(new DefaultListModel());
        jlPropertyDetails.setCellRenderer(new SchemaElementsRenderer());
    }

    /**
     * Control
     * 
     * @author Michael Engelhardt<me@mindcrime-ilab.de>
     * @author $Author$
     * @version $Revision$
     * 
     */
    private class TableSelectionControl implements ListSelectionListener {

        /*
         * (non-Javadoc)
         * 
         * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing. event.ListSelectionEvent)
         */
        @Override
        public void valueChanged(ListSelectionEvent evt) {
            final PropertySheetTable table = propertySheetPanel.getTable();
            final DefaultListModel model = (DefaultListModel) jlPropertyDetails.getModel();

            SwingUtilities.invokeLater(new Runnable() {

                @SuppressWarnings("unchecked")
                @Override
                public void run() {
                    int rowIndex = table.getSelectedRow();
                    if (0 > rowIndex) { return; }
                    Object value = table.getValueAt(rowIndex, 1);

                    model.clear();

                    if (null == value) {
                        model.addElement("<null> value!");
                    }
                    else if (value instanceof Collection) {
                        for (Object item : (Collection) value) {
                            model.addElement(item);
                        }
                    }
                    else if (value instanceof StringList) {
                        StringList strList = (StringList) value;
                        for (int i = 0; i < strList.getLength(); ++i) {
                            model.addElement(strList.item(i));
                        }
                    }
                    else if (value instanceof XSModelGroup) {
                        XSModelGroup group = (XSModelGroup) value;
                        XSObjectList list = group.getParticles();
                        Collection collection = XSModelHelper.flat(list);
                        for (Object item : collection) {
                            model.addElement(item);
                        }
                    }
                    else {
                        model.addElement(value.toString());
                    }

                }
            });

        }
    }
}
