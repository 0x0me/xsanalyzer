// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008,2009,2010 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.beans.PropertyChangeListener;
import java.util.Collection;

import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public interface Analyzer {

    public abstract void setup(final XsAnalyzerApplicationModel model);

    /**
     * @param model
     */
    public abstract void analyze(final XsAnalyzerApplicationModel model);

    public abstract void setProgressListener(PropertyChangeListener listener);

    public abstract void setResult(final XsAnalyzerApplicationModel model);

    /**
     * 
     */
    public abstract void showResult();

    public Collection<XsComponentWorker> getWorkers();

    public LabelProvider getLabelProvider();
}