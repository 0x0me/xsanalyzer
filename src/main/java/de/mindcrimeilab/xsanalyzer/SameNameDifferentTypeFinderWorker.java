// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.xerces.impl.xs.util.XSObjectListImpl;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SameNameDifferentTypeFinderWorker extends NamespaceFilteredWorker implements XsComponentWorker {

    private final Map<QName, XSObjectListImpl> typesWithSameName;

    /**
     * 
     */
    public SameNameDifferentTypeFinderWorker() {
        super();
        typesWithSameName = new HashMap<QName, XSObjectListImpl>();
    }

    /**
     * @return the typesWithSameName
     */
    public synchronized Map<QName, ? extends XSObjectList> getTypesWithSameName() {
        removeSingularTypes();
        return Collections.unmodifiableMap(typesWithSameName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#execute(org.apache.xerces.xs.XSObject,
     * org.apache.xerces.xs.XSObject)
     */
    @Override
    public void execute(XSObject object, XSObject parent) {
        try {
            Method m = object.getClass().getMethod("getTypeDefinition", new Class[] {});
            XSTypeDefinition typedef = (XSTypeDefinition) m.invoke(object, new Object[] {});
            this.addTypeWithSameName(new QName(object.getNamespace(), object.getName()), typedef);
        }
        catch (Exception e) {
            logger.error("Cannot invoke method getTypeDefinition() on " + object + ".", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#isSupported(org.apache.xerces.xs.XSObject)
     */
    @Override
    public boolean isSupported(XSObject object) {
        return object.getType() == XSConstants.ELEMENT_DECLARATION || object.getType() == XSConstants.ATTRIBUTE_DECLARATION;
    }

    private void addTypeWithSameName(QName qname, XSTypeDefinition type) {
        // do not add null ;)
        final XSObjectListImpl xsoList;
        if (typesWithSameName.containsKey(qname)) {
            xsoList = typesWithSameName.get(qname);
            for (int i = 0; i < xsoList.getLength(); ++i) {
                XSObject object = xsoList.item(i);
                // check for duplicate types
                if (XSModelHelper.isSameTypeDefinition(object, type)) { return; }
            }
            xsoList.add(type);
        }
        else {
            xsoList = new XSObjectListImpl();
            xsoList.add(type);
            typesWithSameName.put(qname, xsoList);
        }
    }

    private void removeSingularTypes() {
        Set<QName> keys = typesWithSameName.keySet();
        for (Iterator<QName> it = keys.iterator(); it.hasNext();) {
            QName key = it.next();
            if (typesWithSameName.get(key).getLength() < 2) {
                logger.debug("Removing key [" + key + "] having count < 2");
                it.remove();
            }
        }
    }

}
