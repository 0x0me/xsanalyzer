// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class UnusedTypeFinderWorker extends NamespaceFilteredWorker implements XsComponentWorker {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    /** List of unused types */
    private final Set<XSTypeDefinition> unusedTypes;

    public UnusedTypeFinderWorker() {
        super();
        unusedTypes = new HashSet<XSTypeDefinition>();
    }

    public UnusedTypeFinderWorker(Set<? extends XSTypeDefinition> definedTypes) {
        this();
        /* threat all defined types as potentially unsed types. */
        unusedTypes.addAll(definedTypes);
    }

    public void setDefinedTypes(Set<? extends XSTypeDefinition> definedTypes) {
        unusedTypes.clear();
        unusedTypes.addAll(definedTypes);
    }

    /**
     * Returns the list of unused types in given model.
     * 
     * @return the unusedTypes
     */
    public Set<XSTypeDefinition> getUnusedTypes() {
        return Collections.unmodifiableSet(unusedTypes);
    }

    @Override
    public boolean isSupported(XSObject object) {
        return super.isSupported(object);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#execute(org.apache.xerces .xs.XSObject)
     */
    @Override
    public void execute(XSObject object, XSObject parent) {
        /*
         * LOOK FORWARD
         * 
         * get base type of given object and check if the type is in the list of global defined types.
         * 
         * There are is one special case, if it is a simple type of varity list, there is no real base type.
         */
        final XSTypeDefinition type = XSModelHelper.getBaseType(object);

        if (null != type) {
            boolean result = unusedTypes.remove(type);
            UnusedTypeFinderWorker.logger.debug("Remove result is [" + result + "]");
        }

        // checkUsage(definedTypes, object, (XSTypeDefinition) parent);
    }
}
