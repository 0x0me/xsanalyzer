// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.rules.constraints;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSTypeDefinition;
import org.springframework.core.closure.Constraint;

import de.mindcrimeilab.util.Accessor;

/**
 * Constraint filtering schema elements depending on their type.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SchemaTypeConstraint extends Observable implements Constraint {

    private final List<Short> visibleTypes;

    private Accessor accessor;

    public SchemaTypeConstraint() {
        super();
        visibleTypes = new ArrayList<Short>();
        visibleTypes.add(XSConstants.ELEMENT_DECLARATION);
        visibleTypes.add(XSConstants.ATTRIBUTE_DECLARATION);
        visibleTypes.add(XSConstants.MODEL_GROUP_DEFINITION);
        visibleTypes.add(XSTypeDefinition.COMPLEX_TYPE);
        visibleTypes.add(XSTypeDefinition.SIMPLE_TYPE);
        accessor = null;
    }

    public SchemaTypeConstraint(Accessor accessor) {
        this();
        this.accessor = accessor;

    }

    @Override
    public boolean test(Object argument) {
        final XSObject testObject;
        if (argument instanceof XSObject) {
            testObject = (XSObject) argument;
        }
        else {
            testObject = (XSObject) accessor.getValue(argument);
        }

        short type = (testObject).getType();
        if (type == XSConstants.TYPE_DEFINITION) {
            type = ((XSTypeDefinition) testObject).getTypeCategory();
        }

        return visibleTypes.contains(type);
    }

    /**
     * @param passesTest
     * @param object
     * @return
     */
    private boolean test0(final Object object) {
        boolean passesTest = false;

        return passesTest;
    }

    /**
     * Enable/disable filter for given type.
     * 
     * @param type
     *            to filter
     * @param filtered
     *            true for filtering otherwise false
     */
    public void setFilter(short type, boolean filtered) {
        if (filtered) {
            visibleTypes.remove((Short) type);
        }
        else {
            if (!visibleTypes.contains(type)) {
                visibleTypes.add(type);
            }
        }
        setChanged();
        notifyObservers();
    }
}
