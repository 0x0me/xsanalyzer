/*
 * Copyright 2002-2006 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.mindcrimeilab.xsanalyzer;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JComponent;

import org.apache.xerces.impl.xs.XSGroupDecl;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSNamespaceItem;
import org.apache.xerces.xs.XSTypeDefinition;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.springframework.binding.value.ValueModel;
import org.springframework.binding.value.support.ValueHolder;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.richclient.application.support.AbstractView;
import org.springframework.richclient.factory.ComponentFactory;
import org.springframework.richclient.layout.GridBagLayoutBuilder;

import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.model.PieDatasetValueModel;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;

/**
 * This class defines the initial view to be presented in the archetypeapplication. It is constructed automatically by
 * the platform and configured according to the bean specification in the application context.
 * 
 * @author Larry Streepy
 * 
 */
public class SchemaInfoView extends AbstractView implements ApplicationListener {

    private XsAnalyzerApplicationModel model;

    private final ValueModel numComplexTypes = new ValueHolder(0);

    private final ValueModel numSimpleTypes = new ValueHolder(0);

    private final ValueModel numElements = new ValueHolder(0);

    private final ValueModel numGroups = new ValueHolder(0);

    private final ValueModel targetNamespace = new ValueHolder(getMessage("No schema loaded."));

    private JFreeChart schemaElementsChart;

    private ChartPanel cpSchemaElements;

    /**
     * @return the model
     */
    public XsAnalyzerApplicationModel getModel() {
        return model;
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(XsAnalyzerApplicationModel model) {
        this.model = model;
        if (null != this.model.getSchemaModel()) {
            update();
        }
    }

    /**
     * 
     */
    private void update() {
        XSModel xsModel = model.getSchemaModel();
        if (null != xsModel) {
            final List<XSNamespaceItem> namespaces = new LinkedList<XSNamespaceItem>();
            final XSNamespaceItem tns = model.getTargetNamespace();
            org.springframework.util.Assert.notNull(tns);
            namespaces.add(tns);
            targetNamespace.setValue(tns.getSchemaNamespace());
            Set<? extends XSTypeDefinition> ctypes = XSModelHelper.getComponents(xsModel, namespaces, new short[] { XSTypeDefinition.COMPLEX_TYPE});
            numComplexTypes.setValue(ctypes.size());
            Set<? extends XSTypeDefinition> stypes = XSModelHelper.getComponents(xsModel, namespaces, new short[] { XSTypeDefinition.SIMPLE_TYPE});
            numSimpleTypes.setValue(stypes.size());
            List<XSElementDeclaration> elements = XSModelHelper.getElements(xsModel, namespaces);
            numElements.setValue(elements.size());
            List<? extends XSGroupDecl> groups = XSModelHelper.getGroups(xsModel, namespaces);
            numGroups.setValue(groups.size());
        }
    }

    /**
     * Create the actual UI control for this view. It will be placed into the window according to the layout of the page
     * holding this view.
     */
    @Override
    protected JComponent createControl() {
        createSchemaElementsChart();
        GridBagLayoutBuilder builder = new GridBagLayoutBuilder();

        final ComponentFactory cf = getComponentFactory();
        builder.append(cf.createTitleLabel("schemaInfoView.titleLabel"), 1, 1, true, false).nextLine();
        builder.append(cf.createLabel("schemaInfoView.schema.targetNamespace", new ValueModel[] { targetNamespace}), 1, 1, true, false).nextLine();

        builder.append(cpSchemaElements, 1, 1, true, true);
        return builder.getPanel();
    }

    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof XsAnalyzerApplicationEvent) {
            switch (((XsAnalyzerApplicationEvent) event).getEventType()) {
                case OPEN:
                    update();
                    break;
            }
        }
    }

    private void createSchemaElementsChart() {
        PieDatasetValueModel<String> pieDataset = new PieDatasetValueModel<String>();
        pieDataset.setValue(getMessage("schemaInfoView.schema.numberOfComplexTypes"), numComplexTypes);
        pieDataset.setValue(getMessage("schemaInfoView.schema.numberOfSimpleTypes"), numSimpleTypes);
        pieDataset.setValue(getMessage("schemaInfoView.schema.numberOfGroups"), numGroups);
        pieDataset.setValue(getMessage("schemaInfoView.schema.numberOfElements"), numElements);

        schemaElementsChart = ChartFactory.createPieChart(getMessage("schemaInfoView.sectionLabel"), pieDataset, false, true, false);
        cpSchemaElements = new ChartPanel(schemaElementsChart, true);
    }
}
