package de.mindcrimeilab.xsanalyzer;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.xerces.xs.XSObject;

public abstract class NamespaceFilteredWorker extends AbstractXsComponentWorker implements XsComponentWorker {

    /** Namespaces to ignore - if empty examine all */
    protected List<String> filteredNamespaces;

    public NamespaceFilteredWorker() {
        super();
    }

    /**
     * Set array of namespaces which should be ignored
     * 
     * @param namespaces
     */
    public void setFilteredNamespaces(String[] namespaces) {
        filteredNamespaces = Arrays.asList(namespaces);
    }

    @Override
    public boolean isSupported(XSObject object) {
        boolean accepts = false;
        if (null == object) {
            accepts = false;
        }
        else if (null == filteredNamespaces) {
            accepts = true;
        }
        else {
            String namespace = object.getNamespace();
            // Attributes may have a null namespace
            if (null == namespace) {
                accepts = true;
            }
            else if (StringUtils.isNotEmpty(namespace) && !filteredNamespaces.contains(namespace)) {
                accepts = true;
            }
            else {
                accepts = false;
            }
        }
        return accepts;
    }

    @Override
    abstract public void execute(XSObject object, XSObject parent);
}