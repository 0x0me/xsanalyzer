// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSObject;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class ComponentVisitor implements XsComponentWorker {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private final Map<XSObject, Integer> visits;

    public ComponentVisitor() {
        visits = new HashMap<XSObject, Integer>();
    }

    public Map<XSObject, Integer> getVisits() {
        return Collections.unmodifiableMap(visits);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#execute(org.apache.xerces .xs.XSObject)
     */
    @Override
    public void execute(XSObject object, XSObject parent) {
        // String key = "{" + object.getNamespace() + "}:" + object.getName();
        if (object.getName() == null) {
            ComponentVisitor.logger.debug("Name is null!");
        }
        if (visits.containsKey(object)) {
            int count = visits.get(object);
            count++;
            visits.put(object, count);
        }
        else {
            visits.put(object, 1);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#isSupported(org.apache. xerces.xs.XSObject)
     */
    @Override
    public boolean isSupported(XSObject object) {
        return true;
    }

}
