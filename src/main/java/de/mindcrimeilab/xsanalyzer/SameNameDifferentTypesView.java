// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListModel;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSObjectList;
import org.springframework.binding.value.ValueModel;
import org.springframework.binding.value.support.ValueHolder;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.richclient.application.PageComponentContext;
import org.springframework.richclient.application.support.AbstractView;
import org.springframework.richclient.command.CommandGroup;
import org.springframework.richclient.command.support.GlobalCommandIds;
import org.springframework.richclient.factory.ComponentFactory;
import org.springframework.richclient.layout.GridBagLayoutBuilder;
import org.springframework.richclient.list.ListSelectionValueModelAdapter;
import org.springframework.richclient.util.PopupMenuMouseListener;

import de.mindcrimeilab.xsanalyzer.actions.AbstractXSObjectCommand;
import de.mindcrimeilab.xsanalyzer.actions.TypeHierarchyCommand;
import de.mindcrimeilab.xsanalyzer.actions.TypeInspectionCommand;
import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.actions.XsElementPropertiesExecutor;
import de.mindcrimeilab.xsanalyzer.model.JListSelectionValueModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.SameNameDetailListModelFactory;
import de.mindcrimeilab.xsanalyzer.model.SameNameListModel;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.rules.constraints.SchemaTypeConstraint;
import de.mindcrimeilab.xsanalyzer.ui.MasterListDetailList;
import de.mindcrimeilab.xsanalyzer.ui.filter.FilterToolbarFactory;
import de.mindcrimeilab.xsanalyzer.ui.renderer.QNameRenderer;
import de.mindcrimeilab.xsanalyzer.ui.renderer.SchemaElementsRenderer;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SameNameDifferentTypesView extends AbstractView implements ApplicationListener {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private XsAnalyzerApplicationModel model;

    private final MasterListDetailList masterDetailList;

    private final AbstractXSObjectCommand typeHierarchyCommand = new TypeHierarchyCommand();

    private final AbstractXSObjectCommand typeInspectionCommand = new TypeInspectionCommand();

    private final XsElementPropertiesExecutor xsElementPropertiesExecutor = new XsElementPropertiesExecutor();

    private final ValueModel numNames = new ValueHolder(0);

    private final ValueModel numTypes = new ValueHolder(0);

    private final SchemaTypeConstraint elementTypeConstraint = new SchemaTypeConstraint();

    /**
     * 
     */
    public SameNameDifferentTypesView() {
        masterDetailList = new MasterListDetailList();
        masterDetailList.setListModelFactory(new SameNameDetailListModelFactory(elementTypeConstraint));

        masterDetailList.addPropertyChangeListener(MasterListDetailList.PC_DETAILLIST_LISTMODEL, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                logger.debug("Adjusting commands to new model");
                final ListModel listModel = (ListModel) evt.getNewValue();
                final ValueModel selectionModel = new ListSelectionValueModelAdapter(masterDetailList.getDetailList().getSelectionModel());
                final ValueModel vModel = new JListSelectionValueModelAdapter(listModel, (ListSelectionValueModelAdapter) selectionModel);
                typeHierarchyCommand.setValueModel(vModel);
                typeInspectionCommand.setValueModel(vModel);
                numTypes.setValue(listModel.getSize());
            }
        });
    }

    @Override
    protected void registerLocalCommandExecutors(PageComponentContext context) {
        context.register(GlobalCommandIds.PROPERTIES, xsElementPropertiesExecutor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.application.support.AbstractView#createControl()
     */
    @Override
    protected JComponent createControl() {

        final JList masterList = masterDetailList.getMasterList();
        final JList detailList = masterDetailList.getDetailList();
        detailList.setCellRenderer(new SchemaElementsRenderer());
        masterList.setCellRenderer(new QNameRenderer());

        final CommandGroup group = getWindowCommandManager().createCommandGroup("typeListCommandGroup", new Object[] { typeHierarchyCommand, typeInspectionCommand, "separator", GlobalCommandIds.PROPERTIES});
        final JPopupMenu popup = group.createPopupMenu();
        detailList.addMouseListener(new PopupMenuMouseListener(popup));

        // build master view
        final GridBagLayoutBuilder masterPanelBuilder = new GridBagLayoutBuilder();
        final ComponentFactory cf = getComponentFactory();
        masterPanelBuilder.append(cf.createLabel("sameNameDifferentTypesView.nameList.label", new ValueModel[] { numNames})).nextLine();
        masterPanelBuilder.append(cf.createScrollPane(masterList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), 1, 1, true, true);

        // build detail view
        final GridBagLayoutBuilder detailPanelBuilder = new GridBagLayoutBuilder();
        detailPanelBuilder.append(FilterToolbarFactory.createFilterToolBar(elementTypeConstraint), 1, 1, true, false).nextLine();
        detailPanelBuilder.append(cf.createLabel("sameNameDifferentTypesView.typeList.label", new ValueModel[] { numTypes})).nextLine();
        detailPanelBuilder.append(cf.createScrollPane(detailList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), 1, 1, true, true);

        final JSplitPane jsp = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jsp.setDividerLocation(0.5f);
        jsp.setTopComponent(masterPanelBuilder.getPanel());
        jsp.setBottomComponent(detailPanelBuilder.getPanel());
        return jsp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
     */
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof XsAnalyzerApplicationEvent) {
            switch (((XsAnalyzerApplicationEvent) event).getEventType()) {
                case SAME_NAMES:
                    XsAnalyzerApplicationModel appmodel = (XsAnalyzerApplicationModel) getApplicationContext().getBean("applicationModel");
                    setModel(appmodel);
                    break;
                case OPEN:
                    invalidate();
                    break;
            }
        }
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(XsAnalyzerApplicationModel model) {
        this.model = model;
        updateModel();
    }

    private void updateModel() {
        if (null != model) {
            final Map<QName, XSObjectList> sameNamesDifferentTypes = model.getSameNamesDifferentTypes();
            update(sameNamesDifferentTypes);
        }
    }

    /**
     * @param sameNamesDifferentTypes
     */
    private void update(final Map<QName, XSObjectList> sameNamesDifferentTypes) {
        ListModel listModel = new SameNameListModel(sameNamesDifferentTypes);
        masterDetailList.setMasterListModel(listModel);
        numNames.setValue(listModel.getSize());
    }

    private void invalidate() {
        this.update(Collections.<QName, XSObjectList> emptyMap());
    }
}
