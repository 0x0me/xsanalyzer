// $Id:XSAnalyzerConstants.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import org.apache.xerces.xs.XSSimpleTypeDefinition;

/**
 * Application-wide constants.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public interface XSAnalyzerConstants {

    public static final String APP_VERSION = "0.0.1";

    public final static String GPL_COPYLEFT_NOTICE = "xsAnalyzer version " + XSAnalyzerConstants.APP_VERSION + ", Copyright (C) 2008 Michael Engelhardt\n" + "xsAnalyzer comes with ABSOLUTELY NO WARRANTY; for details\n"
            + "see 'Help->License'.  This is free software, and you are welcome\n" + "to redistribute it under certain conditions; see \'Help->License\' \n" + "for details.";

    public final static String PREFERENCES_IDENTIFIER = "/de/mindcrimeilab/xsanalyzer";

    /**
     * XML Schema namespace
     */
    public static final String XML_SCHEMA_NAMESPACE = "http://www.w3.org/2001/XMLSchema";

    /**
     * XML Schema facets
     */
    public static final Short[] USED_FACETS = { XSSimpleTypeDefinition.FACET_ENUMERATION, XSSimpleTypeDefinition.FACET_FRACTIONDIGITS, XSSimpleTypeDefinition.FACET_LENGTH, XSSimpleTypeDefinition.FACET_MAXEXCLUSIVE,
            XSSimpleTypeDefinition.FACET_MAXINCLUSIVE, XSSimpleTypeDefinition.FACET_MAXLENGTH, XSSimpleTypeDefinition.FACET_MINEXCLUSIVE, XSSimpleTypeDefinition.FACET_MININCLUSIVE, XSSimpleTypeDefinition.FACET_MINLENGTH,
            XSSimpleTypeDefinition.FACET_NONE, XSSimpleTypeDefinition.FACET_PATTERN, XSSimpleTypeDefinition.FACET_TOTALDIGITS, XSSimpleTypeDefinition.FACET_WHITESPACE};

    /**
     * Default values for unset XML Schema facets.
     */
    public static final String[] DEFAULTS = { null, null, null, null, null, null, null, null, null, null, null, null, "preserve"};

}
