// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.awt.Insets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSTypeDefinition;
import org.springframework.binding.value.ValueModel;
import org.springframework.binding.value.support.ValueHolder;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.richclient.application.PageComponentContext;
import org.springframework.richclient.application.support.AbstractView;
import org.springframework.richclient.command.CommandGroup;
import org.springframework.richclient.command.ParameterizableActionCommandExecutor;
import org.springframework.richclient.command.support.GlobalCommandIds;
import org.springframework.richclient.factory.ComponentFactory;
import org.springframework.richclient.layout.GridBagLayoutBuilder;
import org.springframework.richclient.list.FilteredListModel;
import org.springframework.richclient.list.ListSelectionValueModelAdapter;
import org.springframework.richclient.util.PopupMenuMouseListener;

import de.mindcrimeilab.util.DefaultMutableTreeNodeAccessor;
import de.mindcrimeilab.xsanalyzer.actions.AbstractXSObjectCommand;
import de.mindcrimeilab.xsanalyzer.actions.TypeHierarchyCommand;
import de.mindcrimeilab.xsanalyzer.actions.TypeInspectionCommand;
import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.actions.XsElementPropertiesExecutor;
import de.mindcrimeilab.xsanalyzer.model.FilteredTreeModel;
import de.mindcrimeilab.xsanalyzer.model.JListSelectionValueModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.JTreeValueModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.TreeSelectionValueModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.UnusedTypesListModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.UsedTypesTreeModelAdapter;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.rules.constraints.SchemaTypeConstraint;
import de.mindcrimeilab.xsanalyzer.ui.filter.FilterToolbarFactory;
import de.mindcrimeilab.xsanalyzer.ui.renderer.SchemaElementsRenderer;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeUsageView extends AbstractView implements ApplicationListener {

    private final ValueModel numUsedTypes = new ValueHolder(0);

    private final ValueModel numUnusedTypes = new ValueHolder(0);

    private JList jlUnusedTypes;

    private JTree jtUsedTypes;

    private XsAnalyzerApplicationModel model;

    private final XsElementPropertiesExecutor xsElementPropertiesExecutor = new XsElementPropertiesExecutor();

    private final AbstractXSObjectCommand unusedTypeHierarchyCommand = new TypeHierarchyCommand();

    private final AbstractXSObjectCommand usedTypeHierarchyCommand = new TypeHierarchyCommand();

    private final AbstractXSObjectCommand unusedTypeInspectionCommand = new TypeInspectionCommand();

    private final AbstractXSObjectCommand usedTypeInspectionCommand = new TypeInspectionCommand();

    private final SchemaTypeConstraint unusedTypesConstraint;

    private final SchemaTypeConstraint usedTypesConstraint;

    private final XsPropertiesSelectionListener xsPropertiesSelectionListener;

    public TypeUsageView() {
        unusedTypesConstraint = new SchemaTypeConstraint();
        usedTypesConstraint = new SchemaTypeConstraint(new DefaultMutableTreeNodeAccessor());
        xsPropertiesSelectionListener = new XsPropertiesSelectionListener(xsElementPropertiesExecutor);
    }

    @Override
    protected void registerLocalCommandExecutors(PageComponentContext context) {
        context.register(GlobalCommandIds.PROPERTIES, xsElementPropertiesExecutor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.application.support.AbstractView#createControl ()
     */
    @Override
    protected JComponent createControl() {
        createUnusedTypesList();
        createUsedTypesTree();

        final ComponentFactory cf = getComponentFactory();
        final GridBagLayoutBuilder builder = new GridBagLayoutBuilder();
        builder.setDefaultInsets(new Insets(2, 5, 2, 5));

        final JToolBar filterToolbar = FilterToolbarFactory.createFilterToolBar(new SchemaTypeConstraint[] { unusedTypesConstraint, usedTypesConstraint});
        builder.append(filterToolbar, 1, 1, true, false).nextLine();

        builder.appendLabel(cf.createLabel("typeUsageView.label.unusedTypes", new ValueModel[] { numUnusedTypes})).nextLine();

        builder.append(cf.createScrollPane(jlUnusedTypes, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), 1, 1, 1, 1).nextLine();

        builder.appendSeparator();

        builder.appendLabel(cf.createLabel("typeUsageView.label.usedTypes", new ValueModel[] { numUsedTypes})).nextLine();

        builder.append(cf.createScrollPane(jtUsedTypes, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), 1, 1, 1, 2);

        builder.setAutoSpanLastComponent(true);
        JPanel jpTypeUsage = builder.getPanel();

        return jpTypeUsage;
    }

    private void createUnusedTypesList() {
        jlUnusedTypes = getComponentFactory().createList();
        jlUnusedTypes.setCellRenderer(new SchemaElementsRenderer());

        jlUnusedTypes.addListSelectionListener(xsPropertiesSelectionListener);

        final CommandGroup group = getWindowCommandManager().createCommandGroup("unusedTypeUsageCommandGroup", new Object[] { unusedTypeHierarchyCommand, unusedTypeInspectionCommand, "separator", GlobalCommandIds.PROPERTIES});
        final JPopupMenu popup = group.createPopupMenu();
        jlUnusedTypes.addMouseListener(new PopupMenuMouseListener(popup));
    }

    private void createUsedTypesTree() {
        jtUsedTypes = new JTree(new DefaultMutableTreeNode(getMessage("usedTypes.tree.noModel")));
        jtUsedTypes.setCellRenderer(new SchemaElementsRenderer());

        final CommandGroup group = getWindowCommandManager().createCommandGroup("usedTypeUsageCommandGroup", new Object[] { usedTypeHierarchyCommand, usedTypeInspectionCommand, "separator", GlobalCommandIds.PROPERTIES});
        final JPopupMenu popup = group.createPopupMenu();
        jtUsedTypes.addMouseListener(new PopupMenuMouseListener(popup));

        jtUsedTypes.addTreeSelectionListener(xsPropertiesSelectionListener);
    }

    /**
     * @return the model
     */
    public XsAnalyzerApplicationModel getModel() {
        return model;
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(XsAnalyzerApplicationModel model) {
        this.model = model;
        updateModel();
    }

    private void updateModel() {
        if (null != model) {
            Set<XSTypeDefinition> unusedTypes = model.getUnusedTypes();
            updateUnusedTypesView(unusedTypes);

            Map<XSObject, ? extends XSObjectList> usedTypes = model.getUsedTypes();
            updateUsedTypesView(usedTypes);
        }
    }

    /**
     * @param usedTypes
     */
    private void updateUsedTypesView(Map<XSObject, ? extends XSObjectList> usedTypes) {
        if (null != usedTypes) {
            final UsedTypesTreeModelAdapter usedTypesTreeModelAdapter = new UsedTypesTreeModelAdapter(usedTypes);
            final FilteredTreeModel filteredModel = new FilteredTreeModel(usedTypesTreeModelAdapter, usedTypesConstraint);
            jtUsedTypes.setModel(filteredModel);
            // jtUsedTypes.setModel(usedTypesTreeModelAdapter);
            numUsedTypes.setValue(usedTypesTreeModelAdapter.getChildCount(usedTypesTreeModelAdapter.getRoot()));

            final ValueModel selectionHolder = new TreeSelectionValueModelAdapter(jtUsedTypes.getSelectionModel());
            final ValueModel vModel = new JTreeValueModelAdapter((TreeSelectionValueModelAdapter) selectionHolder);

            usedTypeHierarchyCommand.setValueModel(vModel);
            usedTypeHierarchyCommand.setAccessor(new DefaultMutableTreeNodeAccessor());

            usedTypeInspectionCommand.setValueModel(vModel);
            usedTypeInspectionCommand.setAccessor(new DefaultMutableTreeNodeAccessor());
        }
    }

    /**
     * @param unusedTypes
     */
    private void updateUnusedTypesView(Set<XSTypeDefinition> unusedTypes) {
        if (null != unusedTypes) {
            final UnusedTypesListModelAdapter unusedTypesListModelAdapter = new UnusedTypesListModelAdapter(unusedTypes);
            final FilteredListModel filteredModel = new FilteredListModel(unusedTypesListModelAdapter, unusedTypesConstraint);
            jlUnusedTypes.setModel(filteredModel);
            numUnusedTypes.setValue(unusedTypesListModelAdapter.getSize());

            final ValueModel selectionModel = new ListSelectionValueModelAdapter(jlUnusedTypes.getSelectionModel());
            final ValueModel vModel = new JListSelectionValueModelAdapter(jlUnusedTypes.getModel(), (ListSelectionValueModelAdapter) selectionModel);
            unusedTypeHierarchyCommand.setValueModel(vModel);
            unusedTypeInspectionCommand.setValueModel(vModel);

        }
    }

    private void invalidate() {
        updateUnusedTypesView(Collections.<XSTypeDefinition> emptySet());
        updateUsedTypesView(Collections.<XSObject, XSObjectList> emptyMap());
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof XsAnalyzerApplicationEvent) {
            switch (((XsAnalyzerApplicationEvent) event).getEventType()) {
                case TYPE_USAGE:
                    XsAnalyzerApplicationModel appmodel = (XsAnalyzerApplicationModel) getApplicationContext().getBean("applicationModel");
                    setModel(appmodel);
                    break;
                case OPEN:
                    invalidate();
                    break;
            }
        }

    }

    private static class XsPropertiesSelectionListener implements ListSelectionListener, TreeSelectionListener {

        /**
     * 
     */
        private final ParameterizableActionCommandExecutor executor;

        public XsPropertiesSelectionListener(ParameterizableActionCommandExecutor executor) {
            this.executor = executor;
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            Map<String, XSObject> parameters = new HashMap<String, XSObject>();
            JList list = (JList) e.getSource();
            parameters.put(XsElementPropertiesExecutor.OBJECT, (XSObject) list.getSelectedValue());
            executor.execute(parameters);
        }

        @Override
        public void valueChanged(TreeSelectionEvent e) {
            TreePath path = e.getPath();
            if (null != path) {
                Map<String, XSObject> parameters = new HashMap<String, XSObject>();
                Object lastPathComponent = path.getLastPathComponent();

                // unwrap tree nodes
                if (lastPathComponent instanceof DefaultMutableTreeNode) {
                    lastPathComponent = ((DefaultMutableTreeNode) lastPathComponent).getUserObject();
                }

                // execute executor ;)
                if (lastPathComponent instanceof XSObject) {
                    parameters.put(XsElementPropertiesExecutor.OBJECT, (XSObject) lastPathComponent);
                    executor.execute(parameters);
                }
            }
        }
    }
}
