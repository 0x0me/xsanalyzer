// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XSListHelper {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    public static List<String> asList(StringList stringList) {
        return XSListHelper.asList(stringList, ArrayList.class);
    }

    public static List<String> asList(StringList stringList, Class<? extends List> collectionClazz) {
        List<String> result;
        try {
            result = collectionClazz.newInstance();
        }
        catch (InstantiationException e) {
            XSListHelper.logger.warn("Could not create class " + collectionClazz, e);
            result = new ArrayList<String>(stringList.getLength());
        }
        catch (IllegalAccessException e) {
            XSListHelper.logger.warn("Could not create class " + collectionClazz, e);
            result = new ArrayList<String>(stringList.getLength());
        }
        for (int i = 0; i < stringList.getLength(); ++i) {
            result.add(stringList.item(i));
        }
        return result;
    }

    public static List<XSObject> asList(XSObjectList xsobjectList) {
        return XSListHelper.asList(xsobjectList, ArrayList.class);
    }

    public static List<XSObject> asList(XSObjectList xsobjectList, Class<? extends List> collectionClazz) {
        List<XSObject> result;
        try {
            result = collectionClazz.newInstance();
        }
        catch (InstantiationException e) {
            XSListHelper.logger.warn("Could not create class " + collectionClazz, e);
            result = new ArrayList<XSObject>(xsobjectList.getLength());
        }
        catch (IllegalAccessException e) {
            XSListHelper.logger.warn("Could not create class " + collectionClazz, e);
            result = new ArrayList<XSObject>(xsobjectList.getLength());
        }
        for (int i = 0; i < xsobjectList.getLength(); ++i) {
            result.add(xsobjectList.item(i));
        }
        return result;
    }
}
