// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSImplementation;
import org.apache.xerces.xs.XSLoader;
import org.apache.xerces.xs.XSModel;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.LSInput;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XsModelFactory {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    public static XSModel createXsModel(File file) {
        XSLoader schemaLoader = createXsLoader();

        XSModel xsmodel = schemaLoader.loadURI(file.toURI().toString());
        XsModelFactory.logger.info("Model loaded [" + xsmodel + "]");

        return xsmodel;
    }

    public static XSModel createXsModel(final String data) {
        XSLoader schemaLoader = createXsLoader();

        XSModel xsmodel = schemaLoader.load(new LSInput() {

            @Override
            public String getBaseURI() {
                return ".";
            }

            @Override
            public InputStream getByteStream() {
                return new ByteArrayInputStream(data.getBytes());
            }

            @Override
            public boolean getCertifiedText() {
                return false;
            }

            @Override
            public Reader getCharacterStream() {
                return new StringReader(data);
            }

            @Override
            public String getEncoding() {
                return "UTF-8";
            }

            @Override
            public String getPublicId() {
                return null;
            }

            @Override
            public String getStringData() {
                return data;
            }

            @Override
            public String getSystemId() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void setBaseURI(String baseURI) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setByteStream(InputStream byteStream) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setCertifiedText(boolean certifiedText) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setCharacterStream(Reader characterStream) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setEncoding(String encoding) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setPublicId(String publicId) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setStringData(String stringData) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setSystemId(String systemId) {
                // TODO Auto-generated method stub

            }
        });
        XsModelFactory.logger.info("Model loaded [" + xsmodel + "]");

        return xsmodel;
    }

    /**
     * Initializes and creates a new schema loader instance.
     * 
     * @return
     */
    private static XSLoader createXsLoader() {
        System.setProperty(DOMImplementationRegistry.PROPERTY, "org.apache.xerces.dom.DOMXSImplementationSourceImpl");
        DOMImplementationRegistry registry = null;
        try {
            registry = DOMImplementationRegistry.newInstance();
        }
        catch (ClassCastException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        XSImplementation impl = (XSImplementation) registry.getDOMImplementation("XS-Loader");

        XSLoader schemaLoader = impl.createXSLoader(null);
        return schemaLoader;
    }
}
