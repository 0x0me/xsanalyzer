// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import org.apache.commons.lang.StringUtils;

/**
 * A {@code FileFilter} accepting directories and ".xsd" files.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XSAnalyzerDefaultFileFilter extends FileFilter {

    private static final String FILE_SUFFIX_XSD_DESCRIPTION = "XML schema";

    private static final String FILE_SUFFIX_XSD = "xsd";

    @Override
    public boolean accept(File f) {
        boolean result = false;
        String name = f.getName();
        if (f.isFile() && !StringUtils.isEmpty(name)) {
            int idx = name.lastIndexOf(".");
            if (-1 != idx) {
                result = XSAnalyzerDefaultFileFilter.FILE_SUFFIX_XSD.equalsIgnoreCase(name.substring(idx + 1, name.length()));
            }
        }
        else {
            result = f.isDirectory();
        }
        return result;
    }

    @Override
    public String getDescription() {
        return XSAnalyzerDefaultFileFilter.FILE_SUFFIX_XSD_DESCRIPTION;
    }

}
