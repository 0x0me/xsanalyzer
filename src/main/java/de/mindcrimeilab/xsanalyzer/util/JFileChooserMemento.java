// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import java.io.File;

import javax.swing.JFileChooser;

import org.apache.commons.lang.StringUtils;
import org.springframework.richclient.settings.Settings;
import org.springframework.richclient.settings.support.Memento;
import org.springframework.richclient.util.Assert;

/**
 * Saves and restores state of {@code java.swing.JFileChooser}. Currently it handles only the last selected directory.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class JFileChooserMemento implements Memento {

    /*
     * Keys
     */
    /** key to access last selected directory */
    private final static String LAST_SELECTED_DIRECTORY = "lastSelectedDirectory";

    private transient final JFileChooser fileChooser;

    /**
     * ctor()
     * 
     * @param fileChooser
     *            to save or restore state
     */
    public JFileChooserMemento(JFileChooser fileChooser) {
        Assert.required(fileChooser, "fileChooser");
        this.fileChooser = fileChooser;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.richclient.settings.support.Memento#restoreState(org.springframework.richclient.settings.
     * Settings)
     */
    @Override
    public void restoreState(Settings settings) {
        Assert.required(settings, "settings");
        String value = settings.getString(JFileChooserMemento.LAST_SELECTED_DIRECTORY);
        if (StringUtils.isNotBlank(value)) {
            fileChooser.setCurrentDirectory(new File(value));
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.richclient.settings.support.Memento#saveState(org.springframework.richclient.settings.Settings
     * )
     */
    @Override
    public void saveState(Settings settings) {
        Assert.required(settings, "settings");
        File value = fileChooser.getSelectedFile();
        if (null != value) {
            settings.setString(JFileChooserMemento.LAST_SELECTED_DIRECTORY, value.getAbsoluteFile().getParent());
        }
    }

}
