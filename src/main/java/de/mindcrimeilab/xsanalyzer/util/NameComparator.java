// $Id:NameComparator.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import java.util.Comparator;

import org.apache.xerces.xs.XSObject;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class NameComparator implements Comparator<XSObject> {

    /*
     * (non-Javadoc)
     * 
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(XSObject o1, XSObject o2) {
        int ret = 0;
        if (null != o1) {
            if (null != o2) {
                final String name1 = o1.getName();
                final String name2 = o2.getName();
                if (null == name1 && null != name2) {
                    ret = -1;
                }
                else if (null != name1 && null == name2) {
                    ret = 1;
                }
                else if (null == name1 && null == name2) {
                    ret = 0;
                }
                else {
                    ret = (null == name1) ? -1 : name1.compareTo(name2);
                }
            }
            else {
                ret = -1;
            }
        }
        else {
            if (o2 != null) {
                ret = 1;
            }
            else {
                ret = 0;
            }
        }
        return ret;
    }
}
