// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSFacet;
import org.apache.xerces.xs.XSModelGroupDefinition;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSSimpleTypeDefinition;

import com.l2fprod.common.propertysheet.DefaultProperty;
import com.l2fprod.common.propertysheet.Property;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SchemaElementsPropertyFactory {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private static List<String> COMPLEX_TYPE_PROPERTIES = Arrays.asList(new String[] { "abstract", "anonymous", "baseType", "contentType", "derivationMethod", "final", "name", "namespace"});

    private static List<String> SIMPLE_TYPE_PROPERTIES = Arrays.asList(new String[] { "baseType", "facets", "lexicalEnumeration", "lexicalPattern", "name", "namespace"});

    private static List<String> GROUP_DEFINITION_PROPERTIES = Arrays.asList(new String[] { "modelGroup", "name", "namespace"});

    private static List<String> ELEMENT_TYPE_PROPERTIES = Arrays.asList(new String[] { "baseType", "name", "namespace"});

    private static Map<String, String> DISPLAY_NAME_MAPPING = new HashMap<String, String>();

    private static String[] GETTER_PREFIX = new String[] { "get", "has", "is"};

    /*
     * initialize display names
     */
    static {
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("abstract", "Abstract");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("anonymous", "Anonymous");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("baseType", "Base type");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("contentType", "Content type");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("derivationMethod", "Derivation method");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("final", "Final");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("lexicalEnumeration", "Enumeration");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("lexicalPattern", "Pattern");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("name", "Name");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("contentType", "Content type");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("namespace", "Namespace");
        SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.put("modelGroup", "Model group");
    }

    public static Property[] getComplexTypeProperties(XSComplexTypeDefinition complexType) {
        List<Property> properties = new ArrayList<Property>();

        for (int i = 0; i < SchemaElementsPropertyFactory.COMPLEX_TYPE_PROPERTIES.size(); ++i) {

            final String propertyName = SchemaElementsPropertyFactory.COMPLEX_TYPE_PROPERTIES.get(i);

            properties.add(SchemaElementsPropertyFactory.createProperty(propertyName, complexType));
        }

        return properties.toArray(new Property[properties.size()]);
    }

    public static Property[] getElementProperties(XSElementDeclaration elementDeclaration) {
        List<Property> properties = new ArrayList<Property>();

        for (int i = 0; i < SchemaElementsPropertyFactory.ELEMENT_TYPE_PROPERTIES.size(); ++i) {

            final String propertyName = SchemaElementsPropertyFactory.ELEMENT_TYPE_PROPERTIES.get(i);

            properties.add(SchemaElementsPropertyFactory.createProperty(propertyName, elementDeclaration));
        }

        return properties.toArray(new Property[properties.size()]);
    }

    public static Property[] getModelGroupProperties(XSModelGroupDefinition modelGroup) {
        List<Property> properties = new ArrayList<Property>();

        for (int i = 0; i < SchemaElementsPropertyFactory.GROUP_DEFINITION_PROPERTIES.size(); ++i) {

            final String propertyName = SchemaElementsPropertyFactory.GROUP_DEFINITION_PROPERTIES.get(i);

            properties.add(SchemaElementsPropertyFactory.createProperty(propertyName, modelGroup));
        }

        return properties.toArray(new Property[properties.size()]);
    }

    public static Property[] getSimpleTypeProperties(XSSimpleTypeDefinition simpleType) {
        List<Property> properties = new ArrayList<Property>();
        for (int i = 0; i < SchemaElementsPropertyFactory.SIMPLE_TYPE_PROPERTIES.size(); ++i) {
            final String propertyName = SchemaElementsPropertyFactory.SIMPLE_TYPE_PROPERTIES.get(i);

            if ("facets".equals(propertyName)) {
                XSObjectList facets = simpleType.getFacets();
                for (int j = 0; j < facets.getLength(); ++j) {
                    XSFacet facet = (XSFacet) facets.item(j);
                    if (null == facets) {
                        continue;
                    }
                    DefaultProperty p = new DefaultProperty();
                    p.setName(String.valueOf(facet.getFacetKind()));
                    p.setDisplayName(SchemaElementsPropertyFactory.getNameForFacet(facet.getFacetKind()));
                    p.setValue(facet.getLexicalFacetValue());
                    p.setEditable(false);
                    properties.add(p);

                }
            }
            else {
                properties.add(SchemaElementsPropertyFactory.createProperty(propertyName, simpleType));
            }
        }
        return properties.toArray(new Property[properties.size()]);
    }

    private static DefaultProperty createProperty(String propertyName, Object value) {
        final String methodSuffix = propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);

        DefaultProperty p = new DefaultProperty();
        p.setName(propertyName);
        p.setDisplayName(SchemaElementsPropertyFactory.DISPLAY_NAME_MAPPING.get(propertyName));
        p.setEditable(false);
        try {
            for (String prefix : SchemaElementsPropertyFactory.GETTER_PREFIX) {
                String methodName = prefix + methodSuffix;
                try {
                    Method m = value.getClass().getMethod(methodName, null);
                    p.setType(m.getReturnType());
                    break;
                }
                catch (NoSuchMethodException e) {
                    SchemaElementsPropertyFactory.logger.debug("Cannot get return type for getter of property [" + propertyName + "] using method [" + methodName + "]");
                }
            }

        }
        catch (SecurityException e) {
            SchemaElementsPropertyFactory.logger.warn("Cannot get return type for getter of property [" + propertyName + "]");
            e.printStackTrace();
        }
        return p;
    }

    private static String getNameForFacet(short type) {
        switch (type) {
            case XSSimpleTypeDefinition.FACET_ENUMERATION:
                return "Enumeration";
            case XSSimpleTypeDefinition.FACET_FRACTIONDIGITS:
                return "Fraction digits";
            case XSSimpleTypeDefinition.FACET_LENGTH:
                return "Length";
            case XSSimpleTypeDefinition.FACET_MAXEXCLUSIVE:
                return "Max exclusive";
            case XSSimpleTypeDefinition.FACET_MAXINCLUSIVE:
                return "Max exclusive";
            case XSSimpleTypeDefinition.FACET_MAXLENGTH:
                return "Max length";
            case XSSimpleTypeDefinition.FACET_MINEXCLUSIVE:
                return "Min exclusive";
            case XSSimpleTypeDefinition.FACET_MININCLUSIVE:
                return "Min inclusive";
            case XSSimpleTypeDefinition.FACET_MINLENGTH:
                return "Min length";
            case XSSimpleTypeDefinition.FACET_NONE:
                return "None";
            case XSSimpleTypeDefinition.FACET_PATTERN:
                return "Pattern";
            case XSSimpleTypeDefinition.FACET_TOTALDIGITS:
                return "Total digits";
            case XSSimpleTypeDefinition.FACET_WHITESPACE:
                return "Whitspace";
            default:
                return String.valueOf(type);
        }

    }
}
