// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSAttributeUse;

import de.mindcrimeilab.util.Converter;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 *          Converts {@link XSAttributeUse} object to a {@link XSAttributeDeclration} object.
 */
public class XsAttributeExtractor implements Converter<XSAttributeUse, XSAttributeDeclaration> {

    @Override
    public XSAttributeDeclaration convert(XSAttributeUse object) {
        return (null == object) ? null : object.getAttrDeclaration();
    }
}
