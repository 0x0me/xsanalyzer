// $Id:XSModelHelper.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.impl.xs.XSGroupDecl;
import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSModelGroup;
import org.apache.xerces.xs.XSNamedMap;
import org.apache.xerces.xs.XSNamespaceItem;
import org.apache.xerces.xs.XSNamespaceItemList;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTerm;
import org.apache.xerces.xs.XSTypeDefinition;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public abstract class XSModelHelper {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    public static Set<? extends XSTypeDefinition> getComponents(XSModel model, List<XSNamespaceItem> namespaces) {
        short[] selectedTypes = { XSTypeDefinition.SIMPLE_TYPE, XSTypeDefinition.COMPLEX_TYPE};
        return XSModelHelper.getComponents(model, namespaces, selectedTypes);
    }

    /**
     * Returns all public and global types for a given set of namespaces
     * 
     * @param model
     *            schema
     * @param namespaces
     *            List of namespaces
     * @return
     */
    public static Set<? extends XSTypeDefinition> getComponents(XSModel model, List<XSNamespaceItem> namespaces, short[] selectedTypes) {
        final Set<XSTypeDefinition> tmp = new HashSet<XSTypeDefinition>();

        for (short type : selectedTypes) {
            for (XSNamespaceItem ns : namespaces) {

                XSNamedMap xsmap = model.getComponentsByNamespace(type, ns.getSchemaNamespace());

                if (xsmap != null) {
                    for (int i = 0; i < xsmap.getLength(); ++i) {
                        if (null != xsmap.item(i)) {
                            tmp.add((XSTypeDefinition) xsmap.item(i));
                        }
                    }
                }
            }
        }
        return Collections.unmodifiableSet(tmp);
    }

    /**
     * Returns all public and global elements for given set of namespaces
     * 
     * @param model
     *            schema
     * @param namespaces
     *            List of namespaces
     * @return
     */
    public static List<XSElementDeclaration> getElements(XSModel model, List<XSNamespaceItem> namespaces) {

        final List<XSElementDeclaration> elements = new LinkedList<XSElementDeclaration>();

        for (XSNamespaceItem ns : namespaces) {
            XSNamedMap xsmap = model.getComponentsByNamespace(XSConstants.ELEMENT_DECLARATION, ns.getSchemaNamespace());

            if (xsmap != null) {
                for (int i = 0; i < xsmap.getLength(); ++i) {
                    if (null != xsmap.item(i)) {
                        elements.add((XSElementDeclaration) xsmap.item(i));
                    }
                }

            }
        }
        return Collections.unmodifiableList(elements);
    }

    public static List<XSGroupDecl> getGroups(XSModel model, List<XSNamespaceItem> namespaces) {

        final List<XSGroupDecl> groups = new LinkedList<XSGroupDecl>();

        for (XSNamespaceItem ns : namespaces) {
            XSNamedMap xsmap = model.getComponentsByNamespace(XSConstants.MODEL_GROUP_DEFINITION, ns.getSchemaNamespace());

            if (xsmap != null) {
                for (int i = 0; i < xsmap.getLength(); ++i) {
                    if (null != xsmap.item(i)) {
                        groups.add((XSGroupDecl) xsmap.item(i));
                    }
                }

            }
        }
        return Collections.unmodifiableList(groups);
    }

    public static List<XSNamespaceItem> getNamespaceItemsAsList(XSModel model) {
        final XSNamespaceItemList namespaceItems = model.getNamespaceItems();
        final List<XSNamespaceItem> list = new ArrayList<XSNamespaceItem>(namespaceItems.getLength());
        for (int i = 0; i < namespaceItems.getLength(); ++i) {
            list.add(namespaceItems.item(i));
        }
        return list;
    }

    /**
     * Check if both values have the same name and namespace definition.
     * 
     * @param lhs
     * @param rhs
     * @return true if both have same name and namespace value
     */
    public static boolean isSameTypeDefinition(final XSObject lhs, final XSObject rhs) {
        final boolean ret;
        if (null == lhs && null == rhs) {
            ret = true;
        }
        else if (null != lhs && null != rhs) {
            EqualsBuilder equalsBuilder = new EqualsBuilder();
            ret = equalsBuilder.append(lhs.getName(), rhs.getName()).append(lhs.getNamespace(), rhs.getNamespace()).isEquals();
        }
        else {
            ret = false;
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    public static Collection<XSObject> flat(XSObjectList list) {
        Collection<XSObject> collection = new ArrayList<XSObject>();
        for (int i = 0; i < list.getLength(); i++) {
            final Object item = list.item(i);
            if (item instanceof XSParticle) {
                XSTerm term = ((XSParticle) item).getTerm();
                if (term instanceof XSModelGroup) {
                    collection.addAll(XSModelHelper.flat(((XSModelGroup) term).getParticles()));
                }

                else {
                    XSModelHelper.logger.debug("Adding element of class [" + item.getClass().getName() + "]");
                    collection.add((XSObject) item);
                }
            }
            else {
                XSModelHelper.logger.debug("Adding element of class [" + item.getClass().getName() + "]");
                collection.add((XSObject) item);
            }
        }
        return collection;
    }

    public static <T> void addComponents(XSNamedMap map, List<T> list) {
        for (int i = 0; i < map.getLength(); ++i) {
            list.add((T) map.item(i));
        }
    }

    /**
     * @param object
     * @return
     */
    public static XSTypeDefinition getBaseType(XSObject object) {
        final XSTypeDefinition type;
        switch (object.getType()) {
            case XSConstants.TYPE_DEFINITION:
                final XSTypeDefinition typedef = ((XSTypeDefinition) object);
                if (typedef.getTypeCategory() == XSTypeDefinition.SIMPLE_TYPE) {
                    type = XSModelHelper.getBaseType((XSSimpleTypeDefinition) typedef);
                }
                else {
                    type = typedef.getBaseType();
                }
                break;
            case XSConstants.ELEMENT_DECLARATION:
                type = ((XSElementDeclaration) object).getTypeDefinition();
                break;
            case XSConstants.ATTRIBUTE_DECLARATION:
                type = ((XSAttributeDeclaration) object).getTypeDefinition();
                break;
            case XSConstants.NOTATION_DECLARATION:
            case XSConstants.MODEL_GROUP_DEFINITION:
            default:
                throw new RuntimeException(XSModelHelper.class.getName() + ": Unknown Type [" + object.getType() + "]!");
        }
        return type;
    }

    public static XSTypeDefinition getBaseType(XSSimpleTypeDefinition stypedef) {
        return (stypedef.getVariety() == XSSimpleTypeDefinition.VARIETY_LIST) ? null : stypedef.getBaseType();
    }

    /**
     * Get target namespace from model.
     * 
     * @param model
     * @param schema
     * @return
     */
    public static XSNamespaceItem getTargetNamespace(XSModel model, File schema) {
        /*
         * Maybe simply use the dom/sax api?
         */
        XSNamespaceItemList namespaces = model.getNamespaceItems();
        final URI absolutePath = schema.getAbsoluteFile().toURI();
        for (int i = 0; i < namespaces.getLength(); ++i) {
            XSNamespaceItem namespace = namespaces.item(i);
            StringList documentLocations = namespace.getDocumentLocations();
            for (int j = 0; j < documentLocations.getLength(); ++j) {

                try {
                    URI fname = new URI(documentLocations.item(j));
                    XSModelHelper.logger.debug("Namespace [" + namespace.getSchemaNamespace() + "] file [" + fname + "] absolute path [" + absolutePath + "]");
                    if (absolutePath.equals(fname)) { return namespace; }
                }
                catch (URISyntaxException e) {
                    e.printStackTrace();
                    XSModelHelper.logger.error("Cannot construct URI from document location [" + documentLocations.item(i) + "] for namespace [" + namespace + "].", e);
                }

            }
        }
        return null;
    }

    public static String compositorToString(short compositor) {
        final String ret;
        switch (compositor) {
            case XSModelGroup.COMPOSITOR_ALL:
                ret = "ALL";
                break;
            case XSModelGroup.COMPOSITOR_CHOICE:
                ret = "CHOICE";
                break;
            case XSModelGroup.COMPOSITOR_SEQUENCE:
                ret = "SEQUENCE";
                break;
            default:
                throw new RuntimeException("Unexpected branch selection - Not implemented");
        }
        return ret;
    }
}
