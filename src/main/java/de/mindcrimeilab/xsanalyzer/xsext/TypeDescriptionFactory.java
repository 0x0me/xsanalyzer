// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.xsext;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTypeDefinition;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeDescriptionFactory {

    private final MessageDigest messageDigest;

    public TypeDescriptionFactory() throws NoSuchAlgorithmException {
        messageDigest = MessageDigest.getInstance("MD5");
    }

    public TypeDescriptionFactory(String algorithm) throws NoSuchAlgorithmException {
        messageDigest = MessageDigest.getInstance(algorithm);
    }

    public String generateTypeDescriptionSignature(XSTypeDefinition typedef) {
        final String signature;
        switch (typedef.getTypeCategory()) {
            case XSTypeDefinition.SIMPLE_TYPE:
                signature = SimpleTypeDescription.getSignatureDigest((XSSimpleTypeDefinition) typedef, messageDigest);
                break;
            case XSTypeDefinition.COMPLEX_TYPE:
                signature = ComplexTypeDescription.getSignatureDigest((XSComplexTypeDefinition) typedef, messageDigest);
                break;
            // throw new RuntimeException("Not implemented yet (complex type)!");
            default:
                throw new RuntimeException("Unexpected type category [" + typedef.getTypeCategory() + "]");
        }
        return signature;
    }
}
