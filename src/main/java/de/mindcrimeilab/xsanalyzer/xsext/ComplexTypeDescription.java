// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.xsext;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSAttributeUse;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSModelGroup;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTerm;
import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.util.CollectionsHelper;
import de.mindcrimeilab.xsanalyzer.util.NameComparator;
import de.mindcrimeilab.xsanalyzer.util.XSListHelper;
import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;
import de.mindcrimeilab.xsanalyzer.util.XsAttributeExtractor;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class ComplexTypeDescription extends AbstractTypeDescription {

    public static String getSignatureDigest(XSComplexTypeDefinition ctypedef, MessageDigest md) {
        String signature = ComplexTypeDescription.getSignature(ctypedef);
        String digest = AbstractTypeDescription.getSignatureDigest(signature, md);
        AbstractTypeDescription.logger.debug("Signature of type [{" + ctypedef.getNamespace() + "}:" + ctypedef.getName() + "|" + signature.toString() + "] is [" + digest + "]");
        return digest;
    }

    public static String getSignature(XSComplexTypeDefinition ctypedef) {

        StringBuilder signature = new StringBuilder();
        XSTypeDefinition baseType = AbstractTypeDescription.getBaseTypeRecursive(ctypedef);

        switch (ctypedef.getContentType()) {
            case XSComplexTypeDefinition.CONTENTTYPE_SIMPLE:
                if (null == ctypedef.getParticle()) {
                    signature.append(SimpleTypeDescription.getSignature(ctypedef.getSimpleType()));
                }
                else {
                    AbstractTypeDescription.appendBaseType(signature, baseType);
                    signature.append(ComplexTypeDescription.inspectComplexType(ctypedef));
                }
                break;
            case XSComplexTypeDefinition.CONTENTTYPE_EMPTY:
                AbstractTypeDescription.appendBaseType(signature, baseType);
                AbstractTypeDescription.logger.debug("{" + ctypedef.getNamespace() + "}" + ctypedef.getName() + " empty content!");
                break;
            case XSComplexTypeDefinition.CONTENTTYPE_ELEMENT:
                AbstractTypeDescription.appendBaseType(signature, baseType);
                signature.append(ComplexTypeDescription.inspectComplexType(ctypedef));
                break;
            default:
                AbstractTypeDescription.appendBaseType(signature, baseType);
                signature.append(ctypedef.getName()).append(ctypedef.getNamespace());
                AbstractTypeDescription.logger.warn("{" + ctypedef.getNamespace() + "}" + ctypedef.getName() + " undefined content type.");
                break;

        }

        List<XSAttributeDeclaration> attributes = ComplexTypeDescription.getAttributes(ctypedef);
        if (null != attributes) {
            for (XSAttributeDeclaration attributeDeclaration : attributes) {
                signature.append(SimpleTypeDescription.getSignature(attributeDeclaration.getTypeDefinition()));
            }
        }

        return signature.toString();
    }

    private static String inspectComplexType(XSComplexTypeDefinition ctypedef) {
        StringBuilder sig = new StringBuilder();
        XSParticle particle = ctypedef.getParticle();
        if (null != particle) {
            XSTerm term = particle.getTerm();
            sig.append(ComplexTypeDescription.inspectTerm(term));
        }
        return sig.toString();
    }

    /**
     * @param ctypedef
     * @param term
     * @param signature
     */
    private static String inspectTerm(XSTerm term) {
        StringBuilder sig = new StringBuilder();
        switch (term.getType()) {
            case XSConstants.MODEL_GROUP:
                AbstractTypeDescription.logger.debug("==> found model group");
                XSModelGroup group = (XSModelGroup) term;
                sig.append(":").append(XSModelHelper.compositorToString(group.getCompositor()));
                sig.append("[");
                XSObjectList list = group.getParticles();
                List<String> subSignatures = new ArrayList<String>();
                for (int i = 0; i < list.getLength(); ++i) {
                    XSParticle part = (XSParticle) list.item(i);
                    XSTerm item = part.getTerm();
                    AbstractTypeDescription.logger.debug("==--> embedded [{" + item.getNamespace() + "}:" + item.getName() + "]");
                    subSignatures.add(ComplexTypeDescription.inspectTerm(item));
                }

                /*
                 * choice elements may differ in their definition order describing the same content model for example:
                 * {@code
                 * 
                 * <xs:choice>
                 * 
                 * <xs:element name="eihter" type="xs:string"/>
                 * 
                 * <xs:element name="or" type="xs:float"/>
                 * 
                 * </xs:choice>
                 * 
                 * }
                 * 
                 * may be equivalent to {@code
                 * 
                 * <xs:choice>
                 * 
                 * <xs:element name="or" type="xs:float"/>
                 * 
                 * <xs:element name="either" type="xs:string"/>
                 * 
                 * </xs:choice>
                 * 
                 * }
                 */
                if (group.getCompositor() == XSModelGroup.COMPOSITOR_CHOICE) {
                    Collections.sort(subSignatures);
                }
                for (String subSig : subSignatures) {
                    sig.append(subSig).append("|");
                }
                sig.append("]");
                break;
            case XSConstants.ELEMENT_DECLARATION:
                XSElementDeclaration elementDeclaration = (XSElementDeclaration) term;
                final String tempSig;
                if (XSTypeDefinition.COMPLEX_TYPE == elementDeclaration.getTypeDefinition().getTypeCategory()) {
                    tempSig = ComplexTypeDescription.getSignature((XSComplexTypeDefinition) elementDeclaration.getTypeDefinition());
                }
                else {
                    tempSig = SimpleTypeDescription.getSignature((XSSimpleTypeDefinition) elementDeclaration.getTypeDefinition());
                }
                sig.append(tempSig);
                break;
            case XSConstants.WILDCARD:
                AbstractTypeDescription.logger.warn("==> found wildcard");
                break;
            default:
                AbstractTypeDescription.logger.warn("-------------- UNKNOWN PARTICLE TYPE OF TYPE [" + term.getType() + "]");
                throw new RuntimeException("Unexpected branch selection - Not implemented");
        }
        return sig.toString();
    }

    /**
     * @param ctypedef
     * @return
     */
    private static List<XSAttributeDeclaration> getAttributes(XSComplexTypeDefinition ctypedef) {
        List<XSAttributeDeclaration> attributes = null;
        XSObjectList attributesObjectList = ctypedef.getAttributeUses();
        if (0 < attributesObjectList.getLength()) {
            List temp = XSListHelper.asList(attributesObjectList);
            attributes = new ArrayList<XSAttributeDeclaration>();
            CollectionsHelper.copyCollection((Collection<XSAttributeUse>) temp, attributes, new XsAttributeExtractor());
            Collections.sort(attributes, new NameComparator());
        }
        return attributes;
    }

}
