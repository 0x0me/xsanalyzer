// $Id:SimpleTypeDescription.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.xsext;

import java.security.MessageDigest;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSFacet;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.xsanalyzer.XSAnalyzerConstants;
import de.mindcrimeilab.xsanalyzer.util.XSListHelper;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class SimpleTypeDescription extends AbstractTypeDescription {

    private SimpleTypeDescription() {
        // empty
    }

    public static String getSignatureDigest(XSSimpleTypeDefinition stypedef, MessageDigest md) {
        String signature = SimpleTypeDescription.getSignature(stypedef);
        String digist = AbstractTypeDescription.getSignatureDigest(signature.toString(), md);
        AbstractTypeDescription.logger.debug("Signature of type [{" + stypedef.getNamespace() + "}:" + stypedef.getName() + "|" + signature.toString() + "] is [" + digist + "]");
        return digist;

    }

    /**
     * @param stypedef
     * @return
     */
    static String getSignature(XSSimpleTypeDefinition stypedef) {
        StringBuilder signature = new StringBuilder();

        // ignore name and namespace
        // XXX get base type (do we really need this?)
        XSTypeDefinition baseType = SimpleTypeDescription.getBaseTypeRecursive(stypedef);
        AbstractTypeDescription.appendBaseType(signature, baseType);

        // get facets
        Map<Short, String> facets = SimpleTypeDescription.getFacets(stypedef);
        StringList patterns = stypedef.getLexicalPattern();

        for (Short facet : XSAnalyzerConstants.USED_FACETS) {
            signature.append(facet).append(":");
            signature.append(facets.get(facet)).append(":");
        }
        for (int i = 0; i < patterns.getLength(); ++i) {
            signature.append(patterns.item(i)).append(":");
        }

        // handle enumeration
        StringList enumeration = stypedef.getLexicalEnumeration();
        List<String> enumCollection = XSListHelper.asList(enumeration);
        Collections.sort(enumCollection);
        for (String value : enumCollection) {
            signature.append(value).append(":");
        }
        return signature.toString();
    }

    private static Map<Short, String> getFacets(XSSimpleTypeDefinition stypedef) {
        Map<Short, String> facets = new HashMap<Short, String>();
        XSObjectList facetList = stypedef.getFacets();
        for (int i = 0; i < facetList.getLength(); ++i) {
            XSFacet facet = (XSFacet) facetList.item(i);
            facets.put(facet.getFacetKind(), facet.getLexicalFacetValue());
        }

        String length = facets.get(XSSimpleTypeDefinition.FACET_LENGTH);
        String minLength = facets.get(XSSimpleTypeDefinition.FACET_MINLENGTH);
        String maxLength = facets.get(XSSimpleTypeDefinition.FACET_MAXLENGTH);
        if (null == length) {
            // contract maxLength and minLength to length if undefined and minLength = maxLength
            if (NumberUtils.toInt(minLength) == NumberUtils.toInt(maxLength)) {
                facets.put(XSSimpleTypeDefinition.FACET_LENGTH, minLength);
            }
        }
        else {
            // expand length to maxLength and minLength
            facets.put(XSSimpleTypeDefinition.FACET_MINLENGTH, length);
            facets.put(XSSimpleTypeDefinition.FACET_MAXLENGTH, length);
        }

        // set defaults
        for (int i = 0; i < XSAnalyzerConstants.USED_FACETS.length; ++i) {
            String value = facets.get(XSAnalyzerConstants.USED_FACETS[i]);
            if (null == value) {
                facets.put(XSAnalyzerConstants.USED_FACETS[i], XSAnalyzerConstants.DEFAULTS[i]);
                AbstractTypeDescription.logger.debug("Setting default [" + XSAnalyzerConstants.USED_FACETS[i] + "] to [" + XSAnalyzerConstants.DEFAULTS[i] + "]");
            }
        }

        return facets;
    }
}
