// $Id:AnonymousTypeFactory.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.xsext;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.xerces.xs.XSTypeDefinition;

/**
 * Factory class to dynamically change behavior of {@code XSTypeDefinition.getName()} of anonymous types.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class AnonymousTypeFactory {

    private final static AnonymousTypeFactory factoryInstance = new AnonymousTypeFactory();

    private final Map<XSTypeDefinition, XSTypeDefinition> knownTypes;

    private long counter;

    private AnonymousTypeFactory() {
        knownTypes = new HashMap<XSTypeDefinition, XSTypeDefinition>();
        counter = 0;
    }

    public boolean containsType(XSTypeDefinition type) {
        return knownTypes.containsKey(type);
    }

    public Object addType(XSTypeDefinition type, XSTypeDefinition wrappedType) {
        return knownTypes.put(type, wrappedType);
    }

    public XSTypeDefinition getType(XSTypeDefinition type) {
        return knownTypes.get(type);
    }

    /**
     * Proxy class
     * 
     * @author Michael Engelhardt<me@mindcrime-ilab.de>
     * @author $Author:me $
     * @version $Revision:62 $
     * 
     * @param <T>
     *            instance of {@code XSTypeDefinition}
     */
    private static class AnonymousTypeProxy<T extends XSTypeDefinition> implements InvocationHandler {

        private final T instance;

        private final String suffix;

        /**
         * ctor()
         * 
         * @param instance
         */
        public AnonymousTypeProxy(T instance) {
            this(instance, null);
        }

        public AnonymousTypeProxy(T instance, String suffix) {
            this.instance = instance;
            this.suffix = suffix;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object, java.lang.reflect.Method,
         * java.lang.Object[])
         */
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            final Object result;
            String meth = method.getName();
            if ("getName".equals(meth)) {
                Object tmp = method.invoke(instance, args);
                if (null == tmp) {
                    tmp = getTypeName();
                }
                result = tmp;
            }
            else {
                result = method.invoke(instance, args);
            }
            return result;
        }

        /**
         * @return
         */
        private String getTypeName() {
            return (StringUtils.isNotEmpty(this.suffix)) ? new String("#AnonymousType_" + suffix) : new String("#AnonymousType");
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return instance.hashCode();
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            return instance.equals(obj);
        }
    }

    /**
     * Factory method - create proxy classes
     * 
     * @param <T>
     *            instance of {@code XSTypeDefinition}
     * @param instance
     * @return proxy instance
     */
    @SuppressWarnings("unchecked")
    public static <T extends XSTypeDefinition> T getProxy(T instance) {
        final T result;
        if (AnonymousTypeFactory.factoryInstance.knownTypes.containsKey(instance)) {
            result = (T) AnonymousTypeFactory.factoryInstance.getType(instance);
        }
        else {
            Class[] interfaces = instance.getClass().getInterfaces();
            result = (T) Proxy.newProxyInstance(AnonymousTypeProxy.class.getClassLoader(), interfaces, new AnonymousTypeProxy<T>(instance, String.valueOf(++AnonymousTypeFactory.factoryInstance.counter)));
            AnonymousTypeFactory.factoryInstance.addType(instance, result);
        }
        return result;
    }
}
