// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008,2009,2010 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSNamespaceItem;
import org.apache.xerces.xs.XSNamespaceItemList;
import org.apache.xerces.xs.XSTypeDefinition;
import org.springframework.context.MessageSource;
import org.springframework.richclient.application.Application;

import de.mindcrimeilab.xsanalyzer.actions.EventType;
import de.mindcrimeilab.xsanalyzer.actions.XsAnalyzerApplicationEvent;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeUsageAnalyzer extends AbstractAnalyzer implements Analyzer {

    private final TypeUsageWorker typeUsageFinder;

    private final UnusedTypeFinderWorker unusedTypeFinder;

    private final Collection<XsComponentWorker> workers;

    /**
     * 
     */
    public TypeUsageAnalyzer() {
        unusedTypeFinder = new UnusedTypeFinderWorker();
        typeUsageFinder = new TypeUsageWorker();
        workers = new LinkedList<XsComponentWorker>();
        workers.add(unusedTypeFinder);
        workers.add(typeUsageFinder);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#getWorkers()
     */
    @Override
    public Collection<XsComponentWorker> getWorkers() {
        return workers;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#analyze(de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel)
     */
    @Override
    public void analyze(final XsAnalyzerApplicationModel model) {
        setup(model);
        final XSModel xsmodel = model.getSchemaModel();
        XsModelWalker walker = new XsModelWalker();
        walker.addPropertyChangeListener(getProgessListener());
        walker.addWorker(unusedTypeFinder);
        walker.addWorker(typeUsageFinder);
        walker.walkModel(xsmodel);
        walker.removePropertyChangeListener(getProgessListener());
        this.setResult(model);
    }

    @Override
    public void setup(final XsAnalyzerApplicationModel model) {
        XSModel xsmodel = model.getSchemaModel();
        final XSNamespaceItemList nsList = xsmodel.getNamespaceItems();
        final List<XSNamespaceItem> namespaceItems = new LinkedList<XSNamespaceItem>();

        for (int i = 0; i < nsList.getLength(); ++i) {
            XSNamespaceItem item = nsList.item(i);
            namespaceItems.add(item);
        }

        Set<? extends XSTypeDefinition> definedTypes = XSModelHelper.getComponents(xsmodel, namespaceItems);

        unusedTypeFinder.setDefinedTypes(definedTypes);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#showResult()
     */
    @Override
    public void showResult() {
        Application.instance().getActiveWindow().getPage().showView("typeUsageView");
        Application.instance().getApplicationContext().publishEvent(new XsAnalyzerApplicationEvent(EventType.TYPE_USAGE, this));
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#setResult(de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel)
     */
    @Override
    public final void setResult(XsAnalyzerApplicationModel model) {
        model.setUnusedTypes(unusedTypeFinder.getUnusedTypes());
        model.setUsedTypes(typeUsageFinder.getUsedBy());
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.Analyzer#getLabelProvider()
     */
    @Override
    public LabelProvider getLabelProvider() {
        return new TypeUsageLabelProvider();
    }

    private final static class TypeUsageLabelProvider implements LabelProvider {

        private static final MessageSource MESSAGE_SOURCE = (MessageSource) Application.services().getService(MessageSource.class);

        private static String getMessage(String code) {
            return TypeUsageLabelProvider.MESSAGE_SOURCE.getMessage(code, null, code, Locale.getDefault());
        }

        /*
         * (non-Javadoc)
         * 
         * @see de.mindcrimeilab.xsanalyzer.LabelProvider#getLabel()
         */
        @Override
        public String getLabel() {
            return getMessage("typeUsageCommand.label");
        }

        /*
         * (non-Javadoc)
         * 
         * @see de.mindcrimeilab.xsanalyzer.LabelProvider#getCaption()
         */
        @Override
        public String getCaption() {
            return getMessage("typeUsageCommand.caption");
        }

        /*
         * (non-Javadoc)
         * 
         * @see de.mindcrimeilab.xsanalyzer.LabelProvider#getDescription()
         */
        @Override
        public String getDescription() {
            return getMessage("typeUsageCommand.description");
        }
    }
}
