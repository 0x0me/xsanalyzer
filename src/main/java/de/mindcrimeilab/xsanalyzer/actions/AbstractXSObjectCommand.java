// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import java.util.HashMap;
import java.util.Map;

import org.apache.xerces.xs.XSObject;
import org.springframework.binding.value.ValueModel;
import org.springframework.richclient.command.ActionCommand;

import de.mindcrimeilab.util.Accessor;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * 
 * @author $Author$
 * 
 * @version $Revision$
 */
public abstract class AbstractXSObjectCommand extends ActionCommand {

    private ValueModel valueModel;

    private Accessor accessor;

    /**
     * @param commandId
     */
    public AbstractXSObjectCommand(String commandId, ValueModel valueModel, Accessor accessor) {
        super(commandId);
        this.valueModel = valueModel;
        this.accessor = accessor;
    }

    /**
     * @return the accessor
     */
    public Accessor<XSObject, ? super Object> getAccessor() {
        return accessor;
    }

    /**
     * @param accessor
     *            the accessor to set
     */
    public void setAccessor(Accessor accessor) {
        this.accessor = accessor;
    }

    /**
     * @return the valueModel
     */
    public ValueModel getValueModel() {
        return valueModel;
    }

    /**
     * @param valueModel
     *            the valueModel to set
     */
    public void setValueModel(ValueModel valueModel) {
        this.valueModel = valueModel;
    }

    protected abstract void executeWrappedExecutor(Map<String, XSObject> parameter);

    @Override
    protected void doExecuteCommand() {
        Map<String, XSObject> parameter = new HashMap<String, XSObject>();
        final Object object = ((Object[]) valueModel.getValue())[0];
        final XSObject target;
        if (null != accessor) {
            target = (XSObject) accessor.getValue(object);
        }
        else {
            target = (XSObject) object;
        }

        assert target instanceof XSObject;

        parameter.put(TypeHierarchyExecutor.TARGET_OBJECT, target);
        logger.debug("Getting value from ValueModel [" + valueModel.getValue() + "]");
        executeWrappedExecutor(parameter);
    }

}