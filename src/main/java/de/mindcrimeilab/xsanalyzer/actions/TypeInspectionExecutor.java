// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import java.util.Map;

import org.apache.xerces.xs.XSObject;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.command.support.AbstractActionCommandExecutor;

import de.mindcrimeilab.xsanalyzer.ui.InspectTypeDialog;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeInspectionExecutor extends AbstractActionCommandExecutor {

    public static final String TARGET_OBJECT = "TARGET_OBJECT";

    @SuppressWarnings("unchecked")
    @Override
    public void execute(Map parameters) {
        InspectTypeDialog dialog = new InspectTypeDialog((XSObject) parameters.get(TARGET_OBJECT));
        dialog.setParentComponent(Application.instance().getActiveWindow().getControl());
        dialog.showDialog();
    }
}
