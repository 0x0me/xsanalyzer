// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.command.support.ApplicationWindowAwareCommand;
import org.springframework.richclient.dialog.ApplicationDialog;
import org.springframework.richclient.dialog.MessageDialog;

import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.ui.RunMultipleDialog;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class RunMultipleCommand extends ApplicationWindowAwareCommand {

    private static final String ID = "runMultipleCommand";

    /** logger */
    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    public RunMultipleCommand() {
        super(RunMultipleCommand.ID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.command.ActionCommand#doExecuteCommand()
     */
    @Override
    protected void doExecuteCommand() {
        logger.debug("Execute RunMultipleCommand");
        XsAnalyzerApplicationModel model = (XsAnalyzerApplicationModel) Application.instance().getApplicationContext().getBean("applicationModel");
        final ApplicationDialog dlg;
        if (null == model.getSchemaModel()) {
            dlg = new MessageDialog("Warning", "Please load a XML Schema file first.");
        }
        else {
            dlg = new RunMultipleDialog();
        }
        dlg.showDialog();
    }

}
