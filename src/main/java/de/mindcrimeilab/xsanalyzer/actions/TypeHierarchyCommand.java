// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import java.util.Map;

import org.apache.xerces.xs.XSObject;
import org.springframework.binding.value.ValueModel;
import org.springframework.richclient.command.ParameterizableActionCommandExecutor;

import de.mindcrimeilab.util.Accessor;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public final class TypeHierarchyCommand extends AbstractXSObjectCommand {

    private final ParameterizableActionCommandExecutor executer = new TypeHierarchyExecutor();

    public TypeHierarchyCommand() {
        this(null, null);
    }

    public TypeHierarchyCommand(ValueModel valueModel) {
        this(valueModel, null);
    }

    public TypeHierarchyCommand(ValueModel valueModel, Accessor accessor) {
        super("typeHierarchyCommand", valueModel, accessor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.actions.AbstractXSObjectCommand#executeWrappedExecutor(java.util.Map)
     */
    @Override
    protected void executeWrappedExecutor(Map<String, XSObject> parameter) {
        executer.execute(parameter);
    }

}
