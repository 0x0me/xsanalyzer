// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import java.util.Map;

import org.apache.xerces.xs.XSObject;
import org.springframework.binding.value.ValueModel;
import org.springframework.richclient.command.ParameterizableActionCommandExecutor;

import de.mindcrimeilab.util.Accessor;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeInspectionCommand extends AbstractXSObjectCommand {

    private final ParameterizableActionCommandExecutor executer = new TypeInspectionExecutor();

    public TypeInspectionCommand() {
        this(null, null);
    }

    public TypeInspectionCommand(ValueModel valueModel) {
        this(valueModel, null);
    }

    public TypeInspectionCommand(ValueModel valueModel, Accessor accessor) {
        super("typeInspectionCommand", valueModel, accessor);

    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.actions.AbstractXSObjectCommand#executeWrappedExecutor(java.util.Map)
     */
    @Override
    protected void executeWrappedExecutor(Map<String, XSObject> parameter) {
        executer.execute(parameter);
    }

}
