// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import javax.swing.JFileChooser;

import org.springframework.richclient.application.Application;
import org.springframework.richclient.command.support.ApplicationWindowAwareCommand;
import org.springframework.richclient.settings.Settings;
import org.springframework.richclient.settings.support.Memento;

import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.util.JFileChooserMemento;
import de.mindcrimeilab.xsanalyzer.util.XSAnalyzerDefaultFileFilter;
import de.mindcrimeilab.xsanalyzer.util.XsModelFactory;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class OpenCommand extends ApplicationWindowAwareCommand {

    private static final String ID = "openCommand";

    private Settings settings;

    public OpenCommand() {
        super(OpenCommand.ID);
    }

    /**
     * @return the settings
     */
    public Settings getSettings() {
        return settings;
    }

    /**
     * @param settings
     *            the settings to set
     */
    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.command.ActionCommand#doExecuteCommand()
     */
    @Override
    protected void doExecuteCommand() {
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new XSAnalyzerDefaultFileFilter());

        Memento memento = new JFileChooserMemento(jfc);
        memento.restoreState(settings);

        int result = jfc.showOpenDialog(getParentWindowControl());
        if (JFileChooser.APPROVE_OPTION == result) {
            memento.saveState(settings);
            XsAnalyzerApplicationModel appmodel = (XsAnalyzerApplicationModel) Application.instance().getApplicationContext().getBean("applicationModel");
            appmodel.setSchemaFile(jfc.getSelectedFile());
            appmodel.setSchemaModel(XsModelFactory.createXsModel(appmodel.getSchemaFile()));
            Application.instance().getApplicationContext().publishEvent(new XsAnalyzerApplicationEvent(EventType.OPEN, this));
        }

    }
}
