// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import java.util.Map;

import org.apache.xerces.xs.XSObject;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.command.support.AbstractActionCommandExecutor;

import de.mindcrimeilab.xsanalyzer.ui.TypeHierarchyDialog;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeHierarchyExecutor extends AbstractActionCommandExecutor {

    public static final String TARGET_OBJECT = "TARGET_OBJECT";

    @SuppressWarnings("unchecked")
    @Override
    public void execute(Map parameters) {
        TypeHierarchyDialog dialog = new TypeHierarchyDialog((XSObject) parameters.get(TypeHierarchyExecutor.TARGET_OBJECT));
        dialog.setParentComponent(Application.instance().getActiveWindow().getControl());
        dialog.showDialog();
    }
}
