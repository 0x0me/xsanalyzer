// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.command.support.ApplicationWindowAwareCommand;

import de.mindcrimeilab.swing.util.VisualProgressWorker;
import de.mindcrimeilab.xsanalyzer.Analyzer;
import de.mindcrimeilab.xsanalyzer.SameNameDifferentTypeAnalyzer;
import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SameNameDifferentTypeCommand extends ApplicationWindowAwareCommand {

    private static final String ID = "sameNameDifferentTypeCommand";

    /** logger */
    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    /**
     * ctor()
     */
    public SameNameDifferentTypeCommand() {
        super(SameNameDifferentTypeCommand.ID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.command.ActionCommand#doExecuteCommand()
     */
    @Override
    protected void doExecuteCommand() {
        final XsAnalyzerApplicationModel model = (XsAnalyzerApplicationModel) Application.instance().getApplicationContext().getBean("applicationModel");
        final JFrame parent = Application.instance().getActiveWindow().getControl();

        final VisualProgressWorker<Analyzer, Void> worker = new VisualProgressWorker<Analyzer, Void>(parent) {

            @Override
            protected Analyzer doInBackground() throws Exception {

                final PropertyChangeListener listener = new PropertyChangeListener() {

                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        setDetailMessage("Examining type:" + evt.getNewValue());
                    }

                };

                final Analyzer analyzer = new SameNameDifferentTypeAnalyzer();
                analyzer.setProgressListener(listener);
                analyzer.analyze(model);
                return analyzer;
            }

            @Override
            protected void done() {
                try {
                    Analyzer analyzer = get();
                    analyzer.showResult();

                }
                catch (InterruptedException e) {
                    logger.error("Failed to analyze schema for same names with different types.", e);
                    e.printStackTrace();
                }
                catch (ExecutionException e) {
                    logger.error("Failed to analyze schema for same names with different types.", e);
                    e.printStackTrace();
                }
                finally {
                    super.done();
                }
            }

        };
        worker.setTitle("Operation in progress...");
        worker.setActionText("Searching for similar type definitions");
        worker.showProgressDialog();
        worker.execute();
    }
}
