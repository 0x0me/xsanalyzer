// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.actions;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.richclient.application.Application;
import org.springframework.richclient.command.support.ApplicationWindowAwareCommand;

import de.mindcrimeilab.xsanalyzer.model.XsAnalyzerApplicationModel;
import de.mindcrimeilab.xsanalyzer.util.XsModelFactory;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class ReloadCommand extends ApplicationWindowAwareCommand {

    private static final String ID = "reloadCommand";

    /** logger */
    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    public ReloadCommand() {
        super(ReloadCommand.ID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.richclient.command.ActionCommand#doExecuteCommand()
     */
    @Override
    protected void doExecuteCommand() {
        logger.debug("Reload command called!");
        XsAnalyzerApplicationModel appmodel = (XsAnalyzerApplicationModel) Application.instance().getApplicationContext().getBean("applicationModel");
        final File schemaFile = appmodel.getSchemaFile();
        if (null != schemaFile) {
            logger.debug("Reloading [" + schemaFile + "]");
            appmodel.setSchemaModel(XsModelFactory.createXsModel(schemaFile));
            Application.instance().getApplicationContext().publishEvent(new XsAnalyzerApplicationEvent(EventType.OPEN, this));
        }
        else logger.info("Reloading impossible - no file loaded...");
    }

}
