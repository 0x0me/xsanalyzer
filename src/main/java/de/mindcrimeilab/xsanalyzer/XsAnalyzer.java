// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package de.mindcrimeilab.xsanalyzer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.richclient.application.ApplicationLauncher;

/**
 * XsAnalyzer - Main application class.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XsAnalyzer {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    /**
     * Main routine for the simple sample application.
     * 
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(XSAnalyzerConstants.GPL_COPYLEFT_NOTICE);
        XsAnalyzer.logger.info("XsAnalyzer starting up");

        // In order to launch the platform, we have to construct an
        // application context that defines the beans (services) and
        // wiring. This is pretty much straight Spring.
        //
        // Part of this configuration will indicate the initial page to be
        // displayed.

        String rootContextDirectoryClassPath = "/ctx";

        // The startup context defines elements that should be available
        // quickly such as a splash screen image.

        String startupContextPath = rootContextDirectoryClassPath + "/richclient-startup-context.xml";

        String richclientApplicationContextPath = rootContextDirectoryClassPath + "/richclient-application-context.xml";

        // The ApplicationLauncher is responsible for loading the contexts,
        // presenting the splash screen, initializing the Application
        // singleton instance, creating the application window to display
        // the initial page.
        try {
            new ApplicationLauncher(startupContextPath, new String[] { richclientApplicationContextPath});
        }
        catch (RuntimeException e) {
            XsAnalyzer.logger.error("RuntimeException during startup", e);
        }
    }

}
