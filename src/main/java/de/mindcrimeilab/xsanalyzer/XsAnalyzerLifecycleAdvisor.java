// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package de.mindcrimeilab.xsanalyzer;

import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsEnvironment;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.richclient.application.ApplicationWindow;
import org.springframework.richclient.application.config.ApplicationWindowConfigurer;
import org.springframework.richclient.application.config.DefaultApplicationLifecycleAdvisor;
import org.springframework.richclient.application.statusbar.StatusBar;
import org.springframework.richclient.settings.Settings;

/**
 * XsAnalyzerLifecycleAdvisor - created by spring-rich
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XsAnalyzerLifecycleAdvisor extends DefaultApplicationLifecycleAdvisor {

    private final Log logger = LogFactory.getLog(getClass());

    private Settings settings;

    private StatusBar statusbar = null;

    /**
     * This method is called prior to the opening of an application window. Note at this point the window control has
     * not been created. This hook allows programmatic control over the configuration of the window (by setting
     * properties on the configurer) and it provides a hook where code that needs to be executed prior to the window
     * opening can be plugged in (like a startup wizard, for example).
     * 
     * @param configurer
     *            The application window configurer
     */
    @Override
    public void onPreWindowOpen(ApplicationWindowConfigurer configurer) {

        // If you override this method, it is critical to allow the superclass
        // implementation to run as well.
        super.onPreWindowOpen(configurer);

        // Uncomment to hide the menubar, toolbar, or alter window size...
        // configurer.setShowMenuBar(false);
        // configurer.setShowToolBar(false);
        configurer.setShowStatusBar(true);

        DisplayMode dm = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();

        // start almost maximized - 10%
        final int maxWidth = dm.getWidth();
        final int maxHeight = dm.getHeight();
        configurer.setInitialSize(new Dimension(maxWidth - (maxWidth / 10), maxHeight - (maxHeight / 10)));

        // start centered...
        // Point p = new Point();
        // p.x = (maxWidth / 2) - (getWidth() / 2);
        // p.y = (maxHeight / 2) - (getHeight() / 2);
        // this.setLocation(p);
    }

    /**
     * Called just after the command context has been internalized. At this point, all the commands for the window have
     * been created and are available for use. If you need to force the execution of a command prior to the display of
     * an application window (like a login command), this is where you'd do it.
     * 
     * @param window
     *            The window who's commands have just been created
     */
    @Override
    public void onCommandsCreated(ApplicationWindow window) {
        if (logger.isInfoEnabled()) {
            logger.info("onCommandsCreated( windowNumber=" + window.getNumber() + " )");
        }
    }

    /**
     * Called after the actual window control has been created.
     * 
     * @param window
     *            The window being processed
     */
    @Override
    public void onWindowCreated(ApplicationWindow window) {
        if (logger.isInfoEnabled()) {
            logger.info("onWindowCreated( windowNumber=" + window.getNumber() + " )");
        }
    }

    /**
     * Called immediately after making the window visible.
     * 
     * @param window
     *            The window being processed
     */
    @Override
    public void onWindowOpened(ApplicationWindow window) {
        if (logger.isInfoEnabled()) {
            logger.info("onWindowOpened( windowNumber=" + window.getNumber() + " )");
        }
    }

    /**
     * Called when the window is being closed. This hook allows control over whether the window is allowed to close. By
     * returning false from this method, the window will not be closed.
     * 
     * @return boolean indicator if window should be closed. <code>true</code> to allow the close, <code>false</code> to
     *         prevent the close.
     */
    @Override
    public boolean onPreWindowClose(ApplicationWindow window) {
        if (logger.isInfoEnabled()) {
            logger.info("onPreWindowClose( windowNumber=" + window.getNumber() + " )");
        }
        return true;
    }

    /**
     * Called when the application has fully started. This is after the initial application window has been made
     * visible.
     */
    @Override
    public void onPostStartup() {
        if (logger.isInfoEnabled()) {
            logger.info("onPostStartup()");
        }
        if (null != settings) {
            try {
                settings.load();
                logger.info("Settings successfully loaded");
            }
            catch (IOException e) {
                logger.error("Could not load settings.", e);
            }
        }
    }

    @Override
    public void onShutdown() {
        if (null != settings) {
            try {
                settings.save();
                logger.info("Settings successfully saved.");
            }
            catch (IOException e) {
                logger.error("Could not save settings.", e);
            }
        }
    }

    /**
     * @return the settings
     */
    public Settings getSettings() {
        return settings;
    }

    /**
     * @param settings
     *            the settings to set
     */
    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    @Override
    public StatusBar getStatusBar() {
        if (null == statusbar) {
            statusbar = super.getStatusBar();
            statusbar.getControl();
        }
        return statusbar;
    }
}
