// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * SAX content handler extracting {@code xs:appinfo} and {@code xs:documentation} entries contents of a schema
 * annotation into a {@code XmlSchemaAnnotation} structure.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * @see XmlSchemaAnnotation
 */
public class AnnotationContentHandler implements ContentHandler {

    /** logger */
    private static final Log logger = LogFactory.getLog(AnnotationContentHandler.class);

    /** annotation tag */
    private static final QName XSD_ANNOTATION_TAG = new QName("http://www.w3.org/2001/XMLSchema", "annotation");

    /** documentation tag */
    private static final QName XSD_DOCUMENTATION_TAG = new QName("http://www.w3.org/2001/XMLSchema", "documentation");

    /** appinfo tag */
    private static final QName XSD_APPINFO_TAG = new QName("http://www.w3.org/2001/XMLSchema", "appinfo");

    private final StringBuilder annotationContent;

    /** temporal holder for the tag contents */
    private StringBuilder anContent;

    /**
     * list of parsed elements of either {@code XmlSchemaAnnotation.Type.APPINFO} or
     * {@code XmlSchemaAnnotation.Type.DOCUMENTATION}
     */
    private final List<XmlSchemaAnnotation> annotations;

    /**
     * ctor()
     */
    public AnnotationContentHandler() {
        annotationContent = new StringBuilder();
        annotations = new ArrayList<XmlSchemaAnnotation>();
    }

    /**
     * Return extracted annotations.
     * 
     * @return
     */
    public List<XmlSchemaAnnotation> getAnnotations() {
        return Collections.unmodifiableList(annotations);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        annotationContent.append(ch, start, length);
        if (null != anContent) {
            anContent.append(ch, start, length);
        }
    }

    @Override
    public void endDocument() throws SAXException {
        logger.debug("Annotation content [" + annotationContent.toString() + "]");
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        annotationContent.append("</").append(qName).append(">");
        if (StringUtils.equals(XSD_APPINFO_TAG.getNamespaceURI(), uri) && StringUtils.equals(XSD_APPINFO_TAG.getLocalPart(), localName)) {
            logger.debug("found appinfo end tag ");
            annotations.add(new XmlSchemaAnnotation(XmlSchemaAnnotation.Type.APPINFO, anContent.toString()));
            anContent = null;
        }
        else if (StringUtils.equals(XSD_DOCUMENTATION_TAG.getNamespaceURI(), uri) && StringUtils.equals(XSD_DOCUMENTATION_TAG.getLocalPart(), localName)) {
            logger.debug("found documentation end tag");
            annotations.add(new XmlSchemaAnnotation(XmlSchemaAnnotation.Type.DOCUMENTATION, anContent.toString()));
            anContent = null;
        }
        else if (null != anContent) {
            anContent.append("</").append(qName).append(">");
        }

    }

    @Override
    public void endPrefixMapping(String arg0) throws SAXException {
        // TODO Auto-generated method stub

    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        // TODO Auto-generated method stub

    }

    @Override
    public void processingInstruction(String target, String data) throws SAXException {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDocumentLocator(Locator locator) {
        // TODO Auto-generated method stub

    }

    @Override
    public void skippedEntity(String name) throws SAXException {
        // TODO Auto-generated method stub

    }

    @Override
    public void startDocument() throws SAXException {
        // TODO Auto-generated method stub

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        if (StringUtils.equals(XSD_ANNOTATION_TAG.getNamespaceURI(), uri) && StringUtils.equals(XSD_ANNOTATION_TAG.getLocalPart(), localName)) {
            logger.debug("found annotation tag");
        }
        else if (StringUtils.equals(XSD_APPINFO_TAG.getNamespaceURI(), uri) && StringUtils.equals(XSD_APPINFO_TAG.getLocalPart(), localName)) {
            logger.debug("found appinfo tag");
            anContent = new StringBuilder();
        }
        else if (StringUtils.equals(XSD_DOCUMENTATION_TAG.getNamespaceURI(), uri) && StringUtils.equals(XSD_DOCUMENTATION_TAG.getLocalPart(), localName)) {
            logger.debug("found documentation tag");
            anContent = new StringBuilder();
        }
        else if (null != anContent) {
            anContent.append("<").append(qName);
            for (int i = 0; i < atts.getLength(); ++i) {
                annotationContent.append(" ").append(atts.getQName(i)).append("=\"").append(atts.getValue(i)).append("\"");
                anContent.append(" ").append(atts.getQName(i)).append("=\"").append(atts.getValue(i)).append("\"");
            }
            anContent.append(">");
        }

        annotationContent.append("<").append(qName);
        for (int i = 0; i < atts.getLength(); ++i) {
            annotationContent.append(" ").append(atts.getQName(i)).append("=\"").append(atts.getValue(i)).append("\"");
        }
        annotationContent.append(">");

    }

    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        // TODO Auto-generated method stub

    }

    public String getAnnotationContent() {
        return annotationContent.toString();
    }
}
