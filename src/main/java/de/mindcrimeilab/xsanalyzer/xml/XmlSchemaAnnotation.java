// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.xml;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class represents an XML schema annotation {@code xs:annotation} child - which could be either an application
 * info {@code xs:appinfo} or an user documentation entry {@code xs:documentation}.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XmlSchemaAnnotation {

    /** content inside the element */
    private String content;

    /** type of the annotation */
    private Type type;

    /**
     * Type of annotation entry. Possible values are @ APPINFO} or {@DOCUMENTATION}.
     * 
     * @author Michael Engelhardt<me@mindcrime-ilab.de>
     * @author $Author$
     * @version $Revision$
     * 
     */
    public static enum Type {
        APPINFO, DOCUMENTATION
    };

    /**
     * ctor();
     */
    public XmlSchemaAnnotation() {
        this(null, null);
    }

    /**
     * ctor().
     * 
     * @param type
     *            of annotation entry
     * @param content
     *            of annotation entry
     */
    public XmlSchemaAnnotation(XmlSchemaAnnotation.Type type, String content) {
        this.type = type;
        this.content = content;
    }

    /**
     * Returns content of this annotation entry.
     * 
     * @return
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets content of this annotation entry.
     * 
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Returns type of this annotation entry.
     * 
     * @return
     */
    public Type getType() {
        return type;
    }

    /**
     * Sets type of this annotation entry.
     */
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append(type).append(content).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(type).append(content).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof XmlSchemaAnnotation)) return false;
        XmlSchemaAnnotation other = (XmlSchemaAnnotation) obj;
        return new EqualsBuilder().append(type, other.getType()).append(content, other.getContent()).isEquals();
    }
}
