// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.xerces.impl.xs.util.XSObjectListImpl;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;
import de.mindcrimeilab.xsanalyzer.xsext.TypeDescriptionFactory;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SimilarTypeFinderWorker extends NamespaceFilteredWorker implements XsComponentWorker {

    private final Map<String, XSObjectListImpl> similarTypes;

    private final TypeDescriptionFactory factory;

    public SimilarTypeFinderWorker() {
        super();
        similarTypes = new HashMap<String, XSObjectListImpl>();
        try {

            factory = new TypeDescriptionFactory();
        }
        catch (NoSuchAlgorithmException e) {
            SimilarTypeFinderWorker.logger.error("SimilarTypeFinder()", e);
            throw new RuntimeException("Cannot create " + TypeDescriptionFactory.class.getName());
        }
    }

    public synchronized Map<String, ? extends XSObjectList> getSimilarTypes() {
        removeSingularTypes();
        return Collections.unmodifiableMap(similarTypes);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#execute(org.apache.xerces.xs.XSObject,
     * org.apache.xerces.xs.XSObject)
     */
    @Override
    public void execute(XSObject object, XSObject parent) {
        final XSTypeDefinition stype = (XSTypeDefinition) object;
        String signature = generateTypeSignature(stype);
        addSimilarType(signature, stype);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.XsComponentWorker#isSupported(org.apache.xerces.xs.XSObject)
     */
    @Override
    public boolean isSupported(XSObject object) {
        final boolean result;
        if (object.getType() == XSConstants.TYPE_DEFINITION) {
            if (((XSTypeDefinition) object).getTypeCategory() == XSTypeDefinition.SIMPLE_TYPE) {
                result = super.isSupported(object);
            }
            else if (((XSTypeDefinition) object).getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE) {
                result = super.isSupported(object);
            }
            else {
                result = false;
            }
        }
        else {
            result = false;
        }
        return result;
    }

    /**
     * 
     * @param type
     * @return
     */
    private String generateTypeSignature(XSTypeDefinition type) {
        return factory.generateTypeDescriptionSignature(type);
    }

    private void addSimilarType(String signatureDigest, XSTypeDefinition type) {
        // do not add null ;)
        final XSObjectListImpl xsoList;
        if (similarTypes.containsKey(signatureDigest)) {
            xsoList = similarTypes.get(signatureDigest);
            for (int i = 0; i < xsoList.getLength(); ++i) {
                XSObject object = xsoList.item(i);
                // check for duplicate types
                if (XSModelHelper.isSameTypeDefinition(object, type)) { return; }
            }
            xsoList.add(type);
        }
        else {
            xsoList = new XSObjectListImpl();
            xsoList.add(type);
            similarTypes.put(signatureDigest, xsoList);
        }
    }

    private void removeSingularTypes() {
        Set<String> keys = similarTypes.keySet();
        for (Iterator<String> it = keys.iterator(); it.hasNext();) {
            String key = it.next();
            if (similarTypes.get(key).getLength() < 2) {
                SimilarTypeFinderWorker.logger.debug("Removing key [" + key + "] having count < 2");
                it.remove();
            }
        }
    }

}
