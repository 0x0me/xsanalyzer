// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import javax.swing.tree.TreePath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.binding.value.ValueModel;
import org.springframework.richclient.core.Guarded;


/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TreeSingleSelectionGuard extends AbstractTreeSelectionGuard {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    public TreeSingleSelectionGuard(ValueModel selectionHolder, Guarded guarded) {
        super(selectionHolder, guarded);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.model.AbstractTreeSelectionGuard#shouldEnable(javax.swing.tree.TreePath[])
     */
    @Override
    protected boolean shouldEnable(TreePath[] selected) {
        TreeSingleSelectionGuard.logger.trace("Returning true!");
        return true;
    }

}
