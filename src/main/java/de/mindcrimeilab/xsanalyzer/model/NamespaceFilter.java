// $Id:NamespaceFilter.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.util.LinkedList;
import java.util.List;

import org.apache.xerces.xs.XSObject;

import de.mindcrimeilab.util.Filter;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class NamespaceFilter implements Filter<XSObject> {

    private final List<String> filteredNamespaces;

    public NamespaceFilter() {
        filteredNamespaces = new LinkedList<String>();
    }

    public boolean addNamespace(String namespace) {
        return filteredNamespaces.add(namespace);
    }

    public boolean removeNamespace(String namespace) {
        return filteredNamespaces.remove(namespace);
    }

    @Override
    public boolean isFiltered(XSObject object) {
        final String namespaceItem = object.getNamespace();
        final boolean result = filteredNamespaces.contains(namespaceItem);
        return result;
    }

}
