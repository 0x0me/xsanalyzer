// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;

/**
 * Represents an entry of the {@code SimilarTypesListModelAdapter}.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SimilarTypeListModelEntry {

    /** Type */
    private final XSObject type;

    /** type signature as md5 hash */
    private final String typeSignature;

    /** list of types which are similar to the value of the {@code type} property. */
    private final XSObjectList similarTypes;

    /**
     * ctor()
     * 
     * @param type
     *            Type
     * @param typeSignature
     *            signature of type
     * @param similarTypes
     *            list of types which are similar to the type.
     */
    public SimilarTypeListModelEntry(XSObject type, String typeDescription, XSObjectList similarTypes) {
        super();
        this.type = type;
        this.typeSignature = typeDescription;
        this.similarTypes = similarTypes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    /**
     * Returns type.
     * 
     * @return the type
     */
    public XSObject getType() {
        return type;
    }

    /**
     * Returns signature of type
     * 
     * @return the typeSignature
     */
    public String getTypeSignature() {
        return typeSignature;
    }

    /**
     * Returns list of types which are similar to type
     * 
     * @return the similarTypes
     */
    public XSObjectList getSimilarTypes() {
        return similarTypes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object) {
        if (null == object) return false;
        if (!(object instanceof SimilarTypeListModelEntry)) return false;
        SimilarTypeListModelEntry other = (SimilarTypeListModelEntry) object;
        return new EqualsBuilder().append(this.similarTypes, other.getSimilarTypes()).append(this.type, other.getType()).append(this.typeSignature, other.getTypeSignature()).isEquals();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.similarTypes).append(this.type).append(this.typeSignature).toHashCode();
    }

}
