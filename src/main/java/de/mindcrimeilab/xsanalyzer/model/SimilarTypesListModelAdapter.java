// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.swing.AbstractListModel;
import javax.swing.ListModel;

import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;

import de.mindcrimeilab.xsanalyzer.util.NameComparator;

/**
 * Adapts the {@code Map} of similar types to a {@code ListModel}.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SimilarTypesListModelAdapter extends AbstractListModel implements ListModel {

    /**
     * serial version uid
     */
    private static final long serialVersionUID = -525227687405772834L;

    /**
     * Represents the list model view to the expanded values of the given map of similar types.
     */
    private final List<Entry<XSObject, String>> model;

    /**
     * Copy of the map containing the similar types. This property is used to fill the result of this model with all
     * information.
     */
    private final Map<String, ? extends XSObjectList> data;

    /**
     * ctor()
     * 
     * @param similarTypes
     *            Map of similar types.
     */
    public SimilarTypesListModelAdapter(final Map<String, ? extends XSObjectList> similarTypes) {
        data = Collections.unmodifiableMap(similarTypes);

        // use TreeMap to provide a sorted list view to the user
        Map<XSObject, String> map = new TreeMap<XSObject, String>(new NameComparator());
        // expand all values of the map and construct a list representing each
        // value of the map associated to its key.
        for (Entry<String, ? extends XSObjectList> entry : data.entrySet()) {
            XSObjectList list = entry.getValue();
            for (int i = 0; null != list && i < list.getLength(); ++i) {
                map.put(list.item(i), entry.getKey());
            }
        }
        model = new ArrayList<Entry<XSObject, String>>(map.entrySet());
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        Entry<XSObject, String> entry = model.get(index);
        return new SimilarTypeListModelEntry(entry.getKey(), entry.getValue(), this.data.get(entry.getValue()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        return model.size();
    }
}
