// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import javax.swing.tree.TreePath;

import org.apache.xerces.xs.XSObject;
import org.springframework.binding.value.ValueModel;
import org.springframework.richclient.core.Guarded;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XSObjectTreeSingleSelectionGuard extends AbstractTreeSelectionGuard {

    public XSObjectTreeSingleSelectionGuard(ValueModel selectionHolder, Guarded guarded) {
        super(selectionHolder, guarded);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.model.AbstractTreeSelectionGuard#shouldEnable(javax.swing.tree.TreePath[])
     */
    @Override
    protected boolean shouldEnable(TreePath[] selected) {
        return null != selected && selected.length == 1 && selected[0].getLastPathComponent() instanceof XSObject;
    }

}
