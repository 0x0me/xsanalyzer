// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.util.Map.Entry;

import javax.swing.ListModel;
import javax.xml.namespace.QName;

import org.apache.xerces.xs.XSObjectList;
import org.springframework.core.closure.Constraint;
import org.springframework.richclient.list.FilteredListModel;

import de.mindcrimeilab.xsanalyzer.ui.ListModelFactory;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SameNameDetailListModelFactory implements ListModelFactory<Entry<QName, XSObjectList>> {

    private final Constraint constraint;

    public SameNameDetailListModelFactory() {
        this(null);
    }

    public SameNameDetailListModelFactory(Constraint constraint) {
        super();
        this.constraint = constraint;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.ui.ListModelFactory#createListModel(java.lang.Object)
     */
    @Override
    public ListModel createListModel(Entry<QName, XSObjectList> value) {
        final ListModel retValue;
        if (null == constraint) {
            retValue = new XSObjectListModelAdapter(value.getValue());
        }
        else {
            retValue = new FilteredListModel(new XSObjectListModelAdapter(value.getValue()), constraint);
        }
        return retValue;
    }
}
