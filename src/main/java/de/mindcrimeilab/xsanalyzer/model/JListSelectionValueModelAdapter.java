// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import javax.swing.ListModel;

import org.springframework.binding.value.support.AbstractValueModelWrapper;
import org.springframework.richclient.list.ListSelectionValueModelAdapter;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public final class JListSelectionValueModelAdapter extends AbstractValueModelWrapper {

    private final ListModel model;

    public JListSelectionValueModelAdapter(ListModel listModel, ListSelectionValueModelAdapter valueModel) {
        super(valueModel);
        model = listModel;
    }

    @Override
    public Object getValue() {
        int[] selected = (int[]) getInnerMostValue();
        Object[] result = new Object[selected.length];
        for (int i = 0; i < selected.length; ++i) {
            result[i] = model.getElementAt(selected[i]);
        }

        return result;
    }
}
