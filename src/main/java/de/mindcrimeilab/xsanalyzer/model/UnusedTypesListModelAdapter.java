// $Id:UnusedTypesListModelAdapter.java 12 2008-02-18 23:04:22Z Michael Engelhardt $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractListModel;
import javax.swing.ListModel;

import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.util.CollectionsHelper;
import de.mindcrimeilab.xsanalyzer.util.NameComparator;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:Michael Engelhardt $
 * @version $Revision:12 $
 * 
 */
public class UnusedTypesListModelAdapter extends AbstractListModel implements ListModel {

    private final List<XSTypeDefinition> unusedTypes;

    public UnusedTypesListModelAdapter(Set<XSTypeDefinition> unusedTypes) {
        this.unusedTypes = new ArrayList<XSTypeDefinition>(unusedTypes);
        NamespaceFilter filter = new NamespaceFilter();
        filter.addNamespace("http://www.w3.org/2001/XMLSchema");
        CollectionsHelper.filterCollection(this.unusedTypes, filter);
        Collections.sort(this.unusedTypes, new NameComparator());
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.ListModel#getElementAt(int)
     */
    @Override
    public Object getElementAt(int index) {
        return unusedTypes.get(index);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.ListModel#getSize()
     */
    @Override
    public int getSize() {
        return unusedTypes.size();
    }

}
