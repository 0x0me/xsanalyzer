// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.tree.TreePath;

import org.springframework.binding.value.ValueModel;
import org.springframework.richclient.core.Guarded;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public abstract class AbstractTreeSelectionGuard implements PropertyChangeListener {

    private final ValueModel selectionHolder;

    private final Guarded guarded;

    public AbstractTreeSelectionGuard(ValueModel selectionHolder, Guarded guarded) {
        this.selectionHolder = selectionHolder;
        this.selectionHolder.addValueChangeListener(this);
        this.guarded = guarded;
        propertyChange(null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        TreePath[] selected = (TreePath[]) selectionHolder.getValue();
        guarded.setEnabled(shouldEnable(selected));

    }

    public Guarded getGuarded() {
        return guarded;
    }

    public ValueModel getSelectionHolder() {
        return selectionHolder;
    }

    protected abstract boolean shouldEnable(TreePath[] selected);
}
