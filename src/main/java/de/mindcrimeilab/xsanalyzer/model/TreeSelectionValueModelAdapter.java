// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.springframework.binding.value.support.AbstractValueModel;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TreeSelectionValueModelAdapter extends AbstractValueModel implements TreeSelectionListener {

    private final TreeSelectionModel model;

    private TreePath[] currentSelection = new TreePath[0];

    private boolean skipSelectionModelUpdate = false;

    public TreeSelectionValueModelAdapter(TreeSelectionModel model) {
        this.model = model;
        this.model.addTreeSelectionListener(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
     */
    @Override
    public void valueChanged(TreeSelectionEvent evt) {
        skipSelectionModelUpdate = true;
        setValue(getSelectedRows());
        skipSelectionModelUpdate = false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.binding.value.ValueModel#getValue()
     */
    @Override
    public Object getValue() {
        return currentSelection;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.binding.value.ValueModel#setValue(java.lang.Object)
     */
    @Override
    public void setValue(Object newValue) {
        logger.debug("New Value " + newValue);
        TreePath[] newSelection = (TreePath[]) newValue;
        if (hasChanged(currentSelection, newSelection)) {

            TreePath[] oldValue = currentSelection;
            currentSelection = newSelection;
            fireValueChange(oldValue, currentSelection);

            if (!skipSelectionModelUpdate) {
                model.removeTreeSelectionListener(this);
                model.clearSelection();

                model.addSelectionPaths(newSelection);

                // Reinstall listener
                model.addTreeSelectionListener(this);
            }
        }
    }

    /**
     * See if two arrays are different.
     */
    private boolean hasChanged(TreePath[] oldValue, TreePath[] newValue) {
        if (null == oldValue && null == newValue) { return false; }
        if ((null == oldValue && null != newValue) || (null != oldValue && null == newValue)) { return false; }
        if (oldValue.length == newValue.length) {
            for (int i = 0; i < newValue.length; i++) {
                if (oldValue[i] != newValue[i]) { return true; }
            }
            return false;
        }
        return true;
    }

    private Object getSelectedRows() {
        return model.getSelectionPaths();
    }
}
