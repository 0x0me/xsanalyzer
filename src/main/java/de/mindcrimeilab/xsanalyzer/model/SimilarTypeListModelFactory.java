// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import javax.swing.ListModel;

import org.springframework.core.closure.Constraint;
import org.springframework.richclient.list.FilteredListModel;

import de.mindcrimeilab.xsanalyzer.ui.ListModelFactory;

/**
 * Create list of types which are similar to the selected type.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SimilarTypeListModelFactory implements ListModelFactory<SimilarTypeListModelEntry> {

    /** constraint to apply to constructed list */
    private final Constraint constraint;

    /**
     * ctor()
     */
    public SimilarTypeListModelFactory() {
        this(null);
    }

    /**
     * ctor() - Initializes the constraint
     * 
     * @param constraint
     *            to apply
     */
    public SimilarTypeListModelFactory(Constraint constraint) {
        super();
        this.constraint = constraint;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.mindcrimeilab.xsanalyzer.ui.ListModelFactory#createListModel(java.lang.Object)
     */
    @Override
    public ListModel createListModel(SimilarTypeListModelEntry value) {
        final ListModel retValue;
        if (null == constraint) {
            retValue = new XSObjectListModelAdapter(value.getSimilarTypes());
        }
        else {
            retValue = new FilteredListModel(new XSObjectListModelAdapter(value.getSimilarTypes()), constraint);
        }
        return retValue;
    }

}
