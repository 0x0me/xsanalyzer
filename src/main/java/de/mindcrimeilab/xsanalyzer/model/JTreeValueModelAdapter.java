// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import javax.swing.tree.TreePath;

import org.springframework.binding.value.support.AbstractValueModelWrapper;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public final class JTreeValueModelAdapter extends AbstractValueModelWrapper {

    public JTreeValueModelAdapter(TreeSelectionValueModelAdapter valueModel) {
        super(valueModel);
    }

    @Override
    public Object getValue() {
        TreePath[] path = (TreePath[]) getInnerMostWrappedValueModel().getValue();
        Object[] result = new Object[path.length];
        for (int i = 0; i < path.length; ++i) {
            result[i] = path[i].getLastPathComponent();
        }

        return result;
    }
}