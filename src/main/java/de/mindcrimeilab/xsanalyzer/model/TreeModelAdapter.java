// $Id:TreeModelAdapter.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSNamedMap;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.xsanalyzer.util.NameComparator;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class TreeModelAdapter implements TreeModel {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private final Vector<TreeModelListener> treeModelListeners = new Vector<TreeModelListener>();

    private final static String ROOT_NODE = "schema.tree.root.label";

    private final static String COMPLEX_TYPES_NODE = "schema.tree.complexTypes.label";

    private final static String SIMPLE_TYPES_NODE = "schema.tree.simpleTypes.label";

    private final static String ELEMENTS_NODE = "schema.tree.elements.label";

    private final static String GROUPS_NODE = "schema.tree.groups.label";

    private final static String[] LEVEL1_NODES = { TreeModelAdapter.COMPLEX_TYPES_NODE, TreeModelAdapter.SIMPLE_TYPES_NODE, TreeModelAdapter.ELEMENTS_NODE, TreeModelAdapter.GROUPS_NODE};

    private final Map<Short, List<XSObject>> model;

    private final XSModel xsmodel;

    // properties of the model

    public TreeModelAdapter(XSModel xsmodel) {
        this.xsmodel = xsmodel;
        model = TreeModelAdapter.createModel(this.xsmodel);
        updateModel(true);
    }

    /*
     * (non-Javadoc)
     * 
     * @seejavax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event. TreeModelListener)
     */
    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.addElement(l);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getChild(java.lang.Object, int)
     */
    @Override
    public Object getChild(Object parent, int index) {
        Object obj = null;
        if (TreeModelAdapter.ROOT_NODE == parent) {
            obj = TreeModelAdapter.LEVEL1_NODES[index];
        }
        else if (TreeModelAdapter.COMPLEX_TYPES_NODE == parent) {
            obj = getComponent(XSTypeDefinition.COMPLEX_TYPE, index);
        }
        else if (TreeModelAdapter.SIMPLE_TYPES_NODE == parent) {
            obj = getComponent(XSTypeDefinition.SIMPLE_TYPE, index);
        }
        else if (TreeModelAdapter.ELEMENTS_NODE == parent) {
            obj = getComponent(XSConstants.ELEMENT_DECLARATION, index);
        }
        else if (TreeModelAdapter.GROUPS_NODE == parent) {
            obj = getComponent(XSConstants.MODEL_GROUP_DEFINITION, index);
        }
        else {
            obj = null;
        }

        return obj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
     */
    @Override
    public int getChildCount(Object parent) {
        int count = 0;
        if (TreeModelAdapter.ROOT_NODE == parent) {
            count = TreeModelAdapter.LEVEL1_NODES.length;
        }
        else if (TreeModelAdapter.COMPLEX_TYPES_NODE == parent) {
            count = getLength(XSTypeDefinition.COMPLEX_TYPE);
        }
        else if (TreeModelAdapter.SIMPLE_TYPES_NODE == parent) {
            count = getLength(XSTypeDefinition.SIMPLE_TYPE);
        }
        else if (TreeModelAdapter.ELEMENTS_NODE == parent) {
            count = getLength(XSConstants.ELEMENT_DECLARATION);
        }
        else if (TreeModelAdapter.GROUPS_NODE == parent) {
            count = getLength(XSConstants.MODEL_GROUP_DEFINITION);
        }
        else {
            count = 0;
        }

        return count;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int index = 0;
        if (TreeModelAdapter.ROOT_NODE == parent) {
            for (int i = 0; i < TreeModelAdapter.LEVEL1_NODES.length; ++i) {
                if (TreeModelAdapter.LEVEL1_NODES[i].equals(child)) {
                    index = i;
                    break;
                }
            }
        }
        else if (TreeModelAdapter.COMPLEX_TYPES_NODE == parent) {
            index = indexOf(XSTypeDefinition.COMPLEX_TYPE, child);
        }
        else if (TreeModelAdapter.SIMPLE_TYPES_NODE == parent) {
            index = indexOf(XSTypeDefinition.SIMPLE_TYPE, child);
        }
        else if (TreeModelAdapter.ELEMENTS_NODE == parent) {
            index = indexOf(XSConstants.ELEMENT_DECLARATION, child);
        }
        else if (TreeModelAdapter.GROUPS_NODE == parent) {
            index = indexOf(XSConstants.MODEL_GROUP_DEFINITION, child);
        }
        else {
            index = 0;
        }
        return index;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getRoot()
     */
    @Override
    public Object getRoot() {
        return TreeModelAdapter.ROOT_NODE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
     */
    @Override
    public boolean isLeaf(Object node) {
        return (0 == getChildCount(node));
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event. TreeModelListener)
     */
    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.removeElement(l);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        TreeModelAdapter.logger.debug("*** valueForPathChanged : " + path + " --> " + newValue);
    }

    /**
     * @param componentType
     *            TODO
     * @param index
     * @return
     */
    private Object getComponent(short componentType, int index) {
        List<XSObject> list = model.get(componentType);
        return (null == list) ? null : list.get(index);
    }

    private int getLength(short componentType) {
        List<XSObject> list = model.get(componentType);
        return (null == list) ? 0 : list.size();
    }

    private int indexOf(short componentType, Object object) {
        List<XSObject> list = model.get(componentType);
        return (null != list) ? list.indexOf(object) : null;
    }

    private void updateModel(boolean sort) {
        if (sort) {
            final Comparator<XSObject> comparator = new NameComparator();
            for (List<XSObject> list : model.values()) {
                Collections.sort(list, comparator);
            }
        }
    }

    private final static Map<Short, List<XSObject>> createModel(XSModel xsmodel) {
        Map<Short, List<XSObject>> map = new HashMap<Short, List<XSObject>>();

        short[] types = { XSTypeDefinition.COMPLEX_TYPE, XSTypeDefinition.SIMPLE_TYPE, XSConstants.ELEMENT_DECLARATION, XSConstants.MODEL_GROUP_DEFINITION};

        for (short type : types) {
            XSNamedMap xsMap = xsmodel.getComponents(type);
            if (null == xsMap) {
                map.put(type, null);
                continue;
            }
            List<XSObject> xsoList = new ArrayList<XSObject>(xsMap.getLength());
            for (int i = 0; i < xsMap.getLength(); ++i) {
                xsoList.add(xsMap.item(i));
            }

            map.put(type, xsoList);
        }

        return map;
    }

}
