// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.util.Observable;
import java.util.Observer;

import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.closure.Constraint;
import org.springframework.util.Assert;

import de.mindcrimeilab.swing.util.AbstractTreeModel;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class FilteredTreeModel extends AbstractTreeModel implements TreeModel, Observer {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private Constraint constraint;

    private final TreeModel defaultModel;

    public FilteredTreeModel(TreeModel model, Constraint constraint) {
        defaultModel = model;
        setConstraint(constraint);
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object object = null;
        int i = 0;
        int tmpIndex = 0;
        FilteredTreeModel.logger.debug("getChild(Object parent, int index) parent=[" + parent + "], index =[" + index + "]");
        if (null != constraint) {
            for (i = 0; i < defaultModel.getChildCount(parent); ++i) {
                object = defaultModel.getChild(parent, i);
                if (constraint.test(object)) {
                    if (index == tmpIndex) {
                        break;
                    }
                    ++tmpIndex;
                }
                object = null;
            }
        }
        else {
            object = defaultModel.getChild(parent, index);
        }
        FilteredTreeModel.logger.debug("returning child [" + object + "] @index[" + index + "] real index [" + i + "]");

        return object;
    }

    @Override
    public int getChildCount(Object parent) {
        int count = 0;
        final int unfilteredChildCount = defaultModel.getChildCount(parent);
        FilteredTreeModel.logger.debug("getChildCount(Object parent) parent=[" + parent + "] - unfiltered child count [" + unfilteredChildCount + "]");
        if (null != constraint) {
            for (int i = 0; i < unfilteredChildCount; ++i) {
                final Object child = defaultModel.getChild(parent, i);
                if (constraint.test(child)) {
                    ++count;
                }
            }
        }
        else {
            count = unfilteredChildCount;
        }
        FilteredTreeModel.logger.debug("returning filtered count=[" + count + "]");
        return count;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int index = 0;
        FilteredTreeModel.logger.debug("getIndexOfChild(Object parent, Object child) parent=[" + parent + "], child =[" + child + "]");
        if (null != constraint) {
            int tmpIndex = 0;
            for (int i = 0; i < defaultModel.getChildCount(parent); ++i) {
                Object tmp = defaultModel.getChild(parent, i);
                if (constraint.test(tmp)) {
                    ++tmpIndex;
                    if (tmp.equals(child)) {
                        index = tmpIndex;
                        break;
                    }
                }
            }
        }
        else {
            index = defaultModel.getIndexOfChild(parent, child);
        }
        FilteredTreeModel.logger.debug("Returning index =[" + index + "]");
        return index;
    }

    @Override
    public Object getRoot() {
        return defaultModel.getRoot();
    }

    @Override
    public boolean isLeaf(Object node) {
        return getChildCount(node) == 0;
    }

    /**
     * @return the constraint
     */
    public Constraint getConstraint() {
        return constraint;
    }

    /**
     * @param constraint
     *            the constraint to set
     */
    public final void setConstraint(Constraint constraint) {
        Assert.notNull(constraint);
        if (!constraint.equals(this.constraint)) {
            if (this.constraint instanceof Observable) {
                ((Observable) constraint).deleteObserver(this);
            }
            this.constraint = constraint;
            if (constraint instanceof Observable) {
                ((Observable) constraint).addObserver(this);
            }
            fireTreeStructureChanged(this, new TreePath[] { new TreePath(getRoot())}, null, null);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        fireTreeStructureChanged(this, new TreePath[] { new TreePath(getRoot())}, null, null);
    }
}
