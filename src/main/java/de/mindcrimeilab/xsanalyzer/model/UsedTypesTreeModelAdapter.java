// $Id:UsedTypesTreeModelAdapter.java 12 2008-02-18 23:04:22Z Michael Engelhardt $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;

import de.mindcrimeilab.xsanalyzer.util.DefaultMutableTreeNodeComparator;
import de.mindcrimeilab.xsanalyzer.util.NameComparator;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:Michael Engelhardt $
 * @version $Revision:12 $
 * 
 */
public class UsedTypesTreeModelAdapter implements TreeModel {

    private final static String ROOT_NODE = "Used Types";

    private final Vector<TreeModelListener> treeModelListeners = new Vector<TreeModelListener>();

    private final Map<XSObject, ? extends XSObjectList> usedBy;

    private final List<DefaultMutableTreeNode> types;

    public UsedTypesTreeModelAdapter(Map<XSObject, ? extends XSObjectList> usage) {
        usedBy = usage;
        final Set<XSObject> keySet = usedBy.keySet();
        types = new ArrayList<DefaultMutableTreeNode>(keySet.size());
        for (XSObject xso : keySet) {
            if (null != xso) {
                types.add(new DefaultMutableTreeNode(xso));
            }
        }
        Collections.sort(types, new DefaultMutableTreeNodeComparator(new NameComparator()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event.TreeModelListener)
     */
    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.addElement(l);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getChild(java.lang.Object, int)
     */
    @Override
    public Object getChild(Object parent, int index) {
        Object child;
        if (parent == UsedTypesTreeModelAdapter.ROOT_NODE) {
            child = types.get(index);
        }
        else if (parent instanceof DefaultMutableTreeNode) {
            DefaultMutableTreeNode tn = (DefaultMutableTreeNode) parent;
            XSObjectList list = usedBy.get(tn.getUserObject());
            child = list.item(index);
        }
        else {
            child = null;
        }
        return child;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
     */
    @Override
    public int getChildCount(Object parent) {
        int count;
        if (parent == UsedTypesTreeModelAdapter.ROOT_NODE) {
            count = types.size();
        }
        else if (parent instanceof DefaultMutableTreeNode) {
            DefaultMutableTreeNode tn = (DefaultMutableTreeNode) parent;
            XSObjectList list = usedBy.get(tn.getUserObject());
            count = list.getLength();
        }
        else {
            count = 0;
        }
        return count;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int index = 0;
        if (parent == UsedTypesTreeModelAdapter.ROOT_NODE) {
            index = types.indexOf(child);
        }
        else if (parent instanceof DefaultMutableTreeNode) {
            DefaultMutableTreeNode tn = (DefaultMutableTreeNode) parent;
            XSObjectList list = usedBy.get(tn.getUserObject());
            for (int i = 0; i < list.getLength(); ++i) {
                if (child.equals(list.item(i))) {
                    index = i;
                    break;
                }
            }
        }
        else {
            index = 0;
        }
        return index;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getRoot()
     */
    @Override
    public Object getRoot() {
        return UsedTypesTreeModelAdapter.ROOT_NODE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
     */
    @Override
    public boolean isLeaf(Object node) {
        return 0 == getChildCount(node);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event.TreeModelListener)
     */
    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.removeElement(l);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        System.out.println("*** valueForPathChanged : " + path + " --> " + newValue);
    }

}
