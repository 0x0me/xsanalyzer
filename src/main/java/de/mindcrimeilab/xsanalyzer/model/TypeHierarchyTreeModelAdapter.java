// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSObject;

import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeHierarchyTreeModelAdapter implements TreeModel {

    private static final Log logger = LogFactory.getLog("xsAnalyzerApplicationLogger");

    private final Vector<TreeModelListener> treeModelListeners = new Vector<TreeModelListener>();

    private final List<XSObject> hierarchy;

    public TypeHierarchyTreeModelAdapter(XSObject object) {
        hierarchy = TypeHierarchyTreeModelAdapter.createModel(object);
    }

    private static List<XSObject> createModel(XSObject object) {
        List<XSObject> list = new ArrayList<XSObject>();
        list.add(object);
        XSObject parent = object;
        while (null != (parent = XSModelHelper.getBaseType(parent))) {
            if (list.contains(parent)) {
                break;
            }
            list.add(parent);
        }

        Collections.reverse(list);
        return list;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event.TreeModelListener)
     */
    @Override
    public void addTreeModelListener(TreeModelListener listener) {
        treeModelListeners.addElement(listener);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getChild(java.lang.Object, int)
     */
    @Override
    public Object getChild(Object parent, int index) {
        int childIndex = hierarchy.indexOf(parent) + 1;
        return (childIndex < hierarchy.size() && index < getChildCount(parent)) ? hierarchy.get(childIndex) : null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
     */
    @Override
    public int getChildCount(Object parent) {
        int childIndex = hierarchy.indexOf(parent) + 1;
        return (null != parent && childIndex < hierarchy.size()) ? 1 : 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return (null == parent || null == child) ? -1 : 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#getRoot()
     */
    @Override
    public Object getRoot() {
        return hierarchy.get(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
     */
    @Override
    public boolean isLeaf(Object value) {
        return !(hierarchy.indexOf(value) + 1 < hierarchy.size());
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event.TreeModelListener)
     */
    @Override
    public void removeTreeModelListener(TreeModelListener listener) {
        treeModelListeners.removeElement(listener);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        TypeHierarchyTreeModelAdapter.logger.debug("*** valueForPathChanged : " + path + " --> " + newValue);
    }

}
