// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.io.File;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSNamespaceItem;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSTypeDefinition;

import de.mindcrimeilab.xsanalyzer.util.XSModelHelper;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XsAnalyzerApplicationModel {

    private File schemaFile;

    private XSModel schemaModel;

    private XSNamespaceItem targetNamespace = null;

    private Map<XSObject, ? extends XSObjectList> usedTypes;

    private Set<XSTypeDefinition> unusedTypes;

    private Map<String, XSObjectList> similarTypes;

    private Map<QName, XSObjectList> sameNamesDifferentTypes;

    public XsAnalyzerApplicationModel() {
        schemaFile = null;
        usedTypes = null;
        unusedTypes = null;
        similarTypes = null;
        sameNamesDifferentTypes = null;
    }

    /**
     * @return the schemaFile
     */
    public File getSchemaFile() {
        return schemaFile;
    }

    /**
     * @param schemaFile
     *            the schemaFile to set
     */
    public void setSchemaFile(File schemaFile) {
        this.schemaFile = schemaFile;
    }

    /**
     * @return the schemaModel
     */
    public XSModel getSchemaModel() {
        return schemaModel;
    }

    /**
     * @param schemaModel
     *            the schemaModel to set
     */
    public void setSchemaModel(XSModel schemaModel) {
        this.schemaModel = schemaModel;
        updateTargetNamespace();
    }

    /**
     * @return the usedTypes
     */
    public Map<XSObject, ? extends XSObjectList> getUsedTypes() {
        return usedTypes;
    }

    /**
     * @param usedTypes
     *            the usedTypes to set
     */
    public void setUsedTypes(Map<XSObject, ? extends XSObjectList> usedTypes) {
        this.usedTypes = usedTypes;
    }

    /**
     * 
     * @return the unusedTypes
     */
    public Set<XSTypeDefinition> getUnusedTypes() {
        return unusedTypes;
    }

    /**
     * @param unusedTypes
     *            the unusedTypes to set
     */
    public void setUnusedTypes(Set<XSTypeDefinition> unusedTypes) {
        this.unusedTypes = unusedTypes;
    }

    /**
     * @return the similarTypes
     */
    public Map<String, XSObjectList> getSimilarTypes() {
        return similarTypes;
    }

    public void setSimilarTypes(Map<String, XSObjectList> similarTypes) {
        this.similarTypes = similarTypes;
    }

    /**
     * @return the sameNamesDifferentTypes
     */
    public Map<QName, XSObjectList> getSameNamesDifferentTypes() {
        return sameNamesDifferentTypes;
    }

    public void setSameNamesDifferentTypes(Map<QName, XSObjectList> sameNames) {
        this.sameNamesDifferentTypes = sameNames;
    }

    public XSNamespaceItem getTargetNamespace() {
        if (null == targetNamespace) {
            updateTargetNamespace();
        }
        return targetNamespace;
    }

    /**
     * @param schemaModel
     */
    private void updateTargetNamespace() {
        targetNamespace = XSModelHelper.getTargetNamespace(schemaModel, schemaFile);
    }
}
