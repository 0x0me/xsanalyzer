// $Id:CollectionsHelper.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class CollectionsHelper {

    /**
     * Removes all elements from the collection for which the given <code>Filter</code> returns true.
     * 
     * @param <T>
     *            type specification of collection contents
     * @param collection
     *            collection to be filtered
     * @param filter
     *            instance of filter which will be evaluated for each element in the collection
     */
    public static <T> void filterCollection(final Collection<T> collection, Filter<? super T> filter) {
        final Iterator<T> iterator = collection.iterator();
        while (null != iterator && iterator.hasNext()) {
            if (filter.isFiltered(iterator.next())) {
                iterator.remove();
            }
        }
    }

    /**
     * Removes all duplicate items from given collection
     * 
     * @param <T>
     *            type of items in collection
     * @param collection
     */
    public static <T> void reduceCollection(final Collection<T> collection) {
        Collection<T> temp = new ArrayList<T>(collection);
        collection.clear();
        final Iterator<T> iterator = temp.iterator();
        while (iterator.hasNext()) {
            T item = iterator.next();
            iterator.remove();
            if (!temp.contains(item)) {
                collection.add(item);
            }
        }
    }

    /**
     * Copies all items from the source collection to given collection using specified converter class.
     * 
     * @param <TIN>
     *            type of items in source collection
     * @param <TOUT>
     *            type of items in destination collection
     * @param source
     *            source collection
     * @param dest
     *            destination collection
     * @param converter
     *            function to convert one object into the other one
     */
    public static <TIN, TOUT> void copyCollection(final Collection<TIN> source, final Collection<TOUT> dest, final Converter<TIN, TOUT> converter) {
        assert source != null;
        assert dest != null;
        Iterator<TIN> iterator = source.iterator();
        while (iterator.hasNext()) {
            dest.add(converter.convert(iterator.next()));
        }
    }
}
