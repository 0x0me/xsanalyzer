// $Id: MapEntryAccessor.java 146 2009-03-04 21:53:47Z agony $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package de.mindcrimeilab.util;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.xerces.xs.XSObject;
/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author: agony $
 * @version $Revision: 146 $
 * 
 */
public class MapEntryAccessor implements Accessor<XSObject, Map.Entry<? extends XSObject, Object>> {

    @Override
    public XSObject getValue(Entry<? extends XSObject, Object> object) {
        return object.getKey();
    }

}
