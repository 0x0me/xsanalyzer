// $Id$
/**
 * 
 */
package de.mindcrimeilab.util;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 * Convert one object of type {@code TIN} to an object of type {@code TOUT}.
 */
public interface Converter<TIN, TOUT> {

    public TOUT convert(TIN object);
}
