// $Id$
/**
 * 
 */
package de.mindcrimeilab.swing.util;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Helpers for JTree handling.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class JTreeHelper {

    /**
     * Expand all nodes in the tree.
     * 
     * @param tree
     */
    public static void expandToLast(JTree tree) {
        TreeModel model = tree.getModel();
        Object node = model.getRoot();

        if (node == null) { return; }

        TreePath path = new TreePath(node);
        while (true) {
            int count = model.getChildCount(node);
            if (count == 0) {
                break;
            }
            node = model.getChild(node, count - 1);
            path = path.pathByAddingChild(node);
        }
        tree.scrollPathToVisible(path);
    }

}
