// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.swing.log4j;

import org.apache.log4j.Appender;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Log4j appender mainly derived from LF5Appender from log4j.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SwingConsoleAppender extends AppenderSkeleton implements Appender {

    private SwingConsole console;

    public SwingConsoleAppender() {
        super();
        activateOptions();
    }

    public SwingConsoleAppender(Layout layout) {
        super();
        setLayout(layout);
        activateOptions();
    }

    /**
     * @return the console
     */
    public SwingConsole getConsole() {
        return console;
    }

    /**
     * @param console
     *            the console to set
     */
    public void setConsole(SwingConsole console) {
        this.console = console;
    }

    @Override
    protected void append(LoggingEvent event) {
        StringBuilder sb = new StringBuilder();
        final Layout layout = getLayout();
        if (null != layout) {
            sb.append(layout.format(event));
            if (layout.ignoresThrowable()) {
                String[] s = event.getThrowableStrRep();
                if (s != null) {
                    int len = s.length;
                    for (int i = 0; i < len; i++) {
                        sb.append(s[i]);
                    }
                }
            }
        }
        else {
            sb.append(event.getMessage());
        }

        sb.append(Layout.LINE_SEP);

        if (console != null) {
            console.addMessage(StyledLogMessage.getLogMessage(event.getLevel(), sb.toString()));
        }

    }

    @Override
    public void close() {
    }

    @Override
    public boolean requiresLayout() {
        return false;
    }

}
