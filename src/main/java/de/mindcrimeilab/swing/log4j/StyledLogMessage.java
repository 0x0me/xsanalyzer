// $Id$
/**
 * 
 */
package de.mindcrimeilab.swing.log4j;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.apache.log4j.Level;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public final class StyledLogMessage {

    private final String formattedLogMessage;

    private final Level level;

    private final AttributeSet style;

    public static MutableAttributeSet STYLE_ERROR = new SimpleAttributeSet();

    public static MutableAttributeSet STYLE_WARNING = new SimpleAttributeSet();

    public static MutableAttributeSet STYLE_INFO = new SimpleAttributeSet();

    public static MutableAttributeSet STYLE_CONFIG = new SimpleAttributeSet();

    public static MutableAttributeSet STYLE_FINE = new SimpleAttributeSet();

    public static MutableAttributeSet STYLE_FINER = new SimpleAttributeSet();

    public static MutableAttributeSet STYLE_FINEST = new SimpleAttributeSet();

    public static MutableAttributeSet STYLE_DEFAULT = new SimpleAttributeSet();

    private static final Map<Level, AttributeSet> levelStyle = new HashMap<Level, AttributeSet>();

    static {
        String FONTFAMILY = "Verdana";
        int FONTSIZE = 12;

        // Log level Error
        StyleConstants.setFontFamily(StyledLogMessage.STYLE_ERROR, FONTFAMILY);
        StyleConstants.setFontSize(StyledLogMessage.STYLE_ERROR, FONTSIZE);
        StyleConstants.setForeground(StyledLogMessage.STYLE_ERROR, Color.red.darker());
        StyleConstants.setBold(StyledLogMessage.STYLE_ERROR, true);

        // Log level Info
        StyleConstants.setFontFamily(StyledLogMessage.STYLE_INFO, FONTFAMILY);
        StyleConstants.setFontSize(StyledLogMessage.STYLE_INFO, FONTSIZE);
        StyleConstants.setForeground(StyledLogMessage.STYLE_INFO, Color.green.darker());

        // Log level fine
        StyleConstants.setFontFamily(StyledLogMessage.STYLE_FINE, FONTFAMILY);
        StyleConstants.setFontSize(StyledLogMessage.STYLE_FINE, FONTSIZE - 1);
        StyleConstants.setForeground(StyledLogMessage.STYLE_FINE, Color.black);
        StyleConstants.setBold(StyledLogMessage.STYLE_FINE, false);

        // Log level finer
        StyleConstants.setFontFamily(StyledLogMessage.STYLE_FINER, FONTFAMILY);
        StyleConstants.setFontSize(StyledLogMessage.STYLE_FINER, FONTSIZE - 2);
        StyleConstants.setForeground(StyledLogMessage.STYLE_FINER, Color.black);
        StyleConstants.setBold(StyledLogMessage.STYLE_FINER, false);

        // Log level finest
        StyleConstants.setFontFamily(StyledLogMessage.STYLE_FINEST, FONTFAMILY);
        StyleConstants.setFontSize(StyledLogMessage.STYLE_FINEST, FONTSIZE - 3);
        StyleConstants.setForeground(StyledLogMessage.STYLE_FINEST, Color.black);
        StyleConstants.setBold(StyledLogMessage.STYLE_FINEST, false);

        // default
        StyleConstants.setFontFamily(StyledLogMessage.STYLE_DEFAULT, FONTFAMILY);
        StyleConstants.setFontSize(StyledLogMessage.STYLE_DEFAULT, FONTSIZE);
        StyleConstants.setForeground(StyledLogMessage.STYLE_DEFAULT, Color.black);
        StyleConstants.setBold(StyledLogMessage.STYLE_DEFAULT, false);

        StyledLogMessage.levelStyle.put(Level.ALL, StyledLogMessage.STYLE_FINEST);
        StyledLogMessage.levelStyle.put(Level.TRACE, StyledLogMessage.STYLE_FINER);
        StyledLogMessage.levelStyle.put(Level.DEBUG, StyledLogMessage.STYLE_FINE);
        StyledLogMessage.levelStyle.put(Level.INFO, StyledLogMessage.STYLE_INFO);
        StyledLogMessage.levelStyle.put(Level.WARN, StyledLogMessage.STYLE_WARNING);
        StyledLogMessage.levelStyle.put(Level.ERROR, StyledLogMessage.STYLE_ERROR);
        StyledLogMessage.levelStyle.put(Level.FATAL, StyledLogMessage.STYLE_ERROR);
    }

    private StyledLogMessage(Level level, String formattedLogMessage) {
        this.level = level;
        this.formattedLogMessage = formattedLogMessage;

        if (Level.ALL.equals(level)) {
            style = StyledLogMessage.STYLE_FINEST;
        }
        else if (Level.TRACE.equals(level)) {
            style = StyledLogMessage.STYLE_FINER;
        }
        else if (Level.DEBUG.equals(level)) {
            style = StyledLogMessage.STYLE_FINE;
        }
        else if (Level.INFO.equals(level)) {
            style = StyledLogMessage.STYLE_INFO;
        }
        else if (Level.WARN.equals(level)) {
            style = StyledLogMessage.STYLE_WARNING;
        }
        else if (Level.ERROR.equals(level)) {
            style = StyledLogMessage.STYLE_ERROR;
        }
        else if (Level.FATAL.equals(level)) {
            style = StyledLogMessage.STYLE_ERROR;
        }
        else {
            style = StyledLogMessage.STYLE_DEFAULT;
        }
    }

    public static final StyledLogMessage getLogMessage(Level level, String formattedLogMessage) {
        return new StyledLogMessage(level, formattedLogMessage);
    }

    /**
     * @return the formattedLogMessage
     */
    public String getFormattedLogMessage() {
        return formattedLogMessage;
    }

    /**
     * @return the level
     */
    public Level getLevel() {
        return level;
    }

    /**
     * @return the style
     */
    public AttributeSet getStyle() {
        return style;
    }

}
