// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.swing.log4j;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SwingConsole extends JPanel {

    /**
     * serial version
     */
    private static final long serialVersionUID = 8247651628443314850L;

    private static final int MAX_EXCESS = 5000;

    private static final int IDEAL_SIZE = 10000;

    private final JTextPane jtpConsole;

    public SwingConsole() {
        jtpConsole = new JTextPane();
        initializeGui();
    }

    public void addMessage(final StyledLogMessage message) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                final Document document = jtpConsole.getDocument();

                try {
                    document.insertString(document.getLength(), message.getFormattedLogMessage(), message.getStyle());
                }
                catch (BadLocationException ex) {
                    System.err.println("SwingConsole ERROR:" + ex.getMessage());
                    ex.printStackTrace(System.err);
                }

                // Make sure the last line is always visible
                jtpConsole.setCaretPosition(document.getLength());

                // Keep the text area down to a certain
                // character
                // size
                int idealSize = SwingConsole.IDEAL_SIZE;
                int maxExcess = SwingConsole.MAX_EXCESS;
                int excess = document.getLength() - idealSize;
                if (excess >= maxExcess) {
                    // textArea.replaceRange("", 0, excess);
                    try {
                        if (document instanceof AbstractDocument) {
                            ((AbstractDocument) document).replace(0, excess, "", null);
                        }
                        else {
                            document.remove(0, excess);
                            document.insertString(0, "", null);
                        }
                    }
                    catch (BadLocationException ex) {
                        System.err.println("SwingConsole ERROR:" + ex.getMessage());
                        ex.printStackTrace(System.err);
                        throw new IllegalArgumentException(ex.getMessage());
                    }
                }

            }

        });
    }

    private void initializeGui() {
        setLayout(new BorderLayout());
        this.add(new JScrollPane(jtpConsole, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED));
    }
}
