// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package de.mindcrimeilab.xsanalyzer.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Test class for {@code AnnotationContentHandler}.
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class AnnotationContentHandlerTest {

    private static final String NO_ANNOTATION = "<xs:annotation xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"/>";

    private static final String SIMPLE_CONTENT = "Hello World!";

    private static final String COMPLEX_CONTENT = "<html><head><title>Hello World Title</title></head><body><p id=\"content\">Hello World</p></body></html>";

    private static final String DOCUMENTATION_ANNOTATION_SIMPLE = "<xs:documentation  xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" + SIMPLE_CONTENT + "</xs:documentation>";

    private static final String DOCUMENTATION_ANNOTATION_COMPLEX = "<xs:documentation  xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" + COMPLEX_CONTENT + "</xs:documentation>";

    private static final String ANNOTATION_DOCUMENTATION_SIMPLE_CONTENT = "<xs:annotation xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" + DOCUMENTATION_ANNOTATION_SIMPLE + "</xs:annotation>";

    private static final String ANNOTATION_DOCUMENTATION_COMPLEX_CONTENT = "<xs:annotation xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" + DOCUMENTATION_ANNOTATION_COMPLEX + "</xs:annotation>";

    private static final String APPINFO_ANNOTATION_SIMPLE = "<xs:appinfo  xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" + SIMPLE_CONTENT + "</xs:appinfo>";

    private static final String APPINFO_ANNOTATION_COMPLEX = "<xs:appinfo  xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" + COMPLEX_CONTENT + "</xs:appinfo>";

    private static final String ANNOTATION_APPINFO_SIMPLE_CONTENT = "<xs:annotation xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" + APPINFO_ANNOTATION_SIMPLE + "</xs:annotation>";

    private static final String ANNOTATION_APPINFO_COMPLEX_CONTENT = "<xs:annotation xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" + APPINFO_ANNOTATION_COMPLEX + "</xs:annotation>";

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link de.mindcrimeilab.xsanalyzer.xml.AnnotationContentHandler#getAnnotationContent()}.
     */
    @Test
    public void testGetAnnotationContent() {
        List<XmlSchemaAnnotation> result = readAnnotationFromString(NO_ANNOTATION);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = readAnnotationFromString(DOCUMENTATION_ANNOTATION_SIMPLE);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(1 == result.size());
        assertEquals(SIMPLE_CONTENT, result.get(0).getContent());
        assertEquals(XmlSchemaAnnotation.Type.DOCUMENTATION, result.get(0).getType());

        result = readAnnotationFromString(ANNOTATION_DOCUMENTATION_SIMPLE_CONTENT);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(1 == result.size());
        assertEquals(SIMPLE_CONTENT, result.get(0).getContent());
        assertEquals(XmlSchemaAnnotation.Type.DOCUMENTATION, result.get(0).getType());

        result = readAnnotationFromString(DOCUMENTATION_ANNOTATION_COMPLEX);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(1 == result.size());
        assertEquals(COMPLEX_CONTENT, result.get(0).getContent());
        assertEquals(XmlSchemaAnnotation.Type.DOCUMENTATION, result.get(0).getType());

        result = readAnnotationFromString(ANNOTATION_DOCUMENTATION_COMPLEX_CONTENT);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(1 == result.size());
        assertEquals(COMPLEX_CONTENT, result.get(0).getContent());
        assertEquals(XmlSchemaAnnotation.Type.DOCUMENTATION, result.get(0).getType());

        // --
        result = readAnnotationFromString(APPINFO_ANNOTATION_SIMPLE);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(1 == result.size());
        assertEquals(SIMPLE_CONTENT, result.get(0).getContent());
        assertEquals(XmlSchemaAnnotation.Type.APPINFO, result.get(0).getType());

        result = readAnnotationFromString(ANNOTATION_APPINFO_SIMPLE_CONTENT);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(1 == result.size());
        assertEquals(SIMPLE_CONTENT, result.get(0).getContent());
        assertEquals(XmlSchemaAnnotation.Type.APPINFO, result.get(0).getType());

        result = readAnnotationFromString(APPINFO_ANNOTATION_COMPLEX);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(1 == result.size());
        assertEquals(COMPLEX_CONTENT, result.get(0).getContent());
        assertEquals(XmlSchemaAnnotation.Type.APPINFO, result.get(0).getType());

        result = readAnnotationFromString(ANNOTATION_APPINFO_COMPLEX_CONTENT);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertTrue(1 == result.size());
        assertEquals(COMPLEX_CONTENT, result.get(0).getContent());
        assertEquals(XmlSchemaAnnotation.Type.APPINFO, result.get(0).getType());

    }

    /**
     * @return
     */
    private List<XmlSchemaAnnotation> readAnnotationFromString(String content) {
        XMLReader reader = null;

        AnnotationContentHandler annotationContentHandler = new AnnotationContentHandler();

        try {
            reader = XMLReaderFactory.createXMLReader();
            assertNotNull(reader);
            reader.setContentHandler(annotationContentHandler);
            reader.parse(new InputSource(new StringReader(content)));
        }
        catch (SAXException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
        catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        // no annotation content - complete but empty structure
        List<XmlSchemaAnnotation> result = annotationContentHandler.getAnnotations();
        return result;
    }

}
