// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mindcrimeilab.xsanalyzer.xml.XmlSchemaAnnotation.Type;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XmlSchemaAnnotationTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testHashCode() {
        final XmlSchemaAnnotation annotation1 = new XmlSchemaAnnotation();
        final Type type = XmlSchemaAnnotation.Type.DOCUMENTATION;
        final String content = "an annotation";

        annotation1.setType(type);
        annotation1.setContent(content);

        final XmlSchemaAnnotation annotation2 = new XmlSchemaAnnotation(type, content);

        // null test
        final XmlSchemaAnnotation nullAnnotation = new XmlSchemaAnnotation();
        try {
            nullAnnotation.hashCode();
        }
        catch (Throwable t) {
            fail("Calculating of hashCode failed for reason [" + t.getMessage() + "]");
        }

        // hashCode test
        HashCodeBuilder builder = new HashCodeBuilder();
        int hashCode = builder.append(type).append(content).toHashCode();
        assertEquals("Same values should return same hashCode", hashCode, annotation1.hashCode());
        assertEquals("Same values should return same hashCode", hashCode, annotation2.hashCode());

        // same objects test
        assertEquals("Objects with same values should have same hashCode.", annotation1.hashCode(), annotation2.hashCode());
    }

    @Test
    public void testXmlSchemaAnnotation() {
        final XmlSchemaAnnotation annotation = new XmlSchemaAnnotation();
        assertNull(annotation.getContent());
        assertNull(annotation.getType());
    }


    @Test
    public void testGetContent() {
        final Type type = XmlSchemaAnnotation.Type.DOCUMENTATION;
        final String content = "an annotation";
        final XmlSchemaAnnotation annotation = new XmlSchemaAnnotation(type, content);
        assertEquals(content, annotation.getContent());
    }

    @Test
    public void testSetContent() {
        final XmlSchemaAnnotation annotation = new XmlSchemaAnnotation();
        final String content = "an annotation";

        annotation.setContent(content);
        assertEquals(content, annotation.getContent());
    }

    @Test
    public void testGetType() {
        final Type type = XmlSchemaAnnotation.Type.DOCUMENTATION;
        final String content = "an annotation";
        final XmlSchemaAnnotation annotation = new XmlSchemaAnnotation(type, content);
        assertEquals(type, annotation.getType());
    }

    @Test
    public void testSetType() {
        final XmlSchemaAnnotation annotation = new XmlSchemaAnnotation();
        final Type type = XmlSchemaAnnotation.Type.DOCUMENTATION;

        annotation.setType(type);
        assertEquals(type, annotation.getType());
    }

    @Test
    public void testEqualsObject() {
        final XmlSchemaAnnotation annotation1 = new XmlSchemaAnnotation();
        final Type type = XmlSchemaAnnotation.Type.DOCUMENTATION;
        final String content = "an annotation";

        annotation1.setType(type);
        annotation1.setContent(content);

        final XmlSchemaAnnotation annotation2 = new XmlSchemaAnnotation(type, content);
        final XmlSchemaAnnotation nullAnnotation = new XmlSchemaAnnotation();
        
        assertFalse(nullAnnotation.equals(null));
        assertFalse(nullAnnotation.equals(annotation1));
        assertTrue(annotation1.equals(annotation2));
        assertFalse(annotation1.equals(null));
        assertTrue(annotation2.equals(annotation1));
    }

}
