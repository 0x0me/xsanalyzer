// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.io.File;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mindcrimeilab.xsanalyzer.util.XsModelFactory;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeUsageWorkerTest {

    private static final Log logger = LogFactory.getLog("xsAnalyzerJunitTestLogger");

    private XSModel model;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/unusedTypes.xsd"));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link de.mindcrimeilab.xsanalyzer.TypeUsageWorker#getUsedBy()}.
     */
    @Test
    public final void testGetUsedBy() {
        TypeUsageWorker finder = new TypeUsageWorker();

        ComponentVisitor visitor = new ComponentVisitor();

        XsModelWalker walker = new XsModelWalker();
        walker.addWorker(finder);
        walker.addWorker(visitor);
        walker.walkModel(model);

        Map<XSObject, ? extends XSObjectList> usedTypes = finder.getUsedBy();
        Assert.assertNotNull(usedTypes);
        for (XSObject type : usedTypes.keySet()) {
            XSObjectList list = usedTypes.get(type);
            TypeUsageWorkerTest.logger.debug("UsedType [" + type + "] has [" + list + "]");
            if (null == list) {
                continue;
            }
            for (int i = 0; i < list.getLength(); ++i) {
                TypeUsageWorkerTest.logger.debug("Containing type [" + list.item(i) + "]");
                Assert.assertNotNull(list.item(i));
            }
        }
    }

}
