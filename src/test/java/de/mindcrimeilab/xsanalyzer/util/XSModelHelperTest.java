// $Id:XSModelHelperTest.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.util;

import java.io.File;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSNamespaceItem;
import org.apache.xerces.xs.XSTypeDefinition;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class XSModelHelperTest {

    private static final Log logger = LogFactory.getLog("xsAnalyzerJunitTestLogger");

    private XSModel model;

    public XSModelHelperTest() {
        model = null;
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/unusedTypes.xsd"));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link de.mindcrimeilab.xsanalyzer.util.XSModelHelper#getComponents(org.apache.xerces.xs.XSModel, java.util.List)}
     * .
     */
    @Test
    public final void testGetComponents() {
        List<XSNamespaceItem> ns = XSModelHelper.getNamespaceItemsAsList(model);
        Assert.assertNotNull(ns);

        Set<? extends XSTypeDefinition> types = XSModelHelper.getComponents(model, ns);
        Assert.assertNotNull(types);
    }

    /**
     * Test method for
     * {@link de.mindcrimeilab.xsanalyzer.util.XSModelHelper#getElements(org.apache.xerces.xs.XSModel, java.util.List)}
     * .
     */
    @Test
    public final void testGetElements() {
        List<XSNamespaceItem> ns = XSModelHelper.getNamespaceItemsAsList(model);
        Assert.assertNotNull(ns);
        List<XSElementDeclaration> elements = XSModelHelper.getElements(model, ns);
        Assert.assertNotNull(elements);
    }

    @Test
    public final void testGetNamespaceItemsAsList() {
        List<XSNamespaceItem> ns = XSModelHelper.getNamespaceItemsAsList(model);
        Assert.assertNotNull(ns);
    }
}
