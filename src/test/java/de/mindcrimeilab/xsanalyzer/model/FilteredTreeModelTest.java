// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package de.mindcrimeilab.xsanalyzer.model;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.closure.Constraint;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class FilteredTreeModelTest {

    private static final Log logger = LogFactory.getLog("xsAnalyzerJunitTestLogger");

    private TreeModel unfilteredModel;

    private TreeModel filteredModel;

    private DefaultMutableTreeNode root;

    private DefaultMutableTreeNode a;

    private DefaultMutableTreeNode aa;

    DefaultMutableTreeNode aaa;

    DefaultMutableTreeNode aab;

    DefaultMutableTreeNode aac;

    DefaultMutableTreeNode aad;

    DefaultMutableTreeNode ab;

    DefaultMutableTreeNode aba;

    DefaultMutableTreeNode abb;

    DefaultMutableTreeNode abc;

    DefaultMutableTreeNode abd;

    DefaultMutableTreeNode ac;

    DefaultMutableTreeNode aca;

    DefaultMutableTreeNode acb;

    DefaultMutableTreeNode acc;

    DefaultMutableTreeNode acd;

    DefaultMutableTreeNode ad;

    DefaultMutableTreeNode ada;

    DefaultMutableTreeNode adb;

    DefaultMutableTreeNode adc;

    DefaultMutableTreeNode add;

    DefaultMutableTreeNode b;

    DefaultMutableTreeNode ba;

    DefaultMutableTreeNode baa;

    DefaultMutableTreeNode bab;

    DefaultMutableTreeNode bac;

    DefaultMutableTreeNode bad;

    DefaultMutableTreeNode c;

    DefaultMutableTreeNode ca;

    DefaultMutableTreeNode caa;

    DefaultMutableTreeNode cab;

    DefaultMutableTreeNode cac;

    DefaultMutableTreeNode cad;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        root = new DefaultMutableTreeNode("Root");
        a = new DefaultMutableTreeNode("A");
        aa = new DefaultMutableTreeNode("AA");
        aaa = new DefaultMutableTreeNode("AAA");
        aab = new DefaultMutableTreeNode("AAB");
        aac = new DefaultMutableTreeNode("AAC");
        aad = new DefaultMutableTreeNode("AAD");
        ab = new DefaultMutableTreeNode("AB");
        aba = new DefaultMutableTreeNode("ABA");
        abb = new DefaultMutableTreeNode("ABB");
        abc = new DefaultMutableTreeNode("ABC");
        abd = new DefaultMutableTreeNode("ABD");
        ac = new DefaultMutableTreeNode("AC");
        aca = new DefaultMutableTreeNode("ACA");
        acb = new DefaultMutableTreeNode("ACB");
        acc = new DefaultMutableTreeNode("ACC");
        acd = new DefaultMutableTreeNode("ACD");
        ad = new DefaultMutableTreeNode("AD");
        ada = new DefaultMutableTreeNode("ADA");
        adb = new DefaultMutableTreeNode("ADB");
        adc = new DefaultMutableTreeNode("ADC");
        add = new DefaultMutableTreeNode("ADD");
        b = new DefaultMutableTreeNode("B");
        ba = new DefaultMutableTreeNode("BA");
        baa = new DefaultMutableTreeNode("BAA");
        bab = new DefaultMutableTreeNode("BAB");
        bac = new DefaultMutableTreeNode("BAC");
        bad = new DefaultMutableTreeNode("BAD");
        c = new DefaultMutableTreeNode("C");
        ca = new DefaultMutableTreeNode("CA");
        caa = new DefaultMutableTreeNode("CA");
        cab = new DefaultMutableTreeNode("CAB");
        cac = new DefaultMutableTreeNode("CAC");
        cad = new DefaultMutableTreeNode("CAD");

        root.add(a);
        a.add(aa);
        aa.add(aaa);
        aa.add(aab);
        aa.add(aac);
        aa.add(aad);
        a.add(ab);
        ab.add(aba);
        ab.add(abb);
        ab.add(abc);
        ab.add(abd);
        a.add(ac);
        ac.add(aca);
        ac.add(acb);
        ac.add(acc);
        ac.add(acd);
        a.add(ad);
        ad.add(ada);
        ad.add(adb);
        ad.add(adc);
        ad.add(add);
        root.add(b);
        b.add(ba);
        ba.add(baa);
        ba.add(bab);
        ba.add(bac);
        ba.add(bad);
        root.add(c);
        c.add(ca);
        ca.add(caa);
        ca.add(cab);
        ca.add(cac);
        ca.add(cad);

        unfilteredModel = new DefaultTreeModel(root);
        FilteredTreeModelTest.printTree(unfilteredModel);

    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link de.mindcrimeilab.xsanalyzer.model.FilteredTreeModel#getChild(java.lang.Object, int)}.
     */
    @Test
    public final void testGetChild() {
        Assert.assertNotNull(unfilteredModel);
        TreeModel filteredModel = new FilteredTreeModel(unfilteredModel, new ACConstraint());
        FilteredTreeModelTest.printTree(filteredModel);
    }

    // /**
    // * Test method for {@link de.mindcrimeilab.xsanalyzer.model.FilteredTreeModel#getChildCount(java.lang.Object)}.
    // */
    // @Test
    // public final void testGetChildCount() {
    // Assert.fail("Not yet implemented"); // TODO
    // }
    //
    // /**
    // * Test method for
    // * {@link de.mindcrimeilab.xsanalyzer.model.FilteredTreeModel#getIndexOfChild(java.lang.Object,
    // java.lang.Object)}.
    // */
    // @Test
    // public final void testGetIndexOfChild() {
    // Assert.fail("Not yet implemented"); // TODO
    // }
    //
    // /**
    // * Test method for {@link de.mindcrimeilab.xsanalyzer.model.FilteredTreeModel#getRoot()}.
    // */
    // @Test
    // public final void testGetRoot() {
    // Assert.fail("Not yet implemented"); // TODO
    // }
    //
    // /**
    // * Test method for {@link de.mindcrimeilab.xsanalyzer.model.FilteredTreeModel#isLeaf(java.lang.Object)}.
    // */
    // @Test
    // public final void testIsLeaf() {
    // Assert.fail("Not yet implemented"); // TODO
    // }

    private static void printTree(TreeModel tree) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) tree.getRoot();
        FilteredTreeModelTest.printTreeNode(tree, root);
    }

    private static void printTreeNode(TreeModel tree, DefaultMutableTreeNode node) {
        StringBuilder sb = new StringBuilder();
        sb.append(tree.isLeaf(node) ? "-" : "+");

        sb.append(node.getUserObject());
        FilteredTreeModelTest.logger.debug(sb.toString());
        for (int i = 0; i < tree.getChildCount(node); ++i) {
            FilteredTreeModelTest.printTreeNode(tree, (DefaultMutableTreeNode) tree.getChild(node, i));
        }
    }

    private class NoConstraint implements Constraint {

        @Override
        public boolean test(Object argument) {
            return true;
        }
    }

    private class ACConstraint implements Constraint {

        @Override
        public boolean test(Object argument) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) argument;
            final boolean passes = !((String) node.getUserObject()).endsWith("AC");
            FilteredTreeModelTest.logger.debug("[" + node.getUserObject() + "] passes test [" + passes + "]");
            return passes;
        }

    }
}
