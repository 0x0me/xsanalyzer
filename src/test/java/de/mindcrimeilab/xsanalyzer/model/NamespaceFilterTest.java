// $Id:NamespaceFilterTest.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import junit.framework.Assert;

import org.apache.xerces.impl.xs.XSElementDecl;
import org.apache.xerces.xs.XSNamespaceItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class NamespaceFilterTest {

    private NamespaceFilter testee;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        testee = new NamespaceFilter();
    }

    @After
    public void tearDown() throws Exception {
        testee = null;
    }

    @Test
    public final void testAddNamespace() {
        boolean result = testee.addNamespace(null);
        Assert.assertTrue(result);
    }

    @Test
    public final void testRemoveNamespace() {
        boolean result = testee.removeNamespace(null);
        Assert.assertFalse(result);
    }

    @Test
    public final void testIsFiltered() {
        XSElementDecl filteredElement = new XSElementDecl();
        filteredElement.fTargetNamespace = "http://test/filtered";
        filteredElement.fName = "filteredElement";
        XSNamespaceItem filteredNs = filteredElement.getNamespaceItem();

        boolean result = testee.addNamespace("http://test/filtered");
        Assert.assertTrue(result);

        result = testee.isFiltered(filteredElement);
        Assert.assertTrue(result);

        XSElementDecl element = new XSElementDecl();
        element.fTargetNamespace = "http://test/not-filtered";
        element.fName = "unFilteredElement";
        XSNamespaceItem ns = filteredElement.getNamespaceItem();

        result = testee.isFiltered(element);
        Assert.assertFalse(result);

    }

}
