// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.model;

import java.io.File;

import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSTypeDefinition;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mindcrimeilab.xsanalyzer.util.XsModelFactory;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class TypeHierarchyTreeModelAdapterTest {

    private XSModel model;

    private TypeHierarchyTreeModelAdapter testee;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/typeHierarchy.xsd"));
        XSTypeDefinition def = model.getTypeDefinition("root", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy");
        Assert.assertNotNull(def);
        testee = new TypeHierarchyTreeModelAdapter(def);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link de.mindcrimeilab.xsanalyzer.model.TypeHierarchyTreeModelAdapter#getChild(java.lang.Object, int)}.
     */
    @Test
    public final void testGetChild() {
        XSObject[] expectedChildren = new XSObject[] { model.getTypeDefinition("anySimpleType", "http://www.w3.org/2001/XMLSchema"), model.getTypeDefinition("string", "http://www.w3.org/2001/XMLSchema"),
                model.getTypeDefinition("childchild", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"), model.getTypeDefinition("child", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"),
                model.getTypeDefinition("root", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"), null};

        XSObject parent = (XSObject) testee.getRoot();
        XSObject child = null;
        int i = 0;
        while (null != (child = (XSObject) testee.getChild(parent, 0))) {
            Assert.assertEquals(expectedChildren[++i], child);
            Assert.assertNull(testee.getChild(parent, 1));
            parent = child;
        }
    }

    /**
     * Test method for
     * {@link de.mindcrimeilab.xsanalyzer.model.TypeHierarchyTreeModelAdapter#getChildCount(java.lang.Object)}.
     */
    @Test
    public final void testGetChildCount() {
        final XSTypeDefinition leaf = model.getTypeDefinition("root", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy");
        XSObject[] nodes = new XSObject[] { model.getTypeDefinition("anySimpleType", "http://www.w3.org/2001/XMLSchema"), model.getTypeDefinition("string", "http://www.w3.org/2001/XMLSchema"),
                model.getTypeDefinition("childchild", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"), model.getTypeDefinition("child", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"), leaf, null};

        for (XSObject node : nodes) {
            int actualCount = testee.getChildCount(node);
            Assert.assertEquals((null == node || leaf.equals(node)) ? 0 : 1, actualCount);
        }
    }

    /**
     * Test method for
     * {@link de.mindcrimeilab.xsanalyzer.model.TypeHierarchyTreeModelAdapter#getIndexOfChild(java.lang.Object, java.lang.Object)}
     * .
     */
    @Test
    public final void testGetIndexOfChild() {
        XSObject[] nodes = new XSObject[] { model.getTypeDefinition("anySimpleType", "http://www.w3.org/2001/XMLSchema"), model.getTypeDefinition("string", "http://www.w3.org/2001/XMLSchema"),
                model.getTypeDefinition("childchild", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"), model.getTypeDefinition("child", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"),
                model.getTypeDefinition("root", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"), null, null};

        for (int i = 1; i < nodes.length; ++i) {
            int actualIdx = testee.getIndexOfChild(nodes[i - 1], nodes[i]);
            Assert.assertEquals((null == nodes[i - 1] || null == nodes[i]) ? -1 : 0, actualIdx);
        }

    }

    /**
     * Test method for {@link de.mindcrimeilab.xsanalyzer.model.TypeHierarchyTreeModelAdapter#getRoot()}.
     */
    @Test
    public final void testGetRoot() {
        XSObject expectedRoot = model.getTypeDefinition("anySimpleType", "http://www.w3.org/2001/XMLSchema");
        XSObject root = (XSObject) testee.getRoot();
        Assert.assertEquals(expectedRoot, root);
    }

    /**
     * Test method for {@link de.mindcrimeilab.xsanalyzer.model.TypeHierarchyTreeModelAdapter#isLeaf(java.lang.Object)}.
     */
    @Test
    public final void testIsLeaf() {
        XSObject[] nodes = new XSObject[] { model.getTypeDefinition("anySimpleType", "http://www.w3.org/2001/XMLSchema"), model.getTypeDefinition("string", "http://www.w3.org/2001/XMLSchema"),
                model.getTypeDefinition("childchild", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"), model.getTypeDefinition("child", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy"),
                model.getTypeDefinition("root", "http://mindcrime-ilab.de/xsdanalyzer/test/typeHierarchy")};

        boolean[] expectedResult = new boolean[] { false, false, false, false, true};

        Assert.assertEquals("Length of arrays must be the same.", expectedResult.length, nodes.length);
        for (int i = 0; i < expectedResult.length; ++i) {
            Assert.assertEquals(expectedResult[i], testee.isLeaf(nodes[i]));
        }
    }

}
