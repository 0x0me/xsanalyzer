// $Id$
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer.xsext;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTypeDefinition;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mindcrimeilab.xsanalyzer.util.XsModelFactory;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class ComplexTypeDescriptionTest {

    private static final Log logger = LogFactory.getLog("xsAnalyzerJunitTestLogger");

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link de.mindcrimeilab.xsanalyzer.xsext.ComplexTypeDescription#getSignatureDigest(org.apache.xerces.xs.XSComplexTypeDefinition, java.security.MessageDigest)}
     * .
     */
    @Test
    public final void testGetSignatureDigest() {
        simpleComplexTypeChecks();
        complexComplexTypeChecks();
    }

    private void complexComplexTypeChecks() {
        XSModel model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/complexComplexType.xsd"));

        final String namespace = "http://mindcrime-ilab.de/xsdanalyzer/test/complexComplexType";

        String actual = getDigest(model, namespace, "typeA");
        ComplexTypeDescriptionTest.logger.debug("expected result = [" + actual + "]");
        Assert.assertEquals(actual, getDigest(model, namespace, "typeB"));

        actual = getDigest(model, namespace, "typeC");
        Assert.assertEquals(actual, getDigest(model, namespace, "typeD"));
        Assert.assertFalse(actual.equals(getDigest(model, namespace, "typeE")));

        actual = getDigest(model, namespace, "typeE");
        Assert.assertFalse(actual.equals(getDigest(model, namespace, "typeF")));
    }

    /**
     * 
     */
    private void simpleComplexTypeChecks() {
        XSModel model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/simpleComplexType.xsd"));

        String actual = null;

        final String namespace = "http://mindcrime-ilab.de/xsdanalyzer/test/simpleComplexType";
        actual = getDigest(model, namespace, "simpleRestrictedComplexType");
        checkSimilarTo(model, actual, "simpleType");
        simpleTypeNotSimilarTo(model, actual, "notSimilarToLengthType");
        complexTypeNotSimilarTo(model, actual, "ctypeWithElement");

        actual = getDigest(model, namespace, "lengthType");
        simpleTypeNotSimilarTo(model, actual, "notSimilarToLengthType");
        complexTypeNotSimilarTo(model, actual, "attributeTypeB");

        actual = getDigest(model, namespace, "attributeTypeA");
        simpleTypeNotSimilarTo(model, actual, "notSimilarToLengthType");
        complexTypeNotSimilarTo(model, actual, "lengthType");
        checkSimilarTo(model, actual, "attributeTypeB");
        complexTypeNotSimilarTo(model, actual, "attributeTypeC");
    }

    /**
     * @param model
     * @param actual
     * @param namespace
     * @param name
     * @return
     */
    private String getDigest(XSModel model, final String namespace, final String name) {
        String actual = null;
        try {
            XSComplexTypeDefinition ctypedef = (XSComplexTypeDefinition) model.getTypeDefinition(name, namespace);
            Assert.assertNotNull("Could not load complex type definition", ctypedef);
            actual = ComplexTypeDescription.getSignatureDigest(ctypedef, MessageDigest.getInstance("MD5"));
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        return actual;
    }

    /**
     * @param model
     * @param actual
     * @param name
     * @return
     */
    private void simpleTypeNotSimilarTo(XSModel model, String actual, final String name) {
        String notExpectedType = null;

        try {
            XSTypeDefinition typedef = model.getTypeDefinition(name, "http://mindcrime-ilab.de/xsdanalyzer/test/simpleComplexType");
            Assert.assertTrue("{" + typedef.getNamespace() + "}" + typedef.getName() + " is no simple type", typedef instanceof XSSimpleTypeDefinition);
            notExpectedType = SimpleTypeDescription.getSignatureDigest((XSSimpleTypeDefinition) typedef, MessageDigest.getInstance("MD5"));
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

        ComplexTypeDescriptionTest.logger.debug("in[" + actual + "] calc[" + notExpectedType + "]");

        Assert.assertNotNull("Could not find type " + name + " in schema!", notExpectedType);
        Assert.assertFalse(notExpectedType.equals(actual));
    }

    /**
     * @param model
     * @param actual
     * @param name
     * @return
     */
    private void complexTypeNotSimilarTo(XSModel model, String actual, final String name) {
        String digest = null;

        try {
            XSTypeDefinition typedef = model.getTypeDefinition(name, "http://mindcrime-ilab.de/xsdanalyzer/test/simpleComplexType");
            Assert.assertTrue("{" + typedef.getNamespace() + "}" + typedef.getName() + " is no complex type", typedef instanceof XSComplexTypeDefinition);
            digest = ComplexTypeDescription.getSignatureDigest((XSComplexTypeDefinition) typedef, MessageDigest.getInstance("MD5"));
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        ComplexTypeDescriptionTest.logger.debug("in[" + actual + "] calc[" + digest + "]");

        Assert.assertNotNull(digest);
        Assert.assertFalse(digest.equals(actual));
    }

    /**
     * @param model
     * @param actual
     * @param name
     * @return
     */
    private void checkSimilarTo(XSModel model, String actual, final String name) {
        String expected = null;
        try {

            XSTypeDefinition typedef = model.getTypeDefinition(name, "http://mindcrime-ilab.de/xsdanalyzer/test/simpleComplexType");
            switch (typedef.getTypeCategory()) {
                case XSTypeDefinition.SIMPLE_TYPE:
                    expected = SimpleTypeDescription.getSignatureDigest((XSSimpleTypeDefinition) typedef, MessageDigest.getInstance("MD5"));
                    break;
                case XSTypeDefinition.COMPLEX_TYPE:
                    expected = ComplexTypeDescription.getSignatureDigest((XSComplexTypeDefinition) typedef, MessageDigest.getInstance("MD5"));
                    break;
                default:
                    Assert.fail("Unexpected type {" + typedef.getNamespace() + "}" + typedef.getName() + "!");
            }

        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected, actual);
    }
}
