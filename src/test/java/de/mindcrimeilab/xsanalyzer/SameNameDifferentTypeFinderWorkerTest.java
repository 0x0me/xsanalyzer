// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.io.File;
import java.util.Map;

import javax.xml.namespace.QName;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSObjectList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mindcrimeilab.xsanalyzer.util.XsModelFactory;

/**
 * 
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SameNameDifferentTypeFinderWorkerTest {

    private static final Log logger = LogFactory.getLog("xsAnalyzerJunitTestLogger");

    private XSModel model;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/sameNameDifferentType.xsd"));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link de.mindcrimeilab.xsanalyzer.SameNameDifferentTypeFinderWorker#execute(org.apache.xerces.xs.XSObject, org.apache.xerces.xs.XSObject)}
     * .
     */
    @Test
    public void testExecute() {
        SameNameDifferentTypeFinderWorker finder = new SameNameDifferentTypeFinderWorker();
        finder.setFilteredNamespaces(new String[] { "http://www.w3.org/2001/XMLSchema"});

        ComponentVisitor visitor = new ComponentVisitor();

        XsModelWalker walker = new XsModelWalker();
        walker.addWorker(finder);
        walker.addWorker(visitor);
        walker.walkModel(model);

        Map<QName, ? extends XSObjectList> m = finder.getTypesWithSameName();
        Assert.assertNotNull(m);

        for (QName qname : m.keySet()) {
            XSObjectList list = m.get(qname);
            logger.debug("UsedType [" + qname + "] has [" + list + "]");
            logger.debug("List contains [" + list.getLength() + "] types");
            if (null == list) {
                continue;
            }
            for (int i = 0; i < list.getLength(); ++i) {
                logger.debug("Containing type [" + list.item(i) + "]");
                Assert.assertNotNull(list.item(i));
            }
        }
    }

}
