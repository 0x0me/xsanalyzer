// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mindcrimeilab.xsanalyzer.util.XsModelFactory;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class XsModelWalkerTest {

    private static final Log logger = LogFactory.getLog("xsAnalyzerJunitTestLogger");

    private XSModel model;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/unusedTypes.xsd"));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link de.mindcrimeilab.xsanalyzer.XsModelWalker#walkModel(org.apache.xerces.xs.XSModel)} .
     */
    @Test
    public final void testWalk() {
        ComponentVisitor visitor = new ComponentVisitor();
        XsModelWalker walker = new XsModelWalker();
        walker.addWorker(visitor);
        walker.walkModel(model);
        Map<XSObject, Integer> visits = visitor.getVisits();

        for (XSObject object : visits.keySet()) {
            System.out.println("- [{" + object.getNamespace() + "}:" + object.getName() + "] visited [" + visits.get(object) + "] times.");
        }

    }

    @Test
    public final void testAddWorker() {
        XsComponentWorker visitor = new ComponentVisitor();
        XsComponentWorker typeUsage = new TypeUsageWorker();
        XsComponentWorker similar = new SimilarTypeFinderWorker();
        XsComponentWorker sameName = new SameNameDifferentTypeFinderWorker();

        XsModelWalker testee = new XsModelWalker();
        assertNotNull(testee.getWorkers());
        assertTrue(testee.getWorkers().isEmpty());

        assertTrue(testee.addWorker(visitor));
        assertTrue(testee.addWorker(typeUsage));
        assertTrue(testee.addWorker(similar));
        assertTrue(testee.addWorker(sameName));

        assertTrue(testee.hasWorker(visitor));
        assertTrue(testee.hasWorker(typeUsage));
        assertTrue(testee.hasWorker(similar));
        assertTrue(testee.hasWorker(sameName));

        List<XsComponentWorker> expectedWorkers = new LinkedList<XsComponentWorker>();
        assertTrue(expectedWorkers.add(visitor));
        assertTrue(expectedWorkers.add(typeUsage));
        assertTrue(expectedWorkers.add(similar));
        assertTrue(expectedWorkers.add(sameName));

        List<XsComponentWorker> workers = new LinkedList<XsComponentWorker>(testee.getWorkers());
        for (XsComponentWorker w : workers) {
            assertTrue(expectedWorkers.remove(w));
        }

        assertTrue(expectedWorkers.isEmpty());

    }

    @Test
    public final void testAddWorkers() {
        XsComponentWorker visitor = new ComponentVisitor();
        XsComponentWorker typeUsage = new TypeUsageWorker();
        XsComponentWorker similar = new SimilarTypeFinderWorker();
        XsComponentWorker sameName = new SameNameDifferentTypeFinderWorker();

        XsModelWalker testee = new XsModelWalker();
        assertNotNull("Assume that the worker list is not null", testee.getWorkers());
        assertTrue("Assume that the worker list is not empty", testee.getWorkers().isEmpty());

        List<XsComponentWorker> expectedWorkers = new LinkedList<XsComponentWorker>();
        assertTrue("Adding worker to expected result list", expectedWorkers.add(visitor));
        assertTrue("Adding worker to expected result list", expectedWorkers.add(typeUsage));
        assertTrue("Adding worker to expected result list", expectedWorkers.add(similar));
        assertTrue("Adding worker to expected result list", expectedWorkers.add(sameName));
        assertTrue("Expecting 4 workers now", expectedWorkers.size() == 4);

        List<XsComponentWorker> workerList = new LinkedList<XsComponentWorker>();
        assertTrue("Adding worker to list", workerList.add(visitor));
        assertTrue("Adding worker to list", workerList.add(typeUsage));
        assertTrue("Adding worker to list", workerList.add(similar));
        assertTrue("Adding worker to list", workerList.add(sameName));
        assertTrue("Expecting 4 workers now", workerList.size() == 4);

        assertTrue("Adding collection of workers", testee.addWorkers(workerList));

        List<XsComponentWorker> workers = new LinkedList<XsComponentWorker>(testee.getWorkers());
        for (XsComponentWorker w : workers) {
            assertTrue("Remove worker from expected worker list", expectedWorkers.remove(w));
        }

        assertTrue("Assume that the list of expected workers is now empty", expectedWorkers.isEmpty());
    }
}
