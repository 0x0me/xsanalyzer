// $Id$
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.xsanalyzer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.mindcrimeilab.xsanalyzer.util.XsModelFactory;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author$
 * @version $Revision$
 * 
 */
public class SimilarTypeFinderWorkerTest {

    private static final Log logger = LogFactory.getLog("xsAnalyzerJunitTestLogger");

    private XSModel model;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/simpleTypes.xsd"));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link de.mindcrimeilab.xsanalyzer.SimilarTypeFinderWorker#getSimilarTypes()}.
     */
    @Test
    public final void testGetSimilarTypes() {
        // testSimilarTypes();
        // testEnumeration();
        // testLength();
        // testInheritence();
        testUnion();
    }

    /**
     * 
     */
    private void testSimilarTypes() {
        SimilarTypeFinderWorker finder = new SimilarTypeFinderWorker();
        finder.setFilteredNamespaces(new String[] { "http://www.w3.org/2001/XMLSchema"});

        ComponentVisitor visitor = new ComponentVisitor();

        XsModelWalker walker = new XsModelWalker();
        walker.addWorker(finder);
        walker.addWorker(visitor);
        walker.walkModel(model);

        Map<String, ? extends XSObjectList> m = finder.getSimilarTypes();
        Assert.assertNotNull(m);

        for (String signatureDigest : m.keySet()) {
            XSObjectList list = m.get(signatureDigest);
            SimilarTypeFinderWorkerTest.logger.debug("UsedType [" + signatureDigest + "] has [" + list + "]");
            SimilarTypeFinderWorkerTest.logger.debug("List contains [" + list.getLength() + "] types");
            if (null == list) {
                continue;
            }
            for (int i = 0; i < list.getLength(); ++i) {
                SimilarTypeFinderWorkerTest.logger.debug("Containing type [" + list.item(i) + "]");
                Assert.assertNotNull(list.item(i));
            }
        }
    }

    private void testEnumeration() {
        String twoDifferent = "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"\n" + "    targetNamespace=\"http://mindcrime-ilab.de/xsdanalyzer/test/similarTypes\"\n"
                + "    xmlns:tns=\"http://mindcrime-ilab.de/xsdanalyzer/test/similarTypes\"\n" + "    elementFormDefault=\"qualified\"> <xs:simpleType name=\"MassUnitType\">\n" + "        <xs:restriction base=\"xs:string\">\n"
                + "            <xs:enumeration value=\"t\" />\n" + "            <xs:enumeration value=\"kg\" />\n" + "            <xs:enumeration value=\"g\" />\n" + "            <xs:enumeration value=\"mg\" />\n"
                + "        </xs:restriction>\n" + "    </xs:simpleType>" + "    <xs:simpleType name=\"LengthUnitType\">\n" + "        <xs:restriction base=\"xs:string\">\n" + "            <xs:enumeration value=\"km\" />\n"
                + "            <xs:enumeration value=\"m\" />\n" + "            <xs:enumeration value=\"dm\" />\n" + "            <xs:enumeration value=\"cm\" />\n" + "        </xs:restriction>\n" + "    </xs:simpleType>" + "</xs:schema>";

        XSModel m = XsModelFactory.createXsModel(twoDifferent);

        SimilarTypeFinderWorker finder = new SimilarTypeFinderWorker();
        finder.setFilteredNamespaces(new String[] { "http://www.w3.org/2001/XMLSchema"});

        ComponentVisitor visitor = new ComponentVisitor();

        XsModelWalker walker = new XsModelWalker();
        walker.addWorker(finder);
        walker.addWorker(visitor);
        walker.walkModel(m);

        Map<String, ? extends XSObjectList> similarTypes = finder.getSimilarTypes();
        Assert.assertNotNull(similarTypes);
        Assert.assertTrue("Found similar types, but both types differ in enumeration values", similarTypes.isEmpty());

        String twoEqual = "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"\n" + "    targetNamespace=\"http://mindcrime-ilab.de/xsdanalyzer/test/similarTypes\"\n"
                + "    xmlns:tns=\"http://mindcrime-ilab.de/xsdanalyzer/test/similarTypes\"\n" + "    elementFormDefault=\"qualified\"> <xs:simpleType name=\"MassUnitType\">\n" + "        <xs:restriction base=\"xs:string\">\n"
                + "            <xs:enumeration value=\"t\" />\n" + "            <xs:enumeration value=\"kg\" />\n" + "            <xs:enumeration value=\"g\" />\n" + "            <xs:enumeration value=\"mg\" />\n"
                + "        </xs:restriction>\n" + "    </xs:simpleType>" + "    <xs:simpleType name=\"MassUnitType2\">\n" + "        <xs:restriction base=\"xs:string\">\n" + "            <xs:enumeration value=\"t\" />\n"
                + "            <xs:enumeration value=\"kg\" />\n" + "            <xs:enumeration value=\"g\" />\n" + "            <xs:enumeration value=\"mg\" />\n" + "        </xs:restriction>\n" + "    </xs:simpleType>" + "</xs:schema>";

        m = XsModelFactory.createXsModel(twoEqual);

        walker.addWorker(finder);
        walker.walkModel(m);
        similarTypes = finder.getSimilarTypes();
        Assert.assertNotNull(similarTypes);
        Assert.assertFalse("Found no similar types, but both types are similar", similarTypes.isEmpty());

    }

    private void testLength() {
        XSModel model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/simpleSimilarTypesLength.xsd"));

        SimilarTypeFinderWorker finder = new SimilarTypeFinderWorker();
        finder.setFilteredNamespaces(new String[] { "http://www.w3.org/2001/XMLSchema"});

        ComponentVisitor visitor = new ComponentVisitor();

        XsModelWalker walker = new XsModelWalker();
        walker.addWorker(finder);
        walker.addWorker(visitor);
        walker.walkModel(model);

        Map<String, ? extends XSObjectList> similarTypes = finder.getSimilarTypes();
        Assert.assertNotNull(similarTypes);
        Assert.assertEquals(1, similarTypes.size());

        Collection<? extends XSObjectList> values = similarTypes.values();
        Assert.assertNotNull(values);
        Assert.assertEquals(1, values.size());
        XSObjectList xsoList = values.iterator().next();
        Assert.assertEquals(2, xsoList.getLength());
        XSObject obj1 = xsoList.item(0);
        XSObject obj2 = xsoList.item(1);

        Assert.assertNotNull(obj1);
        Assert.assertNotNull(obj2);

        XSObject expectedObject1 = model.getTypeDefinition("similarType1", "http://mindcrime-ilab.de/xsdanalyzer/test/simpleSimilarTypesLength");
        XSObject expectedObject2 = model.getTypeDefinition("similarType2", "http://mindcrime-ilab.de/xsdanalyzer/test/simpleSimilarTypesLength");

        if (expectedObject1.equals(obj1)) {
            Assert.assertEquals(expectedObject1, obj1);
            Assert.assertEquals(expectedObject2, obj2);
        }
        else {
            Assert.assertEquals(expectedObject1, obj2);
            Assert.assertEquals(expectedObject2, obj1);
        }
    }

    private void testInheritence() {
        XSModel model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/simpleSimilarTypesInherit.xsd"));

        SimilarTypeFinderWorker finder = new SimilarTypeFinderWorker();
        finder.setFilteredNamespaces(new String[] { "http://www.w3.org/2001/XMLSchema"});

        ComponentVisitor visitor = new ComponentVisitor();

        XsModelWalker walker = new XsModelWalker();
        walker.addWorker(finder);
        walker.addWorker(visitor);
        walker.walkModel(model);

        Map<String, ? extends XSObjectList> similarTypes = finder.getSimilarTypes();
        Assert.assertNotNull(similarTypes);
        Assert.assertEquals(1, similarTypes.size());

        Collection<? extends XSObjectList> values = similarTypes.values();
        Assert.assertNotNull(values);
        Assert.assertEquals(1, values.size());
        XSObjectList xsoList = values.iterator().next();

        Assert.assertEquals(4, xsoList.getLength());

        List<XSObject> expectedResult = new ArrayList<XSObject>();
        expectedResult.add(model.getTypeDefinition("similarType1", "http://mindcrime-ilab.de/xsdanalyzer/test/simpleSimilarTypesInherit"));
        expectedResult.add(model.getTypeDefinition("similarType2", "http://mindcrime-ilab.de/xsdanalyzer/test/simpleSimilarTypesInherit"));
        expectedResult.add(model.getTypeDefinition("similarType3", "http://mindcrime-ilab.de/xsdanalyzer/test/simpleSimilarTypesInherit"));
        expectedResult.add(model.getTypeDefinition("baseType1", "http://mindcrime-ilab.de/xsdanalyzer/test/simpleSimilarTypesInherit"));

        checkXSObjectList(xsoList, expectedResult);
    }

    private void testUnion() {
        XSModel model = XsModelFactory.createXsModel(new File("src/test/resources/xsd/simpleSimilarTypesUnion.xsd"));

        SimilarTypeFinderWorker finder = new SimilarTypeFinderWorker();
        finder.setFilteredNamespaces(new String[] { "http://www.w3.org/2001/XMLSchema"});

        ComponentVisitor visitor = new ComponentVisitor();

        XsModelWalker walker = new XsModelWalker();
        walker.addWorker(finder);
        walker.addWorker(visitor);
        walker.walkModel(model);

        Map<String, ? extends XSObjectList> similarTypes = finder.getSimilarTypes();
        Assert.assertNotNull(similarTypes);
        Assert.assertEquals(1, similarTypes.size());

        Collection<? extends XSObjectList> values = similarTypes.values();
        Assert.assertNotNull(values);
        Assert.assertEquals(1, values.size());
        values.iterator().next();

        // TODO complete testcase simple type union
    }

    /**
     * @param xsoList
     * @param expectedResult
     */
    private void checkXSObjectList(XSObjectList xsoList, List<XSObject> expectedResult) {
        for (int i = 0; i < xsoList.getLength(); ++i) {
            XSObject obj = xsoList.item(i);
            if (!expectedResult.contains(obj)) {
                Assert.fail("Found unexpected similar type [" + obj + "]");
            }
            Assert.assertTrue(expectedResult.remove(obj));
        }
        Assert.assertTrue("Expected empty list of similar types, but there is at least one entry left.", expectedResult.isEmpty());
    }

}

/*
 * Signature of type
 * [{http://mindcrime-ilab.de/xsdanalyzer/test/simpleSimilarTypesUnion}:similarType2|http://www.w3.org/
 * 2001/XMLSchema:string
 * :2048:null:1024:null:1:null:64:null:32:null:4:null:128:null:256:null:2:null:0:null:8:null:512:null:16:collapse:] is
 * [32e44df712af6fe9c5ea59134c6eb27] Signature of type
 * [{http://mindcrime-ilab.de/xsdanalyzer/test/simpleSimilarTypesUnion
 * }:similarType1|http://www.w3.org/2001/XMLSchema:string
 * :2048:null:1024:null:1:3:64:null:32:null:4:3:128:null:256:null:2:3:0:null:8:null:512:null:16:preserve:abc:xyz:] is
 * [7eeb87b02c58f974cba56256cff0e752]
 */