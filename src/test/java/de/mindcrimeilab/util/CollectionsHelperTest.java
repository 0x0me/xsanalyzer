// $Id:CollectionsHelperTest.java 62 2008-04-20 12:28:56Z me $
/*
 * xsAnalyzer - XML schema analyzing tool. Copyright (C) 2008 Michael Engelhardt
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/**
 * 
 */
package de.mindcrimeilab.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Michael Engelhardt<me@mindcrime-ilab.de>
 * @author $Author:me $
 * @version $Revision:62 $
 * 
 */
public class CollectionsHelperTest {

    private static final Collection<String> stringCollection = Arrays.asList(new String[] { "123One", "123Two", "One", "Two", "123Three", "Four", "123Five"});

    private static final Collection<String> expectedStringCollection = Arrays.asList(new String[] { "One", "Two", "Four"});

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link de.mindcrimeilab.util.CollectionsHelper#filterList(List<T>, de.mindcrimeilab.util.Filter)}
     * .
     */
    @Test
    public final void testFilterCollection() {
        List<String> tmp = new ArrayList<String>(CollectionsHelperTest.stringCollection);
        CollectionsHelper.filterCollection(tmp, new TestStringFilter());
        Assert.assertEquals(CollectionsHelperTest.expectedStringCollection, tmp);
    }

    private class TestStringFilter implements Filter<String> {

        @Override
        public boolean isFiltered(String object) {
            return object.startsWith("123");
        }

    }

}
